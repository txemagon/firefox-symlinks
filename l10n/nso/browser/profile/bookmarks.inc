# This Source Code Form is subject to the terms of the Mozilla Public# License, v. 2.0. If a copy of the MPL was not distributed with this# file, You can obtain one at http://mozilla.org/MPL/2.0/.#filter emptyLines
# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Dipukutshwayo


#define bookmarks_heading Dipukutshwayo


#define bookmarks_toolbarfolder Foltara ya thulupaa ya dipukutshwayo


#define bookmarks_toolbarfolder_description Oketša dipukutshwayo foltareng ye gore o di bone di bontšhitšwe go Thulupaa ya dipukutshwayo


# LOCALIZATION NOTE (getting_started):

# link title for http://en-US.www.mozilla.com/en-US/firefox/central/

#define getting_started Kamoo o ka thomago


# LOCALIZATION NOTE (firefox_heading):

# Firefox links folder name

#define firefox_heading Mozilla Firefox


# LOCALIZATION NOTE (firefox_help):

# link title for http://en-US.www.mozilla.com/en-US/firefox/help/

#define firefox_help Thušo le tlhahlo


# LOCALIZATION NOTE (firefox_customize):

# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/

#define firefox_customize Tlwaelanya Firefox


# LOCALIZATION NOTE (firefox_community):

# link title for http://en-US.www.mozilla.com/en-US/firefox/community/

#define firefox_community Tsenya letsogo


# LOCALIZATION NOTE (firefox_about):

# link title for http://en-US.www.mozilla.com/en-US/about/

#define firefox_about Ka ga rena


#unfilter emptyLines