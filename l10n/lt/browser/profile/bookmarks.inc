#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Adresynas
#define bookmarks_heading Adresynas

#define bookmarks_toolbarfolder Adresyno juosta
#define bookmarks_toolbarfolder_description Čia įdėti adresai rodomi naršyklės adresyno juostoje

# LOCALIZATION NOTE (getting_started):
# link title for http://www.mozilla.com/lt/firefox/central/
#define getting_started Naudojimasis naršykle

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Mozilla Firefox

# LOCALIZATION NOTE (firefox_help):
# link title for http://www.mozilla.com/lt/firefox/help/
#define firefox_help Žinynas ir pagalba

# LOCALIZATION NOTE (firefox_customize):
# link title for http://www.mozilla.com/lt/firefox/customize/
#define firefox_customize „Firefox“ pritaikymas savo poreikiams

# LOCALIZATION NOTE (firefox_community):
# link title for http://www.mozilla.com/lt/firefox/community/
#define firefox_community Kaip prisidėti prie programos tobulinimo

# LOCALIZATION NOTE (firefox_about):
# link title for http://www.mozilla.com/lt/firefox/about/
#define firefox_about Apie „Mozilla“

#unfilter emptyLines
