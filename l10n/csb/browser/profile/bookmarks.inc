#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Załóżczi
#define bookmarks_heading Załóżczi

#define bookmarks_toolbarfolder Katalog nôrzãdłów załóżków
#define bookmarks_toolbarfolder_description Dodôj załóżczi do tegò katalogù bë widzec je wëskrzënioné w nôrzãdłach załóżków

# LOCALIZATION NOTE (getting_started):
# link title for http://en-US.www.mozilla.com/en-US/firefox/central/
#define getting_started Zaczątk

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Mozilla Firefox

# LOCALIZATION NOTE (firefox_help):
# link title for http://en-US.www.mozilla.com/en-US/firefox/help/
#define firefox_help Pòmòc i ùczbòwniczi

# LOCALIZATION NOTE (firefox_customize):
# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/
#define firefox_customize Dopasëjë Firefoksa

# LOCALIZATION NOTE (firefox_community):
# link title for http://en-US.www.mozilla.com/en-US/firefox/community/
#define firefox_community Wespółrobi

# LOCALIZATION NOTE (firefox_about):
# link title for http://en-US.www.mozilla.com/en-US/about/
#define firefox_about Ò nas

#unfilter emptyLines
