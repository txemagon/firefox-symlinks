#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title ओळखचिन्हे
#define bookmarks_heading ओळखचिन्हे

# LOCALIZATION NOTE (bookmarks_addons):
# link title for https://en-US.add-ons.mozilla.com/en-US/firefox/bookmarks/
# #define bookmarks_addons Get Bookmark Add-ons

#define bookmarks_toolbarfolder ओळखचिन्हे साधनपट्टी फोल्डर
#define bookmarks_toolbarfolder_description ओळखचिन्हे साधनपट्टीवर दाखवण्यासाठी या फोल्डरवर ओळखचिन्हे समाविष्ट करा

# LOCALIZATION NOTE (getting_started):
# link title for http://en-US.www.mozilla.com/en-US/firefox/central/
#define getting_started सुरू करा

# LOCALIZATION NOTE (latest_headlines):
# link title for the live bookmarks sample, a redirect on
# http://en-US.fxfeeds.mozilla.com/en-US/firefox/headlines.xml
# Changing the redirect is subject to approval of l10n-drivers.

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading मोझिला फायरफॉक्स

# LOCALIZATION NOTE (firefox_help):
# link title for http://en-US.www.mozilla.com/en-US/firefox/help/
#define firefox_help मदत व ट्यूटोरिअल्स्

# LOCALIZATION NOTE (firefox_customize):
# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/
#define firefox_customize फायरफॉक्स पसंतीचे करा

# LOCALIZATION NOTE (firefox_community):
# link title for http://en-US.www.mozilla.com/en-US/firefox/community/
#define firefox_community सामिल व्हा

# LOCALIZATION NOTE (firefox_about):
# link title for http://en-US.www.mozilla.com/en-US/firefox/about/
#define firefox_about आमच्या विषयी

#unfilter emptyLines
