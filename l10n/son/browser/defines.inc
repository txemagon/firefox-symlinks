#filter emptyLines
#define MOZ_LANGPACK_CREATOR Soŋay Mozilla kondaa

# If non-English locales wish to credit multiple contributors, uncomment this
# variable definition and use the format specified.
#define MOZ_LANGPACK_CONTRIBUTORS <em:contributor>Abdoul Seydou Cissé</em:contributor><em:contributor>Mohomodou Houssouba</em:contributor>

#unfilter emptyLines