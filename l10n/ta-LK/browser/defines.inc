#filter emptyLines

#define MOZ_LANGPACK_CREATOR mozilla.org

# If non-English locales wish to credit multiple contributors, uncomment this
# variable definition and use the format specified.
#define MOZ_LANGPACK_CONTRIBUTORS <em:contributor>Kengatharaiyer Sarveswaran</em:contributor> <em:contributor>Mohamed Nimshath</em:contributor> <em:contributor>Kavitha Logan</em:contributor> <em:contributor>Balanithy Murugaiah </em:contributor><em:contributor>Mohamed Rila</em:contributor> <em:contributor>Rohana Dasanayaka</em:contributor>

#unfilter emptyLines
