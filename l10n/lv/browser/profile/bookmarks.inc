#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Grāmatzīmes
#define bookmarks_heading Grāmatzīmes

#define bookmarks_toolbarfolder Rīkjoslas grāmatzīmju mape
#define bookmarks_toolbarfolder_description Pievienojiet grāmatzīmes šai mapei, lai redzētu tās uz savas grāmatzīmju rīkjosklas

# LOCALIZATION NOTE (getting_started):
# link title for http://en-US.www.mozilla.com/en-US/firefox/central/
#define getting_started Pirmie soļi

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Mozilla Firefox

# LOCALIZATION NOTE (firefox_help):
# link title for http://en-US.www.mozilla.com/en-US/firefox/help/
#define firefox_help Palīdzība un pamācības

# LOCALIZATION NOTE (firefox_customize):
# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/
#define firefox_customize Firefox pielāgošana

# LOCALIZATION NOTE (firefox_community):
# link title for http://en-US.www.mozilla.com/en-US/firefox/community/
#define firefox_community Kā iesaistīties

# LOCALIZATION NOTE (firefox_about):
# link title for http://en-US.www.mozilla.com/en-US/firefox/about/
#define firefox_about Par mums

#unfilter emptyLines
