# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Script Debugger
# which is available from the Web Developer sub-menu -> 'Script Debugger'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (pauseLabel): The label that is displayed on the pause
# button when the debugger is in a running state.

# LOCALIZATION NOTE (ToolboxDebugger.label):
# This string is displayed in the title of the tab when the debugger is
# displayed inside the developer tools window and in the Developer Tools Menu.
ToolboxDebugger.label=Atkļūdotājs

# LOCALIZATION NOTE (ToolboxDebugger.tooltip):
# This string is displayed in the tooltip of the tab when the debugger is
# displayed inside the developer tools window..
ToolboxDebugger.tooltip=JavaScript atkļūdotājs


resumeButtonTooltip=Klikšķiniet, lai turpinātu (%S)

confirmTabSwitch.message=Atkļūdotājs jau ir atvērts kādā citā cilnē. Turpinot otra atvērtā instance tiks aizvērta.
confirmTabSwitch.buttonSwitch=Pārslēgties uz atkļūdotāja cilni
confirmTabSwitch.buttonSwitch.accessKey=S
confirmTabSwitch.buttonOpen=Atvērt jauno
confirmTabSwitch.buttonOpen.accessKey=O
debuggerMenu.accesskey=D

# LOCALIZATION NOTE (open.commandkey): The key used to open the debugger in
# combination to e.g. ctrl + shift
open.commandkey=S


chromeDebuggerWindowTitle=Pārlūka atkļūdotājs

variablesSeparatorLabel=:

watchExpressionsScopeLabel=Uzraugāmās izteiksmes

# LOCALIZATION NOTE (emptyStackText): The text that is displayed in the stack
# frames list when there are no frames to display.
emptyStackText=Nav nevienas vienības.

# LOCALIZATION NOTE (loadingText): The text that is displayed in the script
# editor when the laoding process has started but there is no file to display
# yet.
loadingText=Ielādē\u2026

emptyChromeGlobalsFilterText=Filtrēt chrome globālos parametrus (%S)
emptyVariablesFilterText=Filtrēt mainīgos
searchPanelVariable=Filtrēt mainīgos (%S)

noGlobalsText=Nav neviena globālā parametra
noMatchingGlobalsText=Nav neviena atbilstoša globālā parametra

noMatchingStringsText=Nekas nav atrasts

emptyVariablesText=Nav neviena mainīgā ko attēlot.
scopeLabel=%S apgabals
globalScopeLabel=Globāls

watchExpressionsSeparatorLabel=\ →

# LOCALIZATION NOTE (noScriptsText): The text to display in the menulist when
# there are no scripts.
noScriptsText=Skriptu nav

# LOCALIZATION NOTE (noMatchingScriptsText): The text to display in the
# menulist when there are no matching scripts after filtering.
noMatchingScriptsText=Neviens skripts neatbilst

# LOCALIZATION NOTE (breakpointMenuItem): The text for all the elements that
# are displayed in the breakpoints menu item popup.
breakpointMenuItem.setConditional=Konfigurēt apturēšanas punktu ar nosacījumu
emptyBreakpointsText=Nav neviena apturēšanas punkta ko attēlot.
breakpointMenuItem.enableSelf=Aktivizēt apturēšanas punktu
breakpointMenuItem.disableSelf=Deaktivizēt apturēšanas punktu
breakpointMenuItem.deleteSelf=Aizvākt apturēšanas punktu
breakpointMenuItem.enableOthers=Aktivizēt citus
breakpointMenuItem.disableOthers=Deaktivizēt citus
breakpointMenuItem.deleteOthers=Aizvākt citus
breakpointMenuItem.enableAll=Aktivizēt visus apturēšanas punktus
breakpointMenuItem.disableAll=Deaktivizēt visus apturēšanas punktus
breakpointMenuItem.deleteAll=Aizvākt visus apturēšanas punktus
collapsePanes=Sakļaut joslas
expandPanes=Izvērst joslas
emptyFilterText=Meklēt skriptos (%S)
pauseButtonTooltip=Klikšķiniet, lai apturētu (%S)

# LOCALIZATION NOTE (remoteDebuggerWindowTitle): The title displayed for the
# remote debugger window.
remoteDebuggerWindowTitle=Attālinātais atkļūdotājs

# LOCALIZATION NOTE (remoteDebuggerPromptTitle): The title displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerPromptTitle=Attālinātais savienojums

# LOCALIZATION NOTE (remoteDebuggerPromptMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerPromptMessage=Ievadiet adresi un porta numuru (host:port)

# LOCALIZATION NOTE (remoteDebuggerPromptCheck): The message displayed on the
# debugger prompt asking if the prompt should be shown again next time.
remoteDebuggerPromptCheck=Turpmāk vairs nejautāt

# LOCALIZATION NOTE (remoteDebuggerReconnectMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerReconnectMessage=Serveris nav atrasts. Mēģināt vēlreiz? (host:port)

# LOCALIZATION NOTE (remoteDebuggerReconnectMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerConnectionFailedMessage=Norādītajā adresē un porta numurā nevarēja atrast serveri.

# LOCALIZATION NOTE (searchPanelGlobal): This is the text that appears in the
# filter panel popup for the global search operation.
searchPanelGlobal=Meklēt starp visiem failiem (%S)

# LOCALIZATION NOTE (searchPanelToken): This is the text that appears in the
# filter panel popup for the token search operation.
searchPanelToken=Meklēt šajā failā (%S)

# LOCALIZATION NOTE (searchPanelLine): This is the text that appears in the
# filter panel popup for the line search operation.
searchPanelLine=Pāriet uz rindiņu (%S)

# LOCALIZATION NOTE (stepOverTooltip): The label that is displayed on the
# button that steps over a function call.
stepOverTooltip=Pārlēkt pāri (%S)
 
# LOCALIZATION NOTE (stepInTooltip): The label that is displayed on the
# button that steps into a function call.
stepInTooltip=Ielēkt iekšā (%S)

# LOCALIZATION NOTE (stepOutTooltip): The label that is displayed on the
# button that steps out of a function call.
stepOutTooltip=Izlēkt ārā (%S)

addWatchExpressionText=Pievienot uzraugāmo izteiksmi

# LOCALIZATION NOTE (variablesEditableNameTooltip): The text that is displayed
# in the variables list on an item with an editable name.
variablesEditableNameTooltip=Dubultklikšķis, lai rediģētu

# LOCALIZATION NOTE (variablesEditableValueTooltip): The text that is displayed
# in the variables list on an item with an editable name.
variablesEditableValueTooltip=Klikšķiniet, lai mainītu vērtību

# LOCALIZATION NOTE (variablesCloseButtonTooltip): The text that is displayed
# in the variables list on an item with which can be removed.
variablesCloseButtonTooltip=Klikšķiniet, lai aizvāktu

