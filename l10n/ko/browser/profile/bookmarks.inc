#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title 북마크
#define bookmarks_heading 북마크

#define bookmarks_toolbarfolder 북마크 도구 모음
#define bookmarks_toolbarfolder_description 북마크 도구 모음에 표시할 북마크를 이 폴더에 추가하십시오.

# LOCALIZATION NOTE (getting_started):
# link title for http://www.mozilla.com/en-US/firefox/central/
#define getting_started Firefox 시작하기

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Mozilla Firefox

# LOCALIZATION NOTE (firefox_help):
# link title for http://www.mozilla.com/en-US/firefox/help/
#define firefox_help 도움말 및 사용법

# LOCALIZATION NOTE (firefox_customize):
# link title for http://www.mozilla.com/en-US/firefox/customize/
#define firefox_customize 나만의 Firefox 만들기

# LOCALIZATION NOTE (firefox_community):
# link title for http://www.mozilla.com/en-US/firefox/community/
#define firefox_community 사용자 모임 참여하기

# LOCALIZATION NOTE (firefox_about):
# link title for http://www.mozilla.com/en-US/firefox/about/
#define firefox_about 만든 사람들 소개

#unfilter emptyLines
