#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Blêdwizers
#define bookmarks_heading Blêdwizers

#define bookmarks_toolbarfolder Blêdwizersarkbalkemap
#define bookmarks_toolbarfolder_description Foech blêdwizers ta oan dizze map om se te sjen op de blêdwizersarkbalke

# LOCALIZATION NOTE (getting_started):
# link title for http://www.mozilla.com/en-US/firefox/central/
#define getting_started Starte

# LOCALIZATION NOTE (firefox_heading):
# Firefox links folder name
#define firefox_heading Mozilla Firefox

# LOCALIZATION NOTE (firefox_help):
# link title for http://www.mozilla.com/en-US/firefox/help/
#define firefox_help Help en útlis

# LOCALIZATION NOTE (firefox_customize):
# link title for http://www.mozilla.com/en-US/firefox/customize/
#define firefox_customize Firefox oanpasse

# LOCALIZATION NOTE (firefox_community):
# link title for http://www.mozilla.com/en-US/firefox/community/
#define firefox_community Doch mei ús mei

# LOCALIZATION NOTE (firefox_about):
# link title for http://www.mozilla.com/en-US/firefox/about/
#define firefox_about Oer ús

#unfilter emptyLines
