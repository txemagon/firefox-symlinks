# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE These strings are used inside the Debugger
# which is available from the Web Developer sub-menu -> 'Debugger'.
# The correct localization of this file might be to keep it in
# English, or another language commonly spoken among web developers.
# You want to make that choice consistent across the developer tools.
# A good criteria is the language in which you'd find the best
# documentation on web development on the web.

# LOCALIZATION NOTE (confirmTabSwitch): The messages displayed for all the
# title and buttons on the notification shown when a user attempts to open a
# debugger in a new tab while a different tab is already being debugged.
confirmTabSwitch.message=Tá an dífhabhtóir oscailte i gcluaisín eile cheana. Má théann tú ar aghaidh, dúnfar an ceann eile.
confirmTabSwitch.buttonSwitch=Téigh go dtí an cluaisín dífhabhtaithe
confirmTabSwitch.buttonSwitch.accessKey=S
confirmTabSwitch.buttonOpen=Oscail mar sin féin
confirmTabSwitch.buttonOpen.accessKey=O

# LOCALIZATION NOTE (open.commandkey): The key used to open the debugger in
# combination to e.g. ctrl + shift
open.commandkey=S

# LOCALIZATION NOTE (debuggerMenu.accesskey): The access key used to open the
# debugger.
debuggerMenu.accesskey=D

# LOCALIZATION NOTE (chromeDebuggerWindowTitle): The title displayed for the
# chrome (browser) debugger window.
chromeDebuggerWindowTitle=Dífhabhtóir an Bhrabhsálaí

# LOCALIZATION NOTE (remoteDebuggerWindowTitle): The title displayed for the
# remote debugger window.
remoteDebuggerWindowTitle=Dífhabhtóir i gCéin

# LOCALIZATION NOTE (remoteDebuggerPromptTitle): The title displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerPromptTitle=Ceangal Cianda

# LOCALIZATION NOTE (remoteDebuggerPromptMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerPromptMessage=Cuir óstainm agus port isteach (óstainm:port)

# LOCALIZATION NOTE (remoteDebuggerPromptCheck): The message displayed on the
# debugger prompt asking if the prompt should be shown again next time.
remoteDebuggerPromptCheck=Ná fiafraigh díom arís

# LOCALIZATION NOTE (remoteDebuggerReconnectMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerReconnectMessage=Freastalaí gan aimsiú. Bain triail eile as? (óstainm:port)

# LOCALIZATION NOTE (remoteDebuggerReconnectMessage): The message displayed on the
# debugger prompt asking for the remote host and port to connect to.
remoteDebuggerConnectionFailedMessage=Níorbh fhéidir freastalaí a aimsiú ag an óstainm agus port a bhí sonraithe.

# LOCALIZATION NOTE (collapsePanes): This is the tooltip for the button
# that collapses the left and right panes in the debugger UI.
collapsePanes=Laghdaigh pánaí

# LOCALIZATION NOTE (expandPanes): This is the tooltip for the button
# that expands the left and right panes in the debugger UI.
expandPanes=Leathnaigh pánaí

# LOCALIZATION NOTE (pauseLabel): The label that is displayed on the pause
# button when the debugger is in a running state.
pauseButtonTooltip=Cliceáil chun cur ar shos (%S)

# LOCALIZATION NOTE (resumeLabel): The label that is displayed on the pause
# button when the debugger is in a paused state.
resumeButtonTooltip=Cliceáil chun dul ar aghaidh (%S)

# LOCALIZATION NOTE (stepOverTooltip): The label that is displayed on the
# button that steps over a function call.
stepOverTooltip=Céimnigh Thart (%S)

# LOCALIZATION NOTE (stepInTooltip): The label that is displayed on the
# button that steps into a function call.
stepInTooltip=Céimnigh Isteach (%S)

# LOCALIZATION NOTE (stepOutTooltip): The label that is displayed on the
# button that steps out of a function call.
stepOutTooltip=Céimnigh Amach (%S)

# LOCALIZATION NOTE (emptyStackText): The text that is displayed in the stack
# frames list when there are no frames to display.
emptyStackText=Níl aon chruacha le taispeáint

# LOCALIZATION NOTE (emptyBreakpointsText): The text that is displayed in the
# breakpoints list when there are no breakpoints to display.
emptyBreakpointsText=Níl aon bhrisphointí le taispeáint

# LOCALIZATION NOTE (emptyGlobalsText): The text to display in the menulist
# when there are no chrome globals available.
noGlobalsText=Níl aon athróga comhchoiteanna ann

# LOCALIZATION NOTE (noMatchingScriptsText): The text to display in the
# menulist when there are no matching chrome globals after filtering.
noMatchingGlobalsText=No matching globals

# LOCALIZATION NOTE (noScriptsText): The text to display in the menulist
# when there are no scripts.
noScriptsText=Gan scripteanna

# LOCALIZATION NOTE (noMatchingScriptsText): The text to display in the
# menulist when there are no matching scripts after filtering.
noMatchingScriptsText=Níl aon scripteanna comhoiriúnacha ann

# LOCALIZATION NOTE (noMatchingStringsText): The text to display in the
# global search results when there are no matching strings after filtering.
noMatchingStringsText=Níor aimsíodh aon rud

# LOCALIZATION NOTE (emptyFilterText): This is the text that appears in the
# filter text box when it is empty and the scripts container is selected.
emptyFilterText=Scag scripteanna (%S)

# LOCALIZATION NOTE (emptyChromeGlobalsFilterText): This is the text that
# appears in the filter text box when it is empty and the chrome globals
# container is selected.
emptyChromeGlobalsFilterText=Filter chrome globals (%S)

# LOCALIZATION NOTE (emptyVariablesFilterText): This is the text that
# appears in the filter text box for the variables view container.
emptyVariablesFilterText=Filter variables

# LOCALIZATION NOTE (searchPanelGlobal): This is the text that appears in the
# filter panel popup for the global search operation.
searchPanelGlobal=Cuardaigh i ngach comhad (%S)

# LOCALIZATION NOTE (searchPanelToken): This is the text that appears in the
# filter panel popup for the token search operation.
searchPanelToken=Aimsigh sa chomhad seo (%S)

# LOCALIZATION NOTE (searchPanelLine): This is the text that appears in the
# filter panel popup for the line search operation.
searchPanelLine=Léim go líne (%S)

# LOCALIZATION NOTE (searchPanelVariable): This is the text that appears in the
# filter panel popup for the variables search operation.
searchPanelVariable=Filter variables (%S)

# LOCALIZATION NOTE (breakpointMenuItem): The text for all the elements that
# are displayed in the breakpoints menu item popup.
breakpointMenuItem.setConditional=Cumraigh brisphointe coinníollach
breakpointMenuItem.enableSelf=Cumasaigh an brisphointe
breakpointMenuItem.disableSelf=Díchumasaigh an brisphointe
breakpointMenuItem.deleteSelf=Bain an brisphointe
breakpointMenuItem.enableOthers=Cumasaigh na cinn eile
breakpointMenuItem.disableOthers=Díchumasaigh na cinn eile
breakpointMenuItem.deleteOthers=Bain na cinn eile
breakpointMenuItem.enableAll=Cumasaigh gach brisphointe
breakpointMenuItem.disableAll=Díchumasaigh gach brisphointe
breakpointMenuItem.deleteAll=Bain gach brisphointe

# LOCALIZATION NOTE (loadingText): The text that is displayed in the script
# editor when the loading process has started but there is no file to display
# yet.
loadingText=Á Luchtú…

# LOCALIZATION NOTE (emptyStackText): The text that is displayed in the watch
# expressions list to add a new item.
addWatchExpressionText=Cuir slonn faire leis

# LOCALIZATION NOTE (emptyVariablesText): The text that is displayed in the
# variables pane when there are no variables to display.
emptyVariablesText=Níl aon athróga le taispeáint

# LOCALIZATION NOTE (scopeLabel): The text that is displayed in the variables
# pane as a header for each variable scope (e.g. "Global scope, "With scope",
# etc.).
scopeLabel=Scóp: %S

# LOCALIZATION NOTE (watchExpressionsScopeLabel): The name of the watch
# expressions scope. This text is displayed in the variables pane as a header for
# the watch expressions scope.
watchExpressionsScopeLabel=Sloinn fhaire

# LOCALIZATION NOTE (globalScopeLabel): The name of the global scope. This text
# is added to scopeLabel and displayed in the variables pane as a header for
# the global scope.
globalScopeLabel=Comhchoiteann

# LOCALIZATION NOTE (ToolboxDebugger.label):
# This string is displayed in the title of the tab when the debugger is
# displayed inside the developer tools window and in the Developer Tools Menu.
ToolboxDebugger.label=Dífhabhtóir

# LOCALIZATION NOTE (ToolboxDebugger.tooltip):
# This string is displayed in the tooltip of the tab when the debugger is
# displayed inside the developer tools window..
ToolboxDebugger.tooltip=JavaScript Debugger

# LOCALIZATION NOTE (variablesEditableNameTooltip): The text that is displayed
# in the variables list on an item with an editable name.
variablesEditableNameTooltip=Double click to edit

# LOCALIZATION NOTE (variablesEditableValueTooltip): The text that is displayed
# in the variables list on an item with an editable name.
variablesEditableValueTooltip=Click to change value

# LOCALIZATION NOTE (variablesCloseButtonTooltip): The text that is displayed
# in the variables list on an item with which can be removed.
variablesCloseButtonTooltip=Click to remove

# LOCALIZATION NOTE (variablesSeparatorLabel): The text that is displayed
# in the variables list as a separator between the name and value.
variablesSeparatorLabel=:

# LOCALIZATION NOTE (watchExpressionsSeparatorLabel): The text that is displayed
# in the watch expressions list as a separator between the code and evaluation.
watchExpressionsSeparatorLabel= →
