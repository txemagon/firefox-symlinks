#filter emptyLines
# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Alama


#define bookmarks_heading Alama Buk


#define bookmarks_toolbarfolder Ket alama me Boc pi bar me Gitic


#define bookmarks_toolbarfolder_description Med alama me pot buk i boc wek i nen ni guyaro i bar me Gitic pi Alama me Pot Buk


# LOCALIZATION NOTE (getting_started):

# link title for http://en-US.www.mozilla.com/en-US/firefox/central/

#define getting_started Tye kacake


# LOCALIZATION NOTE (firefox_heading):

# Firefox links folder name

#define firefox_heading Mozilla Firefox


# LOCALIZATION NOTE (firefox_help):

# link title for http://en-US.www.mozilla.com/en-US/firefox/help/

#define firefox_help Kony ki Tic cing


# LOCALIZATION NOTE (firefox_customize):

# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/

#define firefox_customize Yar Firefox


# LOCALIZATION NOTE (firefox_community):

# link title for http://en-US.www.mozilla.com/en-US/firefox/community/

#define firefox_community Bed Iye


# LOCALIZATION NOTE (firefox_about):

# link title for http://en-US.www.mozilla.com/en-US/about/

#define firefox_about Lok ikom Wa


#unfilter emptyLines