#filter emptyLines
# LOCALIZATION NOTE: The 'en-US' strings in the URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're 
# live.

#define bookmarks_title Izimpawu Zokubekisa


#define bookmarks_heading Izimpawu Zokubekisa


#define bookmarks_toolbarfolder Ifolda Yebha Yamathuluzi Okokuphawula


#define bookmarks_toolbarfolder_description Yengeza uphawu lokubekisa kule folda ukuze ulubone likhonjiwe kwi-bar yamathuluzi lophawu lokubekisa


# LOCALIZATION NOTE (getting_started):

# link title for http://en-US.www.mozilla.com/en-US/firefox/central/

#define getting_started Ngokuqala


# LOCALIZATION NOTE (firefox_heading):

# Firefox links folder name

#define firefox_heading Mozilla Firefox


# LOCALIZATION NOTE (firefox_help):

# link title for http://en-US.www.mozilla.com/en-US/firefox/help/

#define firefox_help Usizo kanye nokufundisa


# LOCALIZATION NOTE (firefox_customize):

# link title for http://en-US.www.mozilla.com/en-US/firefox/customize/

#define firefox_customize yenza i-Firefox ngokwezifiso


# LOCALIZATION NOTE (firefox_community):

# link title for http://en-US.www.mozilla.com/en-US/firefox/community/

#define firefox_community Zibandakanye


# LOCALIZATION NOTE (firefox_about):

# link title for http://en-US.www.mozilla.com/en-US/about/

#define firefox_about Mayelana nathi


#unfilter emptyLines