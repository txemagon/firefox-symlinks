CHANNEL			= release
MOZ_WANT_UNIT_TESTS	= 1
MOZ_BUILD_UNOFFICIAL	= 0
MOZ_ENABLE_BREAKPAD 	= 1

MOZILLA_REPO = http://hg.mozilla.org/releases/mozilla-release
L10N_REPO = http://hg.mozilla.org/releases/l10n/mozilla-release
