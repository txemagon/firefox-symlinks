/* -*- Mode: C++; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is mozilla.org code.
 *
 * The Initial Developer of the Original Code is
 * Netscape Communications Corporation.
 * Portions created by the Initial Developer are Copyright (C) 1999
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Original Author: Mike Pinkerton (pinkerton@netscape.com)
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

WIDGET_ATOM(accesskey)
WIDGET_ATOM(autocheck)
WIDGET_ATOM(checkbox)
WIDGET_ATOM(checked)
WIDGET_ATOM2(_class, "class")
WIDGET_ATOM(collapsed)
WIDGET_ATOM(command)
WIDGET_ATOM(customizing)
WIDGET_ATOM(disabled)
WIDGET_ATOM2(_false, "false")
WIDGET_ATOM(hidden)
WIDGET_ATOM(id)
WIDGET_ATOM(image)
WIDGET_ATOM(key)
WIDGET_ATOM(keycode)
WIDGET_ATOM(label)
WIDGET_ATOM(menu)
WIDGET_ATOM2(menuactive, "_moz-menuactive")
WIDGET_ATOM(menuitem)
WIDGET_ATOM(menupopup)
WIDGET_ATOM(menuseparator)
WIDGET_ATOM(modifiers)
WIDGET_ATOM(name)
WIDGET_ATOM(open)
WIDGET_ATOM(radio)
WIDGET_ATOM(toolbarspring)
WIDGET_ATOM2(_true, "true")
WIDGET_ATOM2(_ubuntu_state, "_ubuntu-state")
WIDGET_ATOM(type)
