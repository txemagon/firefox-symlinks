#include "nsXREAppData.h"
             static const nsXREAppData sAppData = {
                 sizeof(nsXREAppData),
                 NULL, // directory
                 "Mozilla",
                 "Firefox",
                 "20.0",
                 "20130430173359",
                 "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}",
                 NULL, // copyright
                 NS_XRE_ENABLE_EXTENSION_MANAGER | NS_XRE_ENABLE_PROFILE_MIGRATOR,
                 NULL, // xreDirectory
                 "20.0",
                 "20.0",
                 "https://crash-reports.mozilla.com/submit?id={ec8030f7-c20a-464f-9b0e-13a3a9e97384}&version=20.0&buildid=20130430173359",
                 NULL
             };
