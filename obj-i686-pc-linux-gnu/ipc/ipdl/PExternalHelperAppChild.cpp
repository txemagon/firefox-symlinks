//
// Automatically generated by ipdlc.
// Edit at your own risk
//


#include "mozilla/dom/PExternalHelperAppChild.h"

#include "mozilla/dom/PContentChild.h"

typedef IPC::Message Message;
typedef mozilla::ipc::RPCChannel Channel;
typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
typedef base::ProcessHandle ProcessHandle;
typedef mozilla::ipc::AsyncChannel AsyncChannel;
typedef mozilla::ipc::SharedMemory SharedMemory;
typedef mozilla::ipc::Trigger Trigger;
typedef mozilla::ipc::ActorHandle ActorHandle;
typedef mozilla::ipc::FileDescriptor FileDescriptor;
typedef mozilla::ipc::Shmem Shmem;
using mozilla::dom::PContentChild;

namespace mozilla {
namespace dom {


bool
PExternalHelperAppChild::Recv__delete__()
{
    return true;
}

void
PExternalHelperAppChild::ActorDestroy(ActorDestroyReason why)
{
}

PExternalHelperAppChild::PExternalHelperAppChild() :
    mId(0),
    mState(PExternalHelperApp::__Dead)
{
    MOZ_COUNT_CTOR(PExternalHelperAppChild);
}

PExternalHelperAppChild::~PExternalHelperAppChild()
{
    MOZ_COUNT_DTOR(PExternalHelperAppChild);
}

PContentChild*
PExternalHelperAppChild::Manager() const
{
    return static_cast<PContentChild*>(mManager);
}

PExternalHelperApp::State
PExternalHelperAppChild::state()
{
    return mState;
}

bool
PExternalHelperAppChild::SendOnStartRequest(const nsCString& entityID)
{
    PExternalHelperApp::Msg_OnStartRequest* __msg = new PExternalHelperApp::Msg_OnStartRequest();

    Write(entityID, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PExternalHelperAppChild] Sending ", stderr);
    }
    if ((!(PExternalHelperApp::Transition(mState, Trigger(Trigger::Recv, PExternalHelperApp::Msg_OnStartRequest__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

bool
PExternalHelperAppChild::SendOnDataAvailable(
        const nsCString& data,
        const uint64_t& offset,
        const uint32_t& count)
{
    PExternalHelperApp::Msg_OnDataAvailable* __msg = new PExternalHelperApp::Msg_OnDataAvailable();

    Write(data, __msg);
    Write(offset, __msg);
    Write(count, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PExternalHelperAppChild] Sending ", stderr);
    }
    if ((!(PExternalHelperApp::Transition(mState, Trigger(Trigger::Recv, PExternalHelperApp::Msg_OnDataAvailable__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

bool
PExternalHelperAppChild::SendOnStopRequest(const nsresult& code)
{
    PExternalHelperApp::Msg_OnStopRequest* __msg = new PExternalHelperApp::Msg_OnStopRequest();

    Write(code, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PExternalHelperAppChild] Sending ", stderr);
    }
    if ((!(PExternalHelperApp::Transition(mState, Trigger(Trigger::Recv, PExternalHelperApp::Msg_OnStopRequest__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

int32_t
PExternalHelperAppChild::Register(ChannelListener* aRouted)
{
    return (mManager)->Register(aRouted);
}

int32_t
PExternalHelperAppChild::RegisterID(
        ChannelListener* aRouted,
        int32_t aId)
{
    return (mManager)->RegisterID(aRouted, aId);
}

ChannelListener*
PExternalHelperAppChild::Lookup(int32_t aId)
{
    return (mManager)->Lookup(aId);
}

void
PExternalHelperAppChild::Unregister(int32_t aId)
{
    return (mManager)->Unregister(aId);
}

void
PExternalHelperAppChild::RemoveManagee(
        int32_t aProtocolId,
        ChannelListener* aListener)
{
    NS_RUNTIMEABORT("unreached");
    return;
}

Shmem::SharedMemory*
PExternalHelperAppChild::CreateSharedMemory(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        bool aUnsafe,
        Shmem::id_t* aId)
{
    return (mManager)->CreateSharedMemory(aSize, aType, aUnsafe, aId);
}

bool
PExternalHelperAppChild::AdoptSharedMemory(
        Shmem::SharedMemory* segment,
        Shmem::id_t* aId)
{
    return (mManager)->AdoptSharedMemory(segment, aId);
}

Shmem::SharedMemory*
PExternalHelperAppChild::LookupSharedMemory(Shmem::id_t aId)
{
    return (mManager)->LookupSharedMemory(aId);
}

bool
PExternalHelperAppChild::IsTrackingSharedMemory(Shmem::SharedMemory* segment)
{
    return (mManager)->IsTrackingSharedMemory(segment);
}

bool
PExternalHelperAppChild::DestroySharedMemory(Shmem& shmem)
{
    return (mManager)->DestroySharedMemory(shmem);
}

ProcessHandle
PExternalHelperAppChild::OtherProcess() const
{
    return (mManager)->OtherProcess();
}

AsyncChannel*
PExternalHelperAppChild::GetIPCChannel()
{
    return mChannel;
}

PExternalHelperAppChild::Result
PExternalHelperAppChild::OnMessageReceived(const Message& __msg)
{
    switch ((__msg).type()) {
    case PExternalHelperApp::Msg_Cancel__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PExternalHelperApp::Msg_Cancel");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PExternalHelperApp::Msg_Cancel*>((&(__msg))))->Log("[PExternalHelperAppChild] Received ", stderr);
            }

            void* __iter = 0;
            nsresult aStatus;

            if ((!(Read((&(aStatus)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PExternalHelperApp::Transition(mState, Trigger(Trigger::Send, PExternalHelperApp::Msg_Cancel__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvCancel(aStatus)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for Cancel returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PExternalHelperApp::Msg___delete____ID:
        {
            (const_cast<Message&>(__msg)).set_name("PExternalHelperApp::Msg___delete__");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PExternalHelperApp::Msg___delete__*>((&(__msg))))->Log("[PExternalHelperAppChild] Received ", stderr);
            }

            void* __iter = 0;
            PExternalHelperAppChild* actor;

            if ((!(Read((&(actor)), (&(__msg)), (&(__iter)), false)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PExternalHelperApp::Transition(mState, Trigger(Trigger::Send, PExternalHelperApp::Msg___delete____ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(Recv__delete__()))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for __delete__ returned error code");
                return MsgProcessingError;
            }

            (actor)->DestroySubtree(Deletion);
            (actor)->DeallocSubtree();
            ((actor)->mManager)->RemoveManagee(PExternalHelperAppMsgStart, actor);


            return MsgProcessed;
        }
    default:
        {
            return MsgNotKnown;
        }
    }
}

PExternalHelperAppChild::Result
PExternalHelperAppChild::OnMessageReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

PExternalHelperAppChild::Result
PExternalHelperAppChild::OnCallReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

void
PExternalHelperAppChild::OnProcessingError(Result code)
{
    NS_RUNTIMEABORT("`OnProcessingError' called on non-toplevel actor");
}

int32_t
PExternalHelperAppChild::GetProtocolTypeId()
{
    return PExternalHelperAppMsgStart;
}

bool
PExternalHelperAppChild::OnReplyTimeout()
{
    NS_RUNTIMEABORT("`OnReplyTimeout' called on non-toplevel actor");
    return false;
}

void
PExternalHelperAppChild::OnChannelClose()
{
    NS_RUNTIMEABORT("`OnClose' called on non-toplevel actor");
}

void
PExternalHelperAppChild::OnChannelError()
{
    NS_RUNTIMEABORT("`OnError' called on non-toplevel actor");
}

void
PExternalHelperAppChild::OnChannelConnected(int32_t pid)
{
    NS_RUNTIMEABORT("'OnConnected' called on non-toplevel actor");
}

bool
PExternalHelperAppChild::AllocShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, false, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PExternalHelperAppChild::AllocUnsafeShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, true, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PExternalHelperAppChild::AdoptShmem(
        Shmem& aMem,
        Shmem* aOutMem)
{
    Shmem::SharedMemory* rawmem = (aMem).Segment(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    if (((!(rawmem))) || (IsTrackingSharedMemory(rawmem))) {
        NS_RUNTIMEABORT("bad Shmem");
    }

    Shmem::id_t aId;
    if ((!(AdoptSharedMemory(rawmem, (&(aId)))))) {
        return false;
    }

    (*(aOutMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), rawmem, aId);
    return true;
}

bool
PExternalHelperAppChild::DeallocShmem(Shmem& aMem)
{
    bool ok = DestroySharedMemory(aMem);
    (aMem).forget(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    return ok;
}

void
PExternalHelperAppChild::FatalError(const char* const msg) const
{
    // Virtual method to prevent inlining.
    // This give us better error reporting.
    // See bug 589371

    NS_ERROR("IPDL error:");
    NS_ERROR(msg);
    mozilla::ipc::ProtocolErrorBreakpoint(msg);

    NS_RUNTIMEABORT("[PExternalHelperAppChild] abort()ing as a result");
}

void
PExternalHelperAppChild::DestroySubtree(ActorDestroyReason why)
{
    // Unregister from our manager.
    Unregister(mId);
    mId = 1;

    // Finally, destroy "us".
    ActorDestroy(why);
}

void
PExternalHelperAppChild::DeallocSubtree()
{
}

void
PExternalHelperAppChild::Write(
        PExternalHelperAppChild* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PExternalHelperAppChild::Read(
        PExternalHelperAppChild** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PExternalHelperApp");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PExternalHelperApp");
        return false;
    }

    if ((PExternalHelperAppMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PExternalHelperApp has different type");
        return false;
    }

    (*(__v)) = static_cast<PExternalHelperAppChild*>(listener);
    return true;
}



} // namespace dom
} // namespace mozilla
