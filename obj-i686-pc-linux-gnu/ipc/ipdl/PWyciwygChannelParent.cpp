//
// Automatically generated by ipdlc.
// Edit at your own risk
//


#include "mozilla/net/PWyciwygChannelParent.h"

#include "mozilla/net/PNeckoParent.h"
#include "mozilla/dom/PBrowserParent.h"

typedef IPC::Message Message;
typedef mozilla::ipc::RPCChannel Channel;
typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
typedef base::ProcessHandle ProcessHandle;
typedef mozilla::ipc::AsyncChannel AsyncChannel;
typedef mozilla::ipc::SharedMemory SharedMemory;
typedef mozilla::ipc::Trigger Trigger;
typedef mozilla::ipc::ActorHandle ActorHandle;
typedef mozilla::ipc::FileDescriptor FileDescriptor;
typedef mozilla::ipc::GenericURIParams GenericURIParams;
typedef mozilla::ipc::JARURIParams JARURIParams;
typedef mozilla::ipc::OptionalURIParams OptionalURIParams;
typedef IPC::SerializedLoadContext SerializedLoadContext;
typedef mozilla::ipc::Shmem Shmem;
typedef mozilla::ipc::SimpleURIParams SimpleURIParams;
typedef mozilla::ipc::StandardURLParams StandardURLParams;
typedef mozilla::ipc::StandardURLSegment StandardURLSegment;
typedef mozilla::ipc::URIParams URIParams;
typedef mozilla::void_t void_t;
using mozilla::net::PNeckoParent;
using mozilla::dom::PBrowserParent;

namespace mozilla {
namespace net {


bool
PWyciwygChannelParent::Recv__delete__()
{
    return true;
}

void
PWyciwygChannelParent::ActorDestroy(ActorDestroyReason why)
{
}

PWyciwygChannelParent::PWyciwygChannelParent() :
    mId(0),
    mState(PWyciwygChannel::__Dead)
{
    MOZ_COUNT_CTOR(PWyciwygChannelParent);
}

PWyciwygChannelParent::~PWyciwygChannelParent()
{
    MOZ_COUNT_DTOR(PWyciwygChannelParent);
}

PNeckoParent*
PWyciwygChannelParent::Manager() const
{
    return static_cast<PNeckoParent*>(mManager);
}

PWyciwygChannel::State
PWyciwygChannelParent::state()
{
    return mState;
}

bool
PWyciwygChannelParent::SendOnStartRequest(
        const nsresult& statusCode,
        const int64_t& contentLength,
        const int32_t& source,
        const nsCString& charset,
        const nsCString& securityInfo)
{
    PWyciwygChannel::Msg_OnStartRequest* __msg = new PWyciwygChannel::Msg_OnStartRequest();

    Write(statusCode, __msg);
    Write(contentLength, __msg);
    Write(source, __msg);
    Write(charset, __msg);
    Write(securityInfo, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PWyciwygChannelParent] Sending ", stderr);
    }
    if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Send, PWyciwygChannel::Msg_OnStartRequest__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

bool
PWyciwygChannelParent::SendOnDataAvailable(
        const nsCString& data,
        const uint64_t& offset)
{
    PWyciwygChannel::Msg_OnDataAvailable* __msg = new PWyciwygChannel::Msg_OnDataAvailable();

    Write(data, __msg);
    Write(offset, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PWyciwygChannelParent] Sending ", stderr);
    }
    if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Send, PWyciwygChannel::Msg_OnDataAvailable__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

bool
PWyciwygChannelParent::SendOnStopRequest(const nsresult& statusCode)
{
    PWyciwygChannel::Msg_OnStopRequest* __msg = new PWyciwygChannel::Msg_OnStopRequest();

    Write(statusCode, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PWyciwygChannelParent] Sending ", stderr);
    }
    if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Send, PWyciwygChannel::Msg_OnStopRequest__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

bool
PWyciwygChannelParent::SendCancelEarly(const nsresult& statusCode)
{
    PWyciwygChannel::Msg_CancelEarly* __msg = new PWyciwygChannel::Msg_CancelEarly();

    Write(statusCode, __msg);

    (__msg)->set_routing_id(mId);



    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PWyciwygChannelParent] Sending ", stderr);
    }
    if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Send, PWyciwygChannel::Msg_CancelEarly__ID), (&(mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = (mChannel)->Send(__msg);
    return __sendok;
}

int32_t
PWyciwygChannelParent::Register(ChannelListener* aRouted)
{
    return (mManager)->Register(aRouted);
}

int32_t
PWyciwygChannelParent::RegisterID(
        ChannelListener* aRouted,
        int32_t aId)
{
    return (mManager)->RegisterID(aRouted, aId);
}

ChannelListener*
PWyciwygChannelParent::Lookup(int32_t aId)
{
    return (mManager)->Lookup(aId);
}

void
PWyciwygChannelParent::Unregister(int32_t aId)
{
    return (mManager)->Unregister(aId);
}

void
PWyciwygChannelParent::RemoveManagee(
        int32_t aProtocolId,
        ChannelListener* aListener)
{
    NS_RUNTIMEABORT("unreached");
    return;
}

Shmem::SharedMemory*
PWyciwygChannelParent::CreateSharedMemory(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        bool aUnsafe,
        Shmem::id_t* aId)
{
    return (mManager)->CreateSharedMemory(aSize, aType, aUnsafe, aId);
}

bool
PWyciwygChannelParent::AdoptSharedMemory(
        Shmem::SharedMemory* segment,
        Shmem::id_t* aId)
{
    return (mManager)->AdoptSharedMemory(segment, aId);
}

Shmem::SharedMemory*
PWyciwygChannelParent::LookupSharedMemory(Shmem::id_t aId)
{
    return (mManager)->LookupSharedMemory(aId);
}

bool
PWyciwygChannelParent::IsTrackingSharedMemory(Shmem::SharedMemory* segment)
{
    return (mManager)->IsTrackingSharedMemory(segment);
}

bool
PWyciwygChannelParent::DestroySharedMemory(Shmem& shmem)
{
    return (mManager)->DestroySharedMemory(shmem);
}

ProcessHandle
PWyciwygChannelParent::OtherProcess() const
{
    return (mManager)->OtherProcess();
}

AsyncChannel*
PWyciwygChannelParent::GetIPCChannel()
{
    return mChannel;
}

PWyciwygChannelParent::Result
PWyciwygChannelParent::OnMessageReceived(const Message& __msg)
{
    switch ((__msg).type()) {
    case PWyciwygChannel::Msg___delete____ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg___delete__");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg___delete__*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            PWyciwygChannelParent* actor;

            if ((!(Read((&(actor)), (&(__msg)), (&(__iter)), false)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg___delete____ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(Recv__delete__()))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for __delete__ returned error code");
                return MsgProcessingError;
            }

            (actor)->DestroySubtree(Deletion);
            (actor)->DeallocSubtree();
            ((actor)->mManager)->RemoveManagee(PWyciwygChannelMsgStart, actor);


            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_Init__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_Init");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_Init*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            URIParams uri;

            if ((!(Read((&(uri)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_Init__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvInit(uri)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for Init returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_AsyncOpen__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_AsyncOpen");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_AsyncOpen*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            URIParams originalURI;
            uint32_t loadFlags;
            SerializedLoadContext loadContext;
            PBrowserParent* browser;

            if ((!(Read((&(originalURI)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(loadFlags)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(loadContext)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(browser)), (&(__msg)), (&(__iter)), false)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_AsyncOpen__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvAsyncOpen(originalURI, loadFlags, loadContext, browser)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for AsyncOpen returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_WriteToCacheEntry__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_WriteToCacheEntry");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_WriteToCacheEntry*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            nsString data;

            if ((!(Read((&(data)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_WriteToCacheEntry__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvWriteToCacheEntry(data)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for WriteToCacheEntry returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_CloseCacheEntry__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_CloseCacheEntry");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_CloseCacheEntry*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            nsresult reason;

            if ((!(Read((&(reason)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_CloseCacheEntry__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvCloseCacheEntry(reason)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for CloseCacheEntry returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_SetCharsetAndSource__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_SetCharsetAndSource");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_SetCharsetAndSource*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            int32_t source;
            nsCString charset;

            if ((!(Read((&(source)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(charset)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_SetCharsetAndSource__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvSetCharsetAndSource(source, charset)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for SetCharsetAndSource returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_SetSecurityInfo__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_SetSecurityInfo");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_SetSecurityInfo*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            nsCString securityInfo;

            if ((!(Read((&(securityInfo)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_SetSecurityInfo__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvSetSecurityInfo(securityInfo)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for SetSecurityInfo returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PWyciwygChannel::Msg_Cancel__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PWyciwygChannel::Msg_Cancel");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PWyciwygChannel::Msg_Cancel*>((&(__msg))))->Log("[PWyciwygChannelParent] Received ", stderr);
            }

            void* __iter = 0;
            nsresult status;

            if ((!(Read((&(status)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PWyciwygChannel::Transition(mState, Trigger(Trigger::Recv, PWyciwygChannel::Msg_Cancel__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvCancel(status)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for Cancel returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    default:
        {
            return MsgNotKnown;
        }
    }
}

PWyciwygChannelParent::Result
PWyciwygChannelParent::OnMessageReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

PWyciwygChannelParent::Result
PWyciwygChannelParent::OnCallReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

void
PWyciwygChannelParent::OnProcessingError(Result code)
{
    NS_RUNTIMEABORT("`OnProcessingError' called on non-toplevel actor");
}

int32_t
PWyciwygChannelParent::GetProtocolTypeId()
{
    return PWyciwygChannelMsgStart;
}

bool
PWyciwygChannelParent::OnReplyTimeout()
{
    NS_RUNTIMEABORT("`OnReplyTimeout' called on non-toplevel actor");
    return false;
}

void
PWyciwygChannelParent::OnChannelClose()
{
    NS_RUNTIMEABORT("`OnClose' called on non-toplevel actor");
}

void
PWyciwygChannelParent::OnChannelError()
{
    NS_RUNTIMEABORT("`OnError' called on non-toplevel actor");
}

void
PWyciwygChannelParent::OnChannelConnected(int32_t pid)
{
    NS_RUNTIMEABORT("'OnConnected' called on non-toplevel actor");
}

bool
PWyciwygChannelParent::AllocShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, false, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PWyciwygChannelParent::AllocUnsafeShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, true, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PWyciwygChannelParent::AdoptShmem(
        Shmem& aMem,
        Shmem* aOutMem)
{
    Shmem::SharedMemory* rawmem = (aMem).Segment(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    if (((!(rawmem))) || (IsTrackingSharedMemory(rawmem))) {
        NS_RUNTIMEABORT("bad Shmem");
    }

    Shmem::id_t aId;
    if ((!(AdoptSharedMemory(rawmem, (&(aId)))))) {
        return false;
    }

    (*(aOutMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), rawmem, aId);
    return true;
}

bool
PWyciwygChannelParent::DeallocShmem(Shmem& aMem)
{
    bool ok = DestroySharedMemory(aMem);
    (aMem).forget(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    return ok;
}

void
PWyciwygChannelParent::FatalError(const char* const msg) const
{
    // Virtual method to prevent inlining.
    // This give us better error reporting.
    // See bug 589371

    NS_ERROR("IPDL error:");
    NS_ERROR(msg);
    mozilla::ipc::ProtocolErrorBreakpoint(msg);

    NS_ERROR("[PWyciwygChannelParent] killing child side as a result");

    if ((!(base::KillProcess(OtherProcess(), base::PROCESS_END_KILLED_BY_USER, false)))) {
        NS_ERROR("  may have failed to kill child!");
    }
}

void
PWyciwygChannelParent::DestroySubtree(ActorDestroyReason why)
{
    // Unregister from our manager.
    Unregister(mId);
    mId = 1;

    // Finally, destroy "us".
    ActorDestroy(why);
}

void
PWyciwygChannelParent::DeallocSubtree()
{
}

void
PWyciwygChannelParent::Write(
        PBrowserParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PWyciwygChannelParent::Read(
        PBrowserParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PWyciwygChannel");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PBrowser");
        return false;
    }

    if ((PBrowserMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PBrowser has different type");
        return false;
    }

    (*(__v)) = static_cast<PBrowserParent*>(listener);
    return true;
}

void
PWyciwygChannelParent::Write(
        PWyciwygChannelParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PWyciwygChannelParent::Read(
        PWyciwygChannelParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PWyciwygChannel");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PWyciwygChannel");
        return false;
    }

    if ((PWyciwygChannelMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PWyciwygChannel has different type");
        return false;
    }

    (*(__v)) = static_cast<PWyciwygChannelParent*>(listener);
    return true;
}

void
PWyciwygChannelParent::Write(
        const StandardURLParams& __v,
        Message* __msg)
{
    Write((__v).urlType(), __msg);
    Write((__v).port(), __msg);
    Write((__v).defaultPort(), __msg);
    Write((__v).spec(), __msg);
    Write((__v).scheme(), __msg);
    Write((__v).authority(), __msg);
    Write((__v).username(), __msg);
    Write((__v).password(), __msg);
    Write((__v).host(), __msg);
    Write((__v).path(), __msg);
    Write((__v).filePath(), __msg);
    Write((__v).directory(), __msg);
    Write((__v).baseName(), __msg);
    Write((__v).extension(), __msg);
    Write((__v).query(), __msg);
    Write((__v).ref(), __msg);
    Write((__v).originCharset(), __msg);
    Write((__v).isMutable(), __msg);
    Write((__v).supportsFileURL(), __msg);
    Write((__v).hostEncoding(), __msg);
}

bool
PWyciwygChannelParent::Read(
        StandardURLParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->urlType())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->port())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->defaultPort())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->spec())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->scheme())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->authority())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->username())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->password())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->host())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->path())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->filePath())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->directory())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->baseName())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->extension())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->query())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->ref())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->originCharset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->isMutable())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->supportsFileURL())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->hostEncoding())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PWyciwygChannelParent::Write(
        const JARURIParams& __v,
        Message* __msg)
{
    Write((__v).jarFile(), __msg);
    Write((__v).jarEntry(), __msg);
    Write((__v).charset(), __msg);
}

bool
PWyciwygChannelParent::Read(
        JARURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->jarFile())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->jarEntry())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->charset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PWyciwygChannelParent::Write(
        const GenericURIParams& __v,
        Message* __msg)
{
    Write((__v).spec(), __msg);
    Write((__v).charset(), __msg);
}

bool
PWyciwygChannelParent::Read(
        GenericURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->spec())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->charset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PWyciwygChannelParent::Write(
        const StandardURLSegment& __v,
        Message* __msg)
{
    Write((__v).position(), __msg);
    Write((__v).length(), __msg);
}

bool
PWyciwygChannelParent::Read(
        StandardURLSegment* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->position())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->length())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PWyciwygChannelParent::Write(
        const URIParams& __v,
        Message* __msg)
{
    typedef URIParams __type;
    Write(int((__v).type()), __msg);

    switch ((__v).type()) {
    case __type::TSimpleURIParams:
        {
            Write((__v).get_SimpleURIParams(), __msg);
            return;
        }
    case __type::TStandardURLParams:
        {
            Write((__v).get_StandardURLParams(), __msg);
            return;
        }
    case __type::TJARURIParams:
        {
            Write((__v).get_JARURIParams(), __msg);
            return;
        }
    case __type::TGenericURIParams:
        {
            Write((__v).get_GenericURIParams(), __msg);
            return;
        }
    default:
        {
            NS_RUNTIMEABORT("unknown union type");
            return;
        }
    }
}

bool
PWyciwygChannelParent::Read(
        URIParams* __v,
        const Message* __msg,
        void** __iter)
{
    typedef URIParams __type;
    int type;
    if ((!(Read((&(type)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    switch (type) {
    case __type::TSimpleURIParams:
        {
            SimpleURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_SimpleURIParams())), __msg, __iter);
        }
    case __type::TStandardURLParams:
        {
            StandardURLParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_StandardURLParams())), __msg, __iter);
        }
    case __type::TJARURIParams:
        {
            JARURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_JARURIParams())), __msg, __iter);
        }
    case __type::TGenericURIParams:
        {
            GenericURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GenericURIParams())), __msg, __iter);
        }
    default:
        {
            mozilla::ipc::ProtocolErrorBreakpoint("unknown union type");
            return false;
        }
    }
}

void
PWyciwygChannelParent::Write(
        const SimpleURIParams& __v,
        Message* __msg)
{
    Write((__v).scheme(), __msg);
    Write((__v).path(), __msg);
    Write((__v).ref(), __msg);
    Write((__v).isMutable(), __msg);
}

bool
PWyciwygChannelParent::Read(
        SimpleURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->scheme())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->path())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->ref())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->isMutable())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}



} // namespace net
} // namespace mozilla
