//
// Automatically generated by ipdlc.
// Edit at your own risk
//


#include "mozilla/net/PCookieServiceParent.h"

#include "mozilla/net/PNeckoParent.h"
#include "mozilla/dom/PBrowserParent.h"

typedef IPC::Message Message;
typedef mozilla::ipc::RPCChannel Channel;
typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
typedef base::ProcessHandle ProcessHandle;
typedef mozilla::ipc::AsyncChannel AsyncChannel;
typedef mozilla::ipc::SharedMemory SharedMemory;
typedef mozilla::ipc::Trigger Trigger;
typedef mozilla::ipc::ActorHandle ActorHandle;
typedef mozilla::ipc::FileDescriptor FileDescriptor;
typedef mozilla::ipc::GenericURIParams GenericURIParams;
typedef mozilla::ipc::JARURIParams JARURIParams;
typedef mozilla::ipc::OptionalURIParams OptionalURIParams;
typedef IPC::SerializedLoadContext SerializedLoadContext;
typedef mozilla::ipc::Shmem Shmem;
typedef mozilla::ipc::SimpleURIParams SimpleURIParams;
typedef mozilla::ipc::StandardURLParams StandardURLParams;
typedef mozilla::ipc::StandardURLSegment StandardURLSegment;
typedef mozilla::ipc::URIParams URIParams;
typedef mozilla::void_t void_t;
using mozilla::net::PNeckoParent;
using mozilla::dom::PBrowserParent;

namespace mozilla {
namespace net {


bool
PCookieServiceParent::Recv__delete__()
{
    return true;
}

void
PCookieServiceParent::ActorDestroy(ActorDestroyReason why)
{
}

PCookieServiceParent::PCookieServiceParent() :
    mId(0),
    mState(PCookieService::__Dead)
{
    MOZ_COUNT_CTOR(PCookieServiceParent);
}

PCookieServiceParent::~PCookieServiceParent()
{
    MOZ_COUNT_DTOR(PCookieServiceParent);
}

PNeckoParent*
PCookieServiceParent::Manager() const
{
    return static_cast<PNeckoParent*>(mManager);
}

PCookieService::State
PCookieServiceParent::state()
{
    return mState;
}

int32_t
PCookieServiceParent::Register(ChannelListener* aRouted)
{
    return (mManager)->Register(aRouted);
}

int32_t
PCookieServiceParent::RegisterID(
        ChannelListener* aRouted,
        int32_t aId)
{
    return (mManager)->RegisterID(aRouted, aId);
}

ChannelListener*
PCookieServiceParent::Lookup(int32_t aId)
{
    return (mManager)->Lookup(aId);
}

void
PCookieServiceParent::Unregister(int32_t aId)
{
    return (mManager)->Unregister(aId);
}

void
PCookieServiceParent::RemoveManagee(
        int32_t aProtocolId,
        ChannelListener* aListener)
{
    NS_RUNTIMEABORT("unreached");
    return;
}

Shmem::SharedMemory*
PCookieServiceParent::CreateSharedMemory(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        bool aUnsafe,
        Shmem::id_t* aId)
{
    return (mManager)->CreateSharedMemory(aSize, aType, aUnsafe, aId);
}

bool
PCookieServiceParent::AdoptSharedMemory(
        Shmem::SharedMemory* segment,
        Shmem::id_t* aId)
{
    return (mManager)->AdoptSharedMemory(segment, aId);
}

Shmem::SharedMemory*
PCookieServiceParent::LookupSharedMemory(Shmem::id_t aId)
{
    return (mManager)->LookupSharedMemory(aId);
}

bool
PCookieServiceParent::IsTrackingSharedMemory(Shmem::SharedMemory* segment)
{
    return (mManager)->IsTrackingSharedMemory(segment);
}

bool
PCookieServiceParent::DestroySharedMemory(Shmem& shmem)
{
    return (mManager)->DestroySharedMemory(shmem);
}

ProcessHandle
PCookieServiceParent::OtherProcess() const
{
    return (mManager)->OtherProcess();
}

AsyncChannel*
PCookieServiceParent::GetIPCChannel()
{
    return mChannel;
}

PCookieServiceParent::Result
PCookieServiceParent::OnMessageReceived(const Message& __msg)
{
    switch ((__msg).type()) {
    case PCookieService::Msg_SetCookieString__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PCookieService::Msg_SetCookieString");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PCookieService::Msg_SetCookieString*>((&(__msg))))->Log("[PCookieServiceParent] Received ", stderr);
            }

            void* __iter = 0;
            URIParams host;
            bool isForeign;
            nsCString cookieString;
            nsCString serverTime;
            bool fromHttp;
            SerializedLoadContext loadContext;
            PBrowserParent* browser;

            if ((!(Read((&(host)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(isForeign)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(cookieString)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(serverTime)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(fromHttp)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(loadContext)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(browser)), (&(__msg)), (&(__iter)), true)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PCookieService::Transition(mState, Trigger(Trigger::Recv, PCookieService::Msg_SetCookieString__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(RecvSetCookieString(host, isForeign, cookieString, serverTime, fromHttp, loadContext, browser)))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for SetCookieString returned error code");
                return MsgProcessingError;
            }

            return MsgProcessed;
        }
    case PCookieService::Msg___delete____ID:
        {
            (const_cast<Message&>(__msg)).set_name("PCookieService::Msg___delete__");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PCookieService::Msg___delete__*>((&(__msg))))->Log("[PCookieServiceParent] Received ", stderr);
            }

            void* __iter = 0;
            PCookieServiceParent* actor;

            if ((!(Read((&(actor)), (&(__msg)), (&(__iter)), false)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PCookieService::Transition(mState, Trigger(Trigger::Recv, PCookieService::Msg___delete____ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            if ((!(Recv__delete__()))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for __delete__ returned error code");
                return MsgProcessingError;
            }

            (actor)->DestroySubtree(Deletion);
            (actor)->DeallocSubtree();
            ((actor)->mManager)->RemoveManagee(PCookieServiceMsgStart, actor);


            return MsgProcessed;
        }
    default:
        {
            return MsgNotKnown;
        }
    }
}

PCookieServiceParent::Result
PCookieServiceParent::OnMessageReceived(
        const Message& __msg,
        Message*& __reply)
{
    switch ((__msg).type()) {
    case PCookieService::Msg_GetCookieString__ID:
        {
            (const_cast<Message&>(__msg)).set_name("PCookieService::Msg_GetCookieString");
            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PCookieService::Msg_GetCookieString*>((&(__msg))))->Log("[PCookieServiceParent] Received ", stderr);
            }

            void* __iter = 0;
            URIParams host;
            bool isForeign;
            bool fromHttp;
            SerializedLoadContext loadContext;
            PBrowserParent* browser;

            if ((!(Read((&(host)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(isForeign)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(fromHttp)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(loadContext)), (&(__msg)), (&(__iter)))))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            if ((!(Read((&(browser)), (&(__msg)), (&(__iter)), true)))) {
                FatalError("error deserializing (better message TODO)");
                return MsgValueError;
            }
            (__msg).EndRead(__iter);
            if ((!(PCookieService::Transition(mState, Trigger(Trigger::Recv, PCookieService::Msg_GetCookieString__ID), (&(mState)))))) {
                NS_WARNING("bad state transition!");
            }
            nsCString result;
            int32_t __id = mId;
            if ((!(RecvGetCookieString(host, isForeign, fromHttp, loadContext, browser, (&(result)))))) {
                mozilla::ipc::ProtocolErrorBreakpoint("Handler for GetCookieString returned error code");
                return MsgProcessingError;
            }

            __reply = new PCookieService::Reply_GetCookieString();

            Write(result, __reply);
            (__reply)->set_routing_id(__id);
            (__reply)->set_sync();
            (__reply)->set_reply();

            if (mozilla::ipc::LoggingEnabled()) {
                (static_cast<const PCookieService::Reply_GetCookieString*>(__reply))->Log("[PCookieServiceParent] Sending reply ", stderr);
            }
            return MsgProcessed;
        }
    default:
        {
            return MsgNotKnown;
        }
    }
}

PCookieServiceParent::Result
PCookieServiceParent::OnCallReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

void
PCookieServiceParent::OnProcessingError(Result code)
{
    NS_RUNTIMEABORT("`OnProcessingError' called on non-toplevel actor");
}

int32_t
PCookieServiceParent::GetProtocolTypeId()
{
    return PCookieServiceMsgStart;
}

bool
PCookieServiceParent::OnReplyTimeout()
{
    NS_RUNTIMEABORT("`OnReplyTimeout' called on non-toplevel actor");
    return false;
}

void
PCookieServiceParent::OnChannelClose()
{
    NS_RUNTIMEABORT("`OnClose' called on non-toplevel actor");
}

void
PCookieServiceParent::OnChannelError()
{
    NS_RUNTIMEABORT("`OnError' called on non-toplevel actor");
}

void
PCookieServiceParent::OnChannelConnected(int32_t pid)
{
    NS_RUNTIMEABORT("'OnConnected' called on non-toplevel actor");
}

bool
PCookieServiceParent::AllocShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, false, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PCookieServiceParent::AllocUnsafeShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, true, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PCookieServiceParent::AdoptShmem(
        Shmem& aMem,
        Shmem* aOutMem)
{
    Shmem::SharedMemory* rawmem = (aMem).Segment(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    if (((!(rawmem))) || (IsTrackingSharedMemory(rawmem))) {
        NS_RUNTIMEABORT("bad Shmem");
    }

    Shmem::id_t aId;
    if ((!(AdoptSharedMemory(rawmem, (&(aId)))))) {
        return false;
    }

    (*(aOutMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), rawmem, aId);
    return true;
}

bool
PCookieServiceParent::DeallocShmem(Shmem& aMem)
{
    bool ok = DestroySharedMemory(aMem);
    (aMem).forget(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    return ok;
}

void
PCookieServiceParent::FatalError(const char* const msg) const
{
    // Virtual method to prevent inlining.
    // This give us better error reporting.
    // See bug 589371

    NS_ERROR("IPDL error:");
    NS_ERROR(msg);
    mozilla::ipc::ProtocolErrorBreakpoint(msg);

    NS_ERROR("[PCookieServiceParent] killing child side as a result");

    if ((!(base::KillProcess(OtherProcess(), base::PROCESS_END_KILLED_BY_USER, false)))) {
        NS_ERROR("  may have failed to kill child!");
    }
}

void
PCookieServiceParent::DestroySubtree(ActorDestroyReason why)
{
    // Unregister from our manager.
    Unregister(mId);
    mId = 1;

    // Finally, destroy "us".
    ActorDestroy(why);
}

void
PCookieServiceParent::DeallocSubtree()
{
}

void
PCookieServiceParent::Write(
        PBrowserParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PCookieServiceParent::Read(
        PBrowserParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PCookieService");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PBrowser");
        return false;
    }

    if ((PBrowserMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PBrowser has different type");
        return false;
    }

    (*(__v)) = static_cast<PBrowserParent*>(listener);
    return true;
}

void
PCookieServiceParent::Write(
        const StandardURLParams& __v,
        Message* __msg)
{
    Write((__v).urlType(), __msg);
    Write((__v).port(), __msg);
    Write((__v).defaultPort(), __msg);
    Write((__v).spec(), __msg);
    Write((__v).scheme(), __msg);
    Write((__v).authority(), __msg);
    Write((__v).username(), __msg);
    Write((__v).password(), __msg);
    Write((__v).host(), __msg);
    Write((__v).path(), __msg);
    Write((__v).filePath(), __msg);
    Write((__v).directory(), __msg);
    Write((__v).baseName(), __msg);
    Write((__v).extension(), __msg);
    Write((__v).query(), __msg);
    Write((__v).ref(), __msg);
    Write((__v).originCharset(), __msg);
    Write((__v).isMutable(), __msg);
    Write((__v).supportsFileURL(), __msg);
    Write((__v).hostEncoding(), __msg);
}

bool
PCookieServiceParent::Read(
        StandardURLParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->urlType())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->port())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->defaultPort())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->spec())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->scheme())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->authority())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->username())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->password())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->host())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->path())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->filePath())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->directory())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->baseName())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->extension())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->query())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->ref())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->originCharset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->isMutable())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->supportsFileURL())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->hostEncoding())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PCookieServiceParent::Write(
        const JARURIParams& __v,
        Message* __msg)
{
    Write((__v).jarFile(), __msg);
    Write((__v).jarEntry(), __msg);
    Write((__v).charset(), __msg);
}

bool
PCookieServiceParent::Read(
        JARURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->jarFile())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->jarEntry())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->charset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PCookieServiceParent::Write(
        const GenericURIParams& __v,
        Message* __msg)
{
    Write((__v).spec(), __msg);
    Write((__v).charset(), __msg);
}

bool
PCookieServiceParent::Read(
        GenericURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->spec())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->charset())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PCookieServiceParent::Write(
        const StandardURLSegment& __v,
        Message* __msg)
{
    Write((__v).position(), __msg);
    Write((__v).length(), __msg);
}

bool
PCookieServiceParent::Read(
        StandardURLSegment* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->position())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->length())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PCookieServiceParent::Write(
        const URIParams& __v,
        Message* __msg)
{
    typedef URIParams __type;
    Write(int((__v).type()), __msg);

    switch ((__v).type()) {
    case __type::TSimpleURIParams:
        {
            Write((__v).get_SimpleURIParams(), __msg);
            return;
        }
    case __type::TStandardURLParams:
        {
            Write((__v).get_StandardURLParams(), __msg);
            return;
        }
    case __type::TJARURIParams:
        {
            Write((__v).get_JARURIParams(), __msg);
            return;
        }
    case __type::TGenericURIParams:
        {
            Write((__v).get_GenericURIParams(), __msg);
            return;
        }
    default:
        {
            NS_RUNTIMEABORT("unknown union type");
            return;
        }
    }
}

bool
PCookieServiceParent::Read(
        URIParams* __v,
        const Message* __msg,
        void** __iter)
{
    typedef URIParams __type;
    int type;
    if ((!(Read((&(type)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    switch (type) {
    case __type::TSimpleURIParams:
        {
            SimpleURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_SimpleURIParams())), __msg, __iter);
        }
    case __type::TStandardURLParams:
        {
            StandardURLParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_StandardURLParams())), __msg, __iter);
        }
    case __type::TJARURIParams:
        {
            JARURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_JARURIParams())), __msg, __iter);
        }
    case __type::TGenericURIParams:
        {
            GenericURIParams tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GenericURIParams())), __msg, __iter);
        }
    default:
        {
            mozilla::ipc::ProtocolErrorBreakpoint("unknown union type");
            return false;
        }
    }
}

void
PCookieServiceParent::Write(
        const SimpleURIParams& __v,
        Message* __msg)
{
    Write((__v).scheme(), __msg);
    Write((__v).path(), __msg);
    Write((__v).ref(), __msg);
    Write((__v).isMutable(), __msg);
}

bool
PCookieServiceParent::Read(
        SimpleURIParams* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->scheme())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->path())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->ref())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->isMutable())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PCookieServiceParent::Write(
        PCookieServiceParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PCookieServiceParent::Read(
        PCookieServiceParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PCookieService");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PCookieService");
        return false;
    }

    if ((PCookieServiceMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PCookieService has different type");
        return false;
    }

    (*(__v)) = static_cast<PCookieServiceParent*>(listener);
    return true;
}



} // namespace net
} // namespace mozilla
