//
// Automatically generated by ipdlc.
// Edit at your own risk
//


#include "mozilla/ipc/InputStreamParams.h"


//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct StringInputStreamParams|
//
namespace mozilla {
namespace ipc {
StringInputStreamParams::StringInputStreamParams()
{
    Init();
}

StringInputStreamParams::~StringInputStreamParams()
{
}

bool
StringInputStreamParams::operator==(const StringInputStreamParams& _o) const
{
    if ((!((data()) == ((_o).data())))) {
        return false;
    }
    return true;
}

void
StringInputStreamParams::Init()
{
}

void
StringInputStreamParams::Assign(const nsCString& _data)
{
    data_ = _data;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct FileInputStreamParams|
//
namespace mozilla {
namespace ipc {
FileInputStreamParams::FileInputStreamParams()
{
    Init();
}

FileInputStreamParams::~FileInputStreamParams()
{
}

bool
FileInputStreamParams::operator==(const FileInputStreamParams& _o) const
{
    if ((!((file()) == ((_o).file())))) {
        return false;
    }
    if ((!((behaviorFlags()) == ((_o).behaviorFlags())))) {
        return false;
    }
    if ((!((ioFlags()) == ((_o).ioFlags())))) {
        return false;
    }
    return true;
}

void
FileInputStreamParams::Init()
{
}

void
FileInputStreamParams::Assign(
        const FileDescriptor& _file,
        const int32_t& _behaviorFlags,
        const int32_t& _ioFlags)
{
    file_ = _file;
    behaviorFlags_ = _behaviorFlags;
    ioFlags_ = _ioFlags;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct PartialFileInputStreamParams|
//
namespace mozilla {
namespace ipc {
PartialFileInputStreamParams::PartialFileInputStreamParams()
{
    Init();
}

PartialFileInputStreamParams::~PartialFileInputStreamParams()
{
}

bool
PartialFileInputStreamParams::operator==(const PartialFileInputStreamParams& _o) const
{
    if ((!((fileStreamParams()) == ((_o).fileStreamParams())))) {
        return false;
    }
    if ((!((begin()) == ((_o).begin())))) {
        return false;
    }
    if ((!((length()) == ((_o).length())))) {
        return false;
    }
    return true;
}

void
PartialFileInputStreamParams::Init()
{
}

void
PartialFileInputStreamParams::Assign(
        const FileInputStreamParams& _fileStreamParams,
        const uint64_t& _begin,
        const uint64_t& _length)
{
    fileStreamParams_ = _fileStreamParams;
    begin_ = _begin;
    length_ = _length;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct MultiplexInputStreamParams|
//
namespace mozilla {
namespace ipc {
MultiplexInputStreamParams::MultiplexInputStreamParams()
{
    Init();
}

MultiplexInputStreamParams::~MultiplexInputStreamParams()
{
    delete streams_;
}

bool
MultiplexInputStreamParams::operator==(const MultiplexInputStreamParams& _o) const
{
    if ((!((streams()) == ((_o).streams())))) {
        return false;
    }
    if ((!((currentStream()) == ((_o).currentStream())))) {
        return false;
    }
    if ((!((status()) == ((_o).status())))) {
        return false;
    }
    if ((!((startedReadingCurrent()) == ((_o).startedReadingCurrent())))) {
        return false;
    }
    return true;
}

void
MultiplexInputStreamParams::Init()
{
    streams_ = new InfallibleTArray<InputStreamParams>();
}

void
MultiplexInputStreamParams::Assign(
        const InfallibleTArray<InputStreamParams>& _streams,
        const uint32_t& _currentStream,
        const nsresult& _status,
        const bool& _startedReadingCurrent)
{
    (*(streams_)) = _streams;
    currentStream_ = _currentStream;
    status_ = _status;
    startedReadingCurrent_ = _startedReadingCurrent;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct RemoteInputStreamParams|
//
namespace mozilla {
namespace ipc {
RemoteInputStreamParams::RemoteInputStreamParams()
{
    Init();
}

RemoteInputStreamParams::~RemoteInputStreamParams()
{
}

bool
RemoteInputStreamParams::operator==(const RemoteInputStreamParams& _o) const
{
    if ((!((remoteBlobParent()) == ((_o).remoteBlobParent())))) {
        return false;
    }
    if ((!((remoteBlobChild()) == ((_o).remoteBlobChild())))) {
        return false;
    }
    return true;
}

void
RemoteInputStreamParams::Init()
{
    remoteBlobParent_ = 0;
    remoteBlobChild_ = 0;
}

void
RemoteInputStreamParams::Assign(
        PBlobParent* _remoteBlobParent,
        PBlobChild* _remoteBlobChild)
{
    remoteBlobParent_ = _remoteBlobParent;
    remoteBlobChild_ = _remoteBlobChild;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |union InputStreamParams|
//
namespace mozilla {
namespace ipc {
bool
InputStreamParams::MaybeDestroy(Type aNewType)
{
    if ((mType) == (T__None)) {
        return true;
    }
    if ((mType) == (aNewType)) {
        return false;
    }
    switch (mType) {
    case TStringInputStreamParams:
        {
            (ptr_StringInputStreamParams())->~StringInputStreamParams__tdef();
            break;
        }
    case TFileInputStreamParams:
        {
            (ptr_FileInputStreamParams())->~FileInputStreamParams__tdef();
            break;
        }
    case TPartialFileInputStreamParams:
        {
            (ptr_PartialFileInputStreamParams())->~PartialFileInputStreamParams__tdef();
            break;
        }
    case TBufferedInputStreamParams:
        {
            delete ptr_BufferedInputStreamParams();
            break;
        }
    case TMIMEInputStreamParams:
        {
            delete ptr_MIMEInputStreamParams();
            break;
        }
    case TMultiplexInputStreamParams:
        {
            delete ptr_MultiplexInputStreamParams();
            break;
        }
    case TRemoteInputStreamParams:
        {
            (ptr_RemoteInputStreamParams())->~RemoteInputStreamParams__tdef();
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("not reached");
            break;
        }
    }
    return true;
}

InputStreamParams::InputStreamParams(const StringInputStreamParams& aOther)
{
    new (ptr_StringInputStreamParams()) StringInputStreamParams(aOther);
    mType = TStringInputStreamParams;
}

InputStreamParams::InputStreamParams(const FileInputStreamParams& aOther)
{
    new (ptr_FileInputStreamParams()) FileInputStreamParams(aOther);
    mType = TFileInputStreamParams;
}

InputStreamParams::InputStreamParams(const PartialFileInputStreamParams& aOther)
{
    new (ptr_PartialFileInputStreamParams()) PartialFileInputStreamParams(aOther);
    mType = TPartialFileInputStreamParams;
}

InputStreamParams::InputStreamParams(const BufferedInputStreamParams& aOther)
{
    ptr_BufferedInputStreamParams() = new BufferedInputStreamParams(aOther);
    mType = TBufferedInputStreamParams;
}

InputStreamParams::InputStreamParams(const MIMEInputStreamParams& aOther)
{
    ptr_MIMEInputStreamParams() = new MIMEInputStreamParams(aOther);
    mType = TMIMEInputStreamParams;
}

InputStreamParams::InputStreamParams(const MultiplexInputStreamParams& aOther)
{
    ptr_MultiplexInputStreamParams() = new MultiplexInputStreamParams(aOther);
    mType = TMultiplexInputStreamParams;
}

InputStreamParams::InputStreamParams(const RemoteInputStreamParams& aOther)
{
    new (ptr_RemoteInputStreamParams()) RemoteInputStreamParams(aOther);
    mType = TRemoteInputStreamParams;
}

InputStreamParams::InputStreamParams(const InputStreamParams& aOther)
{
    (aOther).AssertSanity();
    switch ((aOther).type()) {
    case TStringInputStreamParams:
        {
            new (ptr_StringInputStreamParams()) StringInputStreamParams((aOther).get_StringInputStreamParams());
            break;
        }
    case TFileInputStreamParams:
        {
            new (ptr_FileInputStreamParams()) FileInputStreamParams((aOther).get_FileInputStreamParams());
            break;
        }
    case TPartialFileInputStreamParams:
        {
            new (ptr_PartialFileInputStreamParams()) PartialFileInputStreamParams((aOther).get_PartialFileInputStreamParams());
            break;
        }
    case TBufferedInputStreamParams:
        {
            ptr_BufferedInputStreamParams() = new BufferedInputStreamParams((aOther).get_BufferedInputStreamParams());
            break;
        }
    case TMIMEInputStreamParams:
        {
            ptr_MIMEInputStreamParams() = new MIMEInputStreamParams((aOther).get_MIMEInputStreamParams());
            break;
        }
    case TMultiplexInputStreamParams:
        {
            ptr_MultiplexInputStreamParams() = new MultiplexInputStreamParams((aOther).get_MultiplexInputStreamParams());
            break;
        }
    case TRemoteInputStreamParams:
        {
            new (ptr_RemoteInputStreamParams()) RemoteInputStreamParams((aOther).get_RemoteInputStreamParams());
            break;
        }
    case T__None:
        {
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            return;
        }
    }
    mType = (aOther).type();
}

InputStreamParams::~InputStreamParams()
{
    MaybeDestroy(T__None);
}

InputStreamParams&
InputStreamParams::operator=(const StringInputStreamParams& aRhs)
{
    if (MaybeDestroy(TStringInputStreamParams)) {
        new (ptr_StringInputStreamParams()) StringInputStreamParams;
    }
    (*(ptr_StringInputStreamParams())) = aRhs;
    mType = TStringInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const FileInputStreamParams& aRhs)
{
    if (MaybeDestroy(TFileInputStreamParams)) {
        new (ptr_FileInputStreamParams()) FileInputStreamParams;
    }
    (*(ptr_FileInputStreamParams())) = aRhs;
    mType = TFileInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const PartialFileInputStreamParams& aRhs)
{
    if (MaybeDestroy(TPartialFileInputStreamParams)) {
        new (ptr_PartialFileInputStreamParams()) PartialFileInputStreamParams;
    }
    (*(ptr_PartialFileInputStreamParams())) = aRhs;
    mType = TPartialFileInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const BufferedInputStreamParams& aRhs)
{
    if (MaybeDestroy(TBufferedInputStreamParams)) {
        ptr_BufferedInputStreamParams() = new BufferedInputStreamParams;
    }
    (*(ptr_BufferedInputStreamParams())) = aRhs;
    mType = TBufferedInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const MIMEInputStreamParams& aRhs)
{
    if (MaybeDestroy(TMIMEInputStreamParams)) {
        ptr_MIMEInputStreamParams() = new MIMEInputStreamParams;
    }
    (*(ptr_MIMEInputStreamParams())) = aRhs;
    mType = TMIMEInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const MultiplexInputStreamParams& aRhs)
{
    if (MaybeDestroy(TMultiplexInputStreamParams)) {
        ptr_MultiplexInputStreamParams() = new MultiplexInputStreamParams;
    }
    (*(ptr_MultiplexInputStreamParams())) = aRhs;
    mType = TMultiplexInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const RemoteInputStreamParams& aRhs)
{
    if (MaybeDestroy(TRemoteInputStreamParams)) {
        new (ptr_RemoteInputStreamParams()) RemoteInputStreamParams;
    }
    (*(ptr_RemoteInputStreamParams())) = aRhs;
    mType = TRemoteInputStreamParams;
    return (*(this));
}

InputStreamParams&
InputStreamParams::operator=(const InputStreamParams& aRhs)
{
    (aRhs).AssertSanity();
    Type t = (aRhs).type();
    switch (t) {
    case TStringInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                new (ptr_StringInputStreamParams()) StringInputStreamParams;
            }
            (*(ptr_StringInputStreamParams())) = (aRhs).get_StringInputStreamParams();
            break;
        }
    case TFileInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                new (ptr_FileInputStreamParams()) FileInputStreamParams;
            }
            (*(ptr_FileInputStreamParams())) = (aRhs).get_FileInputStreamParams();
            break;
        }
    case TPartialFileInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                new (ptr_PartialFileInputStreamParams()) PartialFileInputStreamParams;
            }
            (*(ptr_PartialFileInputStreamParams())) = (aRhs).get_PartialFileInputStreamParams();
            break;
        }
    case TBufferedInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                ptr_BufferedInputStreamParams() = new BufferedInputStreamParams;
            }
            (*(ptr_BufferedInputStreamParams())) = (aRhs).get_BufferedInputStreamParams();
            break;
        }
    case TMIMEInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                ptr_MIMEInputStreamParams() = new MIMEInputStreamParams;
            }
            (*(ptr_MIMEInputStreamParams())) = (aRhs).get_MIMEInputStreamParams();
            break;
        }
    case TMultiplexInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                ptr_MultiplexInputStreamParams() = new MultiplexInputStreamParams;
            }
            (*(ptr_MultiplexInputStreamParams())) = (aRhs).get_MultiplexInputStreamParams();
            break;
        }
    case TRemoteInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                new (ptr_RemoteInputStreamParams()) RemoteInputStreamParams;
            }
            (*(ptr_RemoteInputStreamParams())) = (aRhs).get_RemoteInputStreamParams();
            break;
        }
    case T__None:
        {
            MaybeDestroy(t);
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            break;
        }
    }
    mType = t;
    return (*(this));
}

bool
InputStreamParams::operator==(const StringInputStreamParams& aRhs) const
{
    return (get_StringInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const FileInputStreamParams& aRhs) const
{
    return (get_FileInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const PartialFileInputStreamParams& aRhs) const
{
    return (get_PartialFileInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const BufferedInputStreamParams& aRhs) const
{
    return (get_BufferedInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const MIMEInputStreamParams& aRhs) const
{
    return (get_MIMEInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const MultiplexInputStreamParams& aRhs) const
{
    return (get_MultiplexInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const RemoteInputStreamParams& aRhs) const
{
    return (get_RemoteInputStreamParams()) == (aRhs);
}

bool
InputStreamParams::operator==(const InputStreamParams& aRhs) const
{
    if ((type()) != ((aRhs).type())) {
        return false;
    }

    switch (type()) {
    case TStringInputStreamParams:
        {
            return (get_StringInputStreamParams()) == ((aRhs).get_StringInputStreamParams());
        }
    case TFileInputStreamParams:
        {
            return (get_FileInputStreamParams()) == ((aRhs).get_FileInputStreamParams());
        }
    case TPartialFileInputStreamParams:
        {
            return (get_PartialFileInputStreamParams()) == ((aRhs).get_PartialFileInputStreamParams());
        }
    case TBufferedInputStreamParams:
        {
            return (get_BufferedInputStreamParams()) == ((aRhs).get_BufferedInputStreamParams());
        }
    case TMIMEInputStreamParams:
        {
            return (get_MIMEInputStreamParams()) == ((aRhs).get_MIMEInputStreamParams());
        }
    case TMultiplexInputStreamParams:
        {
            return (get_MultiplexInputStreamParams()) == ((aRhs).get_MultiplexInputStreamParams());
        }
    case TRemoteInputStreamParams:
        {
            return (get_RemoteInputStreamParams()) == ((aRhs).get_RemoteInputStreamParams());
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            return false;
        }
    }
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |union OptionalInputStreamParams|
//
namespace mozilla {
namespace ipc {
bool
OptionalInputStreamParams::MaybeDestroy(Type aNewType)
{
    if ((mType) == (T__None)) {
        return true;
    }
    if ((mType) == (aNewType)) {
        return false;
    }
    switch (mType) {
    case Tvoid_t:
        {
            (ptr_void_t())->~void_t__tdef();
            break;
        }
    case TInputStreamParams:
        {
            delete ptr_InputStreamParams();
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("not reached");
            break;
        }
    }
    return true;
}

OptionalInputStreamParams::OptionalInputStreamParams(const void_t& aOther)
{
    new (ptr_void_t()) void_t(aOther);
    mType = Tvoid_t;
}

OptionalInputStreamParams::OptionalInputStreamParams(const InputStreamParams& aOther)
{
    ptr_InputStreamParams() = new InputStreamParams(aOther);
    mType = TInputStreamParams;
}

OptionalInputStreamParams::OptionalInputStreamParams(const OptionalInputStreamParams& aOther)
{
    (aOther).AssertSanity();
    switch ((aOther).type()) {
    case Tvoid_t:
        {
            new (ptr_void_t()) void_t((aOther).get_void_t());
            break;
        }
    case TInputStreamParams:
        {
            ptr_InputStreamParams() = new InputStreamParams((aOther).get_InputStreamParams());
            break;
        }
    case T__None:
        {
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            return;
        }
    }
    mType = (aOther).type();
}

OptionalInputStreamParams::~OptionalInputStreamParams()
{
    MaybeDestroy(T__None);
}

OptionalInputStreamParams&
OptionalInputStreamParams::operator=(const void_t& aRhs)
{
    if (MaybeDestroy(Tvoid_t)) {
        new (ptr_void_t()) void_t;
    }
    (*(ptr_void_t())) = aRhs;
    mType = Tvoid_t;
    return (*(this));
}

OptionalInputStreamParams&
OptionalInputStreamParams::operator=(const InputStreamParams& aRhs)
{
    if (MaybeDestroy(TInputStreamParams)) {
        ptr_InputStreamParams() = new InputStreamParams;
    }
    (*(ptr_InputStreamParams())) = aRhs;
    mType = TInputStreamParams;
    return (*(this));
}

OptionalInputStreamParams&
OptionalInputStreamParams::operator=(const OptionalInputStreamParams& aRhs)
{
    (aRhs).AssertSanity();
    Type t = (aRhs).type();
    switch (t) {
    case Tvoid_t:
        {
            if (MaybeDestroy(t)) {
                new (ptr_void_t()) void_t;
            }
            (*(ptr_void_t())) = (aRhs).get_void_t();
            break;
        }
    case TInputStreamParams:
        {
            if (MaybeDestroy(t)) {
                ptr_InputStreamParams() = new InputStreamParams;
            }
            (*(ptr_InputStreamParams())) = (aRhs).get_InputStreamParams();
            break;
        }
    case T__None:
        {
            MaybeDestroy(t);
            break;
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            break;
        }
    }
    mType = t;
    return (*(this));
}

bool
OptionalInputStreamParams::operator==(const void_t& aRhs) const
{
    return (get_void_t()) == (aRhs);
}

bool
OptionalInputStreamParams::operator==(const InputStreamParams& aRhs) const
{
    return (get_InputStreamParams()) == (aRhs);
}

bool
OptionalInputStreamParams::operator==(const OptionalInputStreamParams& aRhs) const
{
    if ((type()) != ((aRhs).type())) {
        return false;
    }

    switch (type()) {
    case Tvoid_t:
        {
            return (get_void_t()) == ((aRhs).get_void_t());
        }
    case TInputStreamParams:
        {
            return (get_InputStreamParams()) == ((aRhs).get_InputStreamParams());
        }
    default:
        {
            NS_RUNTIMEABORT("unreached");
            return false;
        }
    }
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct BufferedInputStreamParams|
//
namespace mozilla {
namespace ipc {
BufferedInputStreamParams::BufferedInputStreamParams()
{
    Init();
}

BufferedInputStreamParams::~BufferedInputStreamParams()
{
    delete optionalStream_;
}

bool
BufferedInputStreamParams::operator==(const BufferedInputStreamParams& _o) const
{
    if ((!((optionalStream()) == ((_o).optionalStream())))) {
        return false;
    }
    if ((!((bufferSize()) == ((_o).bufferSize())))) {
        return false;
    }
    return true;
}

void
BufferedInputStreamParams::Init()
{
    optionalStream_ = new OptionalInputStreamParams();
}

void
BufferedInputStreamParams::Assign(
        const OptionalInputStreamParams& _optionalStream,
        const uint32_t& _bufferSize)
{
    (*(optionalStream_)) = _optionalStream;
    bufferSize_ = _bufferSize;
}

} // namespace ipc
} // namespace mozilla

//-----------------------------------------------------------------------------
// Method definitions for the IPDL type |struct MIMEInputStreamParams|
//
namespace mozilla {
namespace ipc {
MIMEInputStreamParams::MIMEInputStreamParams()
{
    Init();
}

MIMEInputStreamParams::~MIMEInputStreamParams()
{
    delete optionalStream_;
}

bool
MIMEInputStreamParams::operator==(const MIMEInputStreamParams& _o) const
{
    if ((!((optionalStream()) == ((_o).optionalStream())))) {
        return false;
    }
    if ((!((headers()) == ((_o).headers())))) {
        return false;
    }
    if ((!((contentLength()) == ((_o).contentLength())))) {
        return false;
    }
    if ((!((startedReading()) == ((_o).startedReading())))) {
        return false;
    }
    if ((!((addContentLength()) == ((_o).addContentLength())))) {
        return false;
    }
    return true;
}

void
MIMEInputStreamParams::Init()
{
    optionalStream_ = new OptionalInputStreamParams();
}

void
MIMEInputStreamParams::Assign(
        const OptionalInputStreamParams& _optionalStream,
        const nsCString& _headers,
        const nsCString& _contentLength,
        const bool& _startedReading,
        const bool& _addContentLength)
{
    (*(optionalStream_)) = _optionalStream;
    headers_ = _headers;
    contentLength_ = _contentLength;
    startedReading_ = _startedReading;
    addContentLength_ = _addContentLength;
}

} // namespace ipc
} // namespace mozilla
