//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PPluginInstanceChild_h
#define PPluginInstanceChild_h

#include "mozilla/plugins/PPluginInstance.h"
namespace mozilla {
namespace plugins {
class PPluginBackgroundDestroyerChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PPluginModuleChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PPluginScriptableObjectChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PBrowserStreamChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PPluginStreamChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PStreamNotifyChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {
class PPluginSurfaceChild;
} // namespace plugins
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace plugins {
class PPluginStreamChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PPluginSurfaceChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PPluginBackgroundDestroyerChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PBrowserStreamChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PPluginModuleChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PStreamNotifyChild;
} // namespace plugins
} // namespace mozilla


namespace mozilla {
namespace plugins {
class PPluginScriptableObjectChild;
} // namespace plugins
} // namespace mozilla

namespace mozilla {
namespace plugins {


class /*NS_ABSTRACT_CLASS*/ PPluginInstanceChild :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::plugins::PPluginStreamChild;

    friend class mozilla::plugins::PPluginSurfaceChild;

    friend class mozilla::plugins::PPluginBackgroundDestroyerChild;

    friend class mozilla::plugins::PBrowserStreamChild;

    friend class mozilla::plugins::PPluginModuleChild;

    friend class mozilla::plugins::PStreamNotifyChild;

    friend class mozilla::plugins::PPluginScriptableObjectChild;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::plugins::AsyncSurfaceDescriptor AsyncSurfaceDescriptor;
    typedef mozilla::CrossProcessMutexHandle CrossProcessMutexHandle;
    typedef mozilla::plugins::DXGISharedSurfaceHandle DXGISharedSurfaceHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::plugins::IOSurfaceDescriptor IOSurfaceDescriptor;
    typedef mozilla::plugins::NPRemoteAsyncSurface NPRemoteAsyncSurface;
    typedef mozilla::plugins::NativeWindowHandle NativeWindowHandle;
    typedef mozilla::plugins::OptionalShmem OptionalShmem;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::plugins::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::plugins::WindowsSharedMemoryHandle WindowsSharedMemoryHandle;
    typedef mozilla::gfxSurfaceType gfxSurfaceType;
    typedef mozilla::null_t null_t;
    typedef mozilla::plugins::PPluginBackgroundDestroyerChild PPluginBackgroundDestroyerChild;
    typedef mozilla::plugins::PPluginModuleChild PPluginModuleChild;
    typedef mozilla::plugins::PPluginScriptableObjectChild PPluginScriptableObjectChild;
    typedef mozilla::plugins::PBrowserStreamChild PBrowserStreamChild;
    typedef mozilla::plugins::PPluginStreamChild PPluginStreamChild;
    typedef mozilla::plugins::PStreamNotifyChild PStreamNotifyChild;
    typedef mozilla::plugins::PPluginSurfaceChild PPluginSurfaceChild;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PPluginInstance::State State;

    virtual bool
    Answer__delete__();
    virtual bool
    AnswerNPP_SetWindow(const NPRemoteWindow& window) = 0;
    virtual bool
    AnswerNPP_GetValue_NPPVpluginWantsAllNetworkStreams(
            bool* value,
            NPError* result) = 0;
    virtual bool
    AnswerNPP_GetValue_NPPVpluginNeedsXEmbed(
            bool* value,
            NPError* result) = 0;
    virtual bool
    AnswerNPP_GetValue_NPPVpluginScriptableNPObject(
            PPluginScriptableObjectChild** value,
            NPError* result) = 0;
    virtual bool
    AnswerNPP_SetValue_NPNVprivateModeBool(
            const bool& value,
            NPError* result) = 0;
    virtual bool
    AnswerNPP_GetValue_NPPVpluginNativeAccessibleAtkPlugId(
            nsCString* plug_id,
            NPError* result) = 0;
    virtual bool
    AnswerNPP_HandleEvent(
            const NPRemoteEvent& event,
            int16_t* handled) = 0;
    virtual bool
    AnswerNPP_HandleEvent_Shmem(
            const NPRemoteEvent& event,
            Shmem& buffer,
            int16_t* handled,
            Shmem* rtnbuffer) = 0;
    virtual bool
    AnswerNPP_HandleEvent_IOSurface(
            const NPRemoteEvent& event,
            const uint32_t& surfaceid,
            int16_t* handled) = 0;
    virtual bool
    AnswerPaint(
            const NPRemoteEvent& event,
            int16_t* handled) = 0;
    virtual bool
    RecvWindowPosChanged(const NPRemoteEvent& event) = 0;
    virtual bool
    RecvContentsScaleFactorChanged(const double& aContentsScaleFactor) = 0;
    virtual bool
    RecvAsyncSetWindow(
            const gfxSurfaceType& surfaceType,
            const NPRemoteWindow& window) = 0;
    virtual bool
    RecvUpdateBackground(
            const SurfaceDescriptor& background,
            const nsIntRect& rect) = 0;
    virtual bool
    AnswerHandleTextEvent(
            const nsTextEvent& event,
            bool* handled) = 0;
    virtual bool
    AnswerHandleKeyEvent(
            const nsKeyEvent& event,
            bool* handled) = 0;
    virtual bool
    RecvNPP_DidComposite() = 0;
    virtual bool
    AnswerNPP_Destroy(NPError* rv) = 0;
    virtual bool
    RecvPPluginScriptableObjectConstructor(PPluginScriptableObjectChild* actor);
    virtual bool
    AnswerPBrowserStreamConstructor(
            PBrowserStreamChild* actor,
            const nsCString& url,
            const uint32_t& length,
            const uint32_t& lastmodified,
            PStreamNotifyChild* notifyData,
            const nsCString& headers,
            const nsCString& mimeType,
            const bool& seekable,
            NPError* rv,
            uint16_t* stype);
    virtual bool
    AnswerSetPluginFocus() = 0;
    virtual bool
    AnswerUpdateWindow() = 0;
    virtual bool
    RecvPPluginBackgroundDestroyerConstructor(PPluginBackgroundDestroyerChild* actor);
    virtual PStreamNotifyChild*
    AllocPStreamNotify(
            const nsCString& url,
            const nsCString& target,
            const bool& post,
            const nsCString& buffer,
            const bool& file,
            NPError* result) = 0;
    virtual bool
    DeallocPStreamNotify(PStreamNotifyChild* actor) = 0;
    virtual PPluginSurfaceChild*
    AllocPPluginSurface(
            const WindowsSharedMemoryHandle& handle,
            const gfxIntSize& size,
            const bool& transparent) = 0;
    virtual bool
    DeallocPPluginSurface(PPluginSurfaceChild* actor) = 0;
    virtual PPluginScriptableObjectChild*
    AllocPPluginScriptableObject() = 0;
    virtual bool
    DeallocPPluginScriptableObject(PPluginScriptableObjectChild* actor) = 0;
    virtual PBrowserStreamChild*
    AllocPBrowserStream(
            const nsCString& url,
            const uint32_t& length,
            const uint32_t& lastmodified,
            PStreamNotifyChild* notifyData,
            const nsCString& headers,
            const nsCString& mimeType,
            const bool& seekable,
            NPError* rv,
            uint16_t* stype) = 0;
    virtual bool
    DeallocPBrowserStream(PBrowserStreamChild* actor) = 0;
    virtual PPluginStreamChild*
    AllocPPluginStream(
            const nsCString& mimeType,
            const nsCString& target,
            NPError* result) = 0;
    virtual bool
    DeallocPPluginStream(PPluginStreamChild* actor) = 0;
    virtual PPluginBackgroundDestroyerChild*
    AllocPPluginBackgroundDestroyer() = 0;
    virtual bool
    DeallocPPluginBackgroundDestroyer(PPluginBackgroundDestroyerChild* actor) = 0;

    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PPluginInstanceChild();

    virtual ~PPluginInstanceChild();

    PPluginModuleChild*
    Manager() const;

    void
    ManagedPPluginBackgroundDestroyerChild(InfallibleTArray<PPluginBackgroundDestroyerChild*>& aArr) const;
    const InfallibleTArray<PPluginBackgroundDestroyerChild*>&
    ManagedPPluginBackgroundDestroyerChild() const;

    void
    ManagedPPluginScriptableObjectChild(InfallibleTArray<PPluginScriptableObjectChild*>& aArr) const;
    const InfallibleTArray<PPluginScriptableObjectChild*>&
    ManagedPPluginScriptableObjectChild() const;

    void
    ManagedPBrowserStreamChild(InfallibleTArray<PBrowserStreamChild*>& aArr) const;
    const InfallibleTArray<PBrowserStreamChild*>&
    ManagedPBrowserStreamChild() const;

    void
    ManagedPPluginStreamChild(InfallibleTArray<PPluginStreamChild*>& aArr) const;
    const InfallibleTArray<PPluginStreamChild*>&
    ManagedPPluginStreamChild() const;

    void
    ManagedPStreamNotifyChild(InfallibleTArray<PStreamNotifyChild*>& aArr) const;
    const InfallibleTArray<PStreamNotifyChild*>&
    ManagedPStreamNotifyChild() const;

    void
    ManagedPPluginSurfaceChild(InfallibleTArray<PPluginSurfaceChild*>& aArr) const;
    const InfallibleTArray<PPluginSurfaceChild*>&
    ManagedPPluginSurfaceChild() const;

    PPluginInstance::State
    state();

    bool
    CallNPN_GetValue_NPNVWindowNPObject(
            PPluginScriptableObjectChild** value,
            NPError* result);

    bool
    CallNPN_GetValue_NPNVPluginElementNPObject(
            PPluginScriptableObjectChild** value,
            NPError* result);

    bool
    CallNPN_GetValue_NPNVprivateModeBool(
            bool* value,
            NPError* result);

    bool
    CallNPN_GetValue_NPNVnetscapeWindow(
            NativeWindowHandle* value,
            NPError* result);

    bool
    CallNPN_GetValue_NPNVdocumentOrigin(
            nsCString* value,
            NPError* result);

    bool
    CallNPN_GetValue_DrawingModelSupport(
            const NPNVariable& model,
            bool* value);

    bool
    CallNPN_SetValue_NPPVpluginWindow(
            const bool& windowed,
            NPError* result);

    bool
    CallNPN_SetValue_NPPVpluginTransparent(
            const bool& transparent,
            NPError* result);

    bool
    CallNPN_SetValue_NPPVpluginUsesDOMForCursor(
            const bool& useDOMForCursor,
            NPError* result);

    bool
    CallNPN_SetValue_NPPVpluginDrawingModel(
            const int& drawingModel,
            OptionalShmem* remoteImageData,
            CrossProcessMutexHandle* mutex,
            NPError* result);

    bool
    CallNPN_SetValue_NPPVpluginEventModel(
            const int& eventModel,
            NPError* result);

    bool
    CallNPN_GetURL(
            const nsCString& url,
            const nsCString& target,
            NPError* result);

    bool
    CallNPN_PostURL(
            const nsCString& url,
            const nsCString& target,
            const nsCString& buffer,
            const bool& file,
            NPError* result);

    PStreamNotifyChild*
    CallPStreamNotifyConstructor(
            const nsCString& url,
            const nsCString& target,
            const bool& post,
            const nsCString& buffer,
            const bool& file,
            NPError* result);

    PStreamNotifyChild*
    CallPStreamNotifyConstructor(
            PStreamNotifyChild* actor,
            const nsCString& url,
            const nsCString& target,
            const bool& post,
            const nsCString& buffer,
            const bool& file,
            NPError* result);

    bool
    SendNPN_InvalidateRect(const NPRect& rect);

    bool
    SendShow(
            const NPRect& updatedRect,
            const SurfaceDescriptor& newSurface,
            SurfaceDescriptor* prevSurface);

    PPluginSurfaceChild*
    SendPPluginSurfaceConstructor(
            const WindowsSharedMemoryHandle& handle,
            const gfxIntSize& size,
            const bool& transparent);

    PPluginSurfaceChild*
    SendPPluginSurfaceConstructor(
            PPluginSurfaceChild* actor,
            const WindowsSharedMemoryHandle& handle,
            const gfxIntSize& size,
            const bool& transparent);

    bool
    CallNPN_PushPopupsEnabledState(const bool& aState);

    bool
    CallNPN_PopPopupsEnabledState();

    bool
    CallNPN_GetValueForURL(
            const NPNURLVariable& variable,
            const nsCString& url,
            nsCString* value,
            NPError* result);

    bool
    CallNPN_SetValueForURL(
            const NPNURLVariable& variable,
            const nsCString& url,
            const nsCString& value,
            NPError* result);

    bool
    CallNPN_GetAuthenticationInfo(
            const nsCString& protocol_,
            const nsCString& host,
            const int32_t& port,
            const nsCString& scheme,
            const nsCString& realm,
            nsCString* username,
            nsCString* password,
            NPError* result);

    bool
    CallNPN_ConvertPoint(
            const double& sourceX,
            const bool& ignoreDestX,
            const double& sourceY,
            const bool& ignoreDestY,
            const NPCoordinateSpace& sourceSpace,
            const NPCoordinateSpace& destSpace,
            double* destX,
            double* destY,
            bool* result);

    bool
    SendRedrawPlugin();

    bool
    CallNPN_InitAsyncSurface(
            const gfxIntSize& size,
            const NPImageFormat& format,
            NPRemoteAsyncSurface* surfData,
            bool* result);

    bool
    SendNegotiatedCarbon();

    bool
    SendReleaseDXGISharedSurface(const DXGISharedSurfaceHandle& handle);

    PPluginScriptableObjectChild*
    SendPPluginScriptableObjectConstructor();

    PPluginScriptableObjectChild*
    SendPPluginScriptableObjectConstructor(PPluginScriptableObjectChild* actor);

    PPluginStreamChild*
    CallPPluginStreamConstructor(
            const nsCString& mimeType,
            const nsCString& target,
            NPError* result);

    PPluginStreamChild*
    CallPPluginStreamConstructor(
            PPluginStreamChild* actor,
            const nsCString& mimeType,
            const nsCString& target,
            NPError* result);

    bool
    CallPluginFocusChange(const bool& gotFocus);

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            const SurfaceDescriptor& __v,
            Message* __msg);

    bool
    Read(
            SurfaceDescriptor* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PPluginStreamChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PPluginStreamChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const NPRemoteAsyncSurface& __v,
            Message* __msg);

    bool
    Read(
            NPRemoteAsyncSurface* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            Shmem& __v,
            Message* __msg);

    bool
    Read(
            Shmem* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PPluginSurfaceChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PPluginSurfaceChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PPluginBackgroundDestroyerChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PPluginBackgroundDestroyerChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const IOSurfaceDescriptor& __v,
            Message* __msg);

    bool
    Read(
            IOSurfaceDescriptor* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PBrowserStreamChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PBrowserStreamChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PPluginInstanceChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PPluginInstanceChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const OptionalShmem& __v,
            Message* __msg);

    bool
    Read(
            OptionalShmem* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PStreamNotifyChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PStreamNotifyChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const AsyncSurfaceDescriptor& __v,
            Message* __msg);

    bool
    Read(
            AsyncSurfaceDescriptor* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PPluginScriptableObjectChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PPluginScriptableObjectChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
    // Sorted by pointer value
    InfallibleTArray<PPluginBackgroundDestroyerChild*> mManagedPPluginBackgroundDestroyerChild;
    // Sorted by pointer value
    InfallibleTArray<PPluginScriptableObjectChild*> mManagedPPluginScriptableObjectChild;
    // Sorted by pointer value
    InfallibleTArray<PBrowserStreamChild*> mManagedPBrowserStreamChild;
    // Sorted by pointer value
    InfallibleTArray<PPluginStreamChild*> mManagedPPluginStreamChild;
    // Sorted by pointer value
    InfallibleTArray<PStreamNotifyChild*> mManagedPStreamNotifyChild;
    // Sorted by pointer value
    InfallibleTArray<PPluginSurfaceChild*> mManagedPPluginSurfaceChild;
};


} // namespace plugins
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace plugins {
class PluginInstanceChild :
    public PPluginInstanceChild
{
    virtual bool
    AnswerNPP_SetWindow(const NPRemoteWindow& window);

    virtual bool
    AnswerNPP_GetValue_NPPVpluginWantsAllNetworkStreams(
            bool* value,
            NPError* result);

    virtual bool
    AnswerNPP_GetValue_NPPVpluginNeedsXEmbed(
            bool* value,
            NPError* result);

    virtual bool
    AnswerNPP_GetValue_NPPVpluginScriptableNPObject(
            PPluginScriptableObjectChild** value,
            NPError* result);

    virtual bool
    AnswerNPP_SetValue_NPNVprivateModeBool(
            const bool& value,
            NPError* result);

    virtual bool
    AnswerNPP_GetValue_NPPVpluginNativeAccessibleAtkPlugId(
            nsCString* plug_id,
            NPError* result);

    virtual bool
    AnswerNPP_HandleEvent(
            const NPRemoteEvent& event,
            int16_t* handled);

    virtual bool
    AnswerNPP_HandleEvent_Shmem(
            const NPRemoteEvent& event,
            Shmem& buffer,
            int16_t* handled,
            Shmem* rtnbuffer);

    virtual bool
    AnswerNPP_HandleEvent_IOSurface(
            const NPRemoteEvent& event,
            const uint32_t& surfaceid,
            int16_t* handled);

    virtual bool
    AnswerPaint(
            const NPRemoteEvent& event,
            int16_t* handled);

    virtual bool
    RecvWindowPosChanged(const NPRemoteEvent& event);

    virtual bool
    RecvContentsScaleFactorChanged(const double& aContentsScaleFactor);

    virtual bool
    RecvAsyncSetWindow(
            const gfxSurfaceType& surfaceType,
            const NPRemoteWindow& window);

    virtual bool
    RecvUpdateBackground(
            const SurfaceDescriptor& background,
            const nsIntRect& rect);

    virtual bool
    AnswerHandleTextEvent(
            const nsTextEvent& event,
            bool* handled);

    virtual bool
    AnswerHandleKeyEvent(
            const nsKeyEvent& event,
            bool* handled);

    virtual bool
    RecvNPP_DidComposite();

    virtual bool
    AnswerNPP_Destroy(NPError* rv);

    virtual bool
    AnswerSetPluginFocus();

    virtual bool
    AnswerUpdateWindow();

    virtual PStreamNotifyChild*
    AllocPStreamNotify(
            const nsCString& url,
            const nsCString& target,
            const bool& post,
            const nsCString& buffer,
            const bool& file,
            NPError* result);

    virtual bool
    DeallocPStreamNotify(PStreamNotifyChild* actor);

    virtual PPluginSurfaceChild*
    AllocPPluginSurface(
            const WindowsSharedMemoryHandle& handle,
            const gfxIntSize& size,
            const bool& transparent);

    virtual bool
    DeallocPPluginSurface(PPluginSurfaceChild* actor);

    virtual PPluginScriptableObjectChild*
    AllocPPluginScriptableObject();

    virtual bool
    DeallocPPluginScriptableObject(PPluginScriptableObjectChild* actor);

    virtual PBrowserStreamChild*
    AllocPBrowserStream(
            const nsCString& url,
            const uint32_t& length,
            const uint32_t& lastmodified,
            PStreamNotifyChild* notifyData,
            const nsCString& headers,
            const nsCString& mimeType,
            const bool& seekable,
            NPError* rv,
            uint16_t* stype);

    virtual bool
    DeallocPBrowserStream(PBrowserStreamChild* actor);

    virtual PPluginStreamChild*
    AllocPPluginStream(
            const nsCString& mimeType,
            const nsCString& target,
            NPError* result);

    virtual bool
    DeallocPPluginStream(PPluginStreamChild* actor);

    virtual PPluginBackgroundDestroyerChild*
    AllocPPluginBackgroundDestroyer();

    virtual bool
    DeallocPPluginBackgroundDestroyer(PPluginBackgroundDestroyerChild* actor);

    PluginInstanceChild();
    virtual ~PluginInstanceChild();
};
} // namespace plugins
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace plugins {
bool
PluginInstanceChild::AnswerNPP_SetWindow(const NPRemoteWindow& window)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_GetValue_NPPVpluginWantsAllNetworkStreams(
        bool* value,
        NPError* result)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_GetValue_NPPVpluginNeedsXEmbed(
        bool* value,
        NPError* result)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_GetValue_NPPVpluginScriptableNPObject(
        PPluginScriptableObjectChild** value,
        NPError* result)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_SetValue_NPNVprivateModeBool(
        const bool& value,
        NPError* result)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_GetValue_NPPVpluginNativeAccessibleAtkPlugId(
        nsCString* plug_id,
        NPError* result)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_HandleEvent(
        const NPRemoteEvent& event,
        int16_t* handled)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_HandleEvent_Shmem(
        const NPRemoteEvent& event,
        Shmem& buffer,
        int16_t* handled,
        Shmem* rtnbuffer)
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_HandleEvent_IOSurface(
        const NPRemoteEvent& event,
        const uint32_t& surfaceid,
        int16_t* handled)
{
    return false;
}

bool
PluginInstanceChild::AnswerPaint(
        const NPRemoteEvent& event,
        int16_t* handled)
{
    return false;
}

bool
PluginInstanceChild::RecvWindowPosChanged(const NPRemoteEvent& event)
{
    return false;
}

bool
PluginInstanceChild::RecvContentsScaleFactorChanged(const double& aContentsScaleFactor)
{
    return false;
}

bool
PluginInstanceChild::RecvAsyncSetWindow(
        const gfxSurfaceType& surfaceType,
        const NPRemoteWindow& window)
{
    return false;
}

bool
PluginInstanceChild::RecvUpdateBackground(
        const SurfaceDescriptor& background,
        const nsIntRect& rect)
{
    return false;
}

bool
PluginInstanceChild::AnswerHandleTextEvent(
        const nsTextEvent& event,
        bool* handled)
{
    return false;
}

bool
PluginInstanceChild::AnswerHandleKeyEvent(
        const nsKeyEvent& event,
        bool* handled)
{
    return false;
}

bool
PluginInstanceChild::RecvNPP_DidComposite()
{
    return false;
}

bool
PluginInstanceChild::AnswerNPP_Destroy(NPError* rv)
{
    return false;
}

bool
PluginInstanceChild::AnswerSetPluginFocus()
{
    return false;
}

bool
PluginInstanceChild::AnswerUpdateWindow()
{
    return false;
}

PStreamNotifyChild*
PluginInstanceChild::AllocPStreamNotify(
        const nsCString& url,
        const nsCString& target,
        const bool& post,
        const nsCString& buffer,
        const bool& file,
        NPError* result)
{
    return 0;
}

bool
PluginInstanceChild::DeallocPStreamNotify(PStreamNotifyChild* actor)
{
    return false;
}

PPluginSurfaceChild*
PluginInstanceChild::AllocPPluginSurface(
        const WindowsSharedMemoryHandle& handle,
        const gfxIntSize& size,
        const bool& transparent)
{
    return 0;
}

bool
PluginInstanceChild::DeallocPPluginSurface(PPluginSurfaceChild* actor)
{
    return false;
}

PPluginScriptableObjectChild*
PluginInstanceChild::AllocPPluginScriptableObject()
{
    return 0;
}

bool
PluginInstanceChild::DeallocPPluginScriptableObject(PPluginScriptableObjectChild* actor)
{
    return false;
}

PBrowserStreamChild*
PluginInstanceChild::AllocPBrowserStream(
        const nsCString& url,
        const uint32_t& length,
        const uint32_t& lastmodified,
        PStreamNotifyChild* notifyData,
        const nsCString& headers,
        const nsCString& mimeType,
        const bool& seekable,
        NPError* rv,
        uint16_t* stype)
{
    return 0;
}

bool
PluginInstanceChild::DeallocPBrowserStream(PBrowserStreamChild* actor)
{
    return false;
}

PPluginStreamChild*
PluginInstanceChild::AllocPPluginStream(
        const nsCString& mimeType,
        const nsCString& target,
        NPError* result)
{
    return 0;
}

bool
PluginInstanceChild::DeallocPPluginStream(PPluginStreamChild* actor)
{
    return false;
}

PPluginBackgroundDestroyerChild*
PluginInstanceChild::AllocPPluginBackgroundDestroyer()
{
    return 0;
}

bool
PluginInstanceChild::DeallocPPluginBackgroundDestroyer(PPluginBackgroundDestroyerChild* actor)
{
    return false;
}

PluginInstanceChild::PluginInstanceChild()
{
    MOZ_COUNT_CTOR(PluginInstanceChild);
}

PluginInstanceChild::~PluginInstanceChild()
{
    MOZ_COUNT_DTOR(PluginInstanceChild);
}

} // namespace plugins
} // namespace mozilla
#endif // if 0

#endif // ifndef PPluginInstanceChild_h
