//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PTestShellChild_h
#define PTestShellChild_h

#include "mozilla/ipc/PTestShell.h"
namespace mozilla {
namespace dom {
class PContentChild;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace ipc {
class PTestShellCommandChild;
} // namespace ipc
} // namespace mozilla

namespace mozilla {
namespace jsipc {
class PContextWrapperChild;
} // namespace jsipc
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace dom {
class PContentChild;
} // namespace dom
} // namespace mozilla


namespace mozilla {
namespace jsipc {
class PContextWrapperChild;
} // namespace jsipc
} // namespace mozilla


namespace mozilla {
namespace ipc {
class PTestShellCommandChild;
} // namespace ipc
} // namespace mozilla

namespace mozilla {
namespace ipc {


class /*NS_ABSTRACT_CLASS*/ PTestShellChild :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::dom::PContentChild;

    friend class mozilla::jsipc::PContextWrapperChild;

    friend class mozilla::ipc::PTestShellCommandChild;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::dom::PContentChild PContentChild;
    typedef mozilla::ipc::PTestShellCommandChild PTestShellCommandChild;
    typedef mozilla::jsipc::PContextWrapperChild PContextWrapperChild;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PTestShell::State State;

    virtual bool
    Recv__delete__();
    virtual bool
    RecvExecuteCommand(const nsString& aCommand) = 0;
    virtual bool
    RecvPTestShellCommandConstructor(
            PTestShellCommandChild* actor,
            const nsString& aCommand);
    virtual PTestShellCommandChild*
    AllocPTestShellCommand(const nsString& aCommand) = 0;
    virtual bool
    DeallocPTestShellCommand(PTestShellCommandChild* actor) = 0;
    virtual PContextWrapperChild*
    AllocPContextWrapper() = 0;
    virtual bool
    DeallocPContextWrapper(PContextWrapperChild* actor) = 0;

    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PTestShellChild();

    virtual ~PTestShellChild();

    PContentChild*
    Manager() const;

    void
    ManagedPTestShellCommandChild(InfallibleTArray<PTestShellCommandChild*>& aArr) const;
    const InfallibleTArray<PTestShellCommandChild*>&
    ManagedPTestShellCommandChild() const;

    void
    ManagedPContextWrapperChild(InfallibleTArray<PContextWrapperChild*>& aArr) const;
    const InfallibleTArray<PContextWrapperChild*>&
    ManagedPContextWrapperChild() const;

    PTestShell::State
    state();

    PContextWrapperChild*
    SendPContextWrapperConstructor();

    PContextWrapperChild*
    SendPContextWrapperConstructor(PContextWrapperChild* actor);

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            PTestShellChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PTestShellChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PContextWrapperChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PContextWrapperChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PTestShellCommandChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PTestShellCommandChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
    // Sorted by pointer value
    InfallibleTArray<PTestShellCommandChild*> mManagedPTestShellCommandChild;
    // Sorted by pointer value
    InfallibleTArray<PContextWrapperChild*> mManagedPContextWrapperChild;
};


} // namespace ipc
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace ipc {
class TestShellChild :
    public PTestShellChild
{
    virtual bool
    RecvExecuteCommand(const nsString& aCommand);

    virtual PTestShellCommandChild*
    AllocPTestShellCommand(const nsString& aCommand);

    virtual bool
    DeallocPTestShellCommand(PTestShellCommandChild* actor);

    virtual PContextWrapperChild*
    AllocPContextWrapper();

    virtual bool
    DeallocPContextWrapper(PContextWrapperChild* actor);

    TestShellChild();
    virtual ~TestShellChild();
};
} // namespace ipc
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace ipc {
bool
TestShellChild::RecvExecuteCommand(const nsString& aCommand)
{
    return false;
}

PTestShellCommandChild*
TestShellChild::AllocPTestShellCommand(const nsString& aCommand)
{
    return 0;
}

bool
TestShellChild::DeallocPTestShellCommand(PTestShellCommandChild* actor)
{
    return false;
}

PContextWrapperChild*
TestShellChild::AllocPContextWrapper()
{
    return 0;
}

bool
TestShellChild::DeallocPContextWrapper(PContextWrapperChild* actor)
{
    return false;
}

TestShellChild::TestShellChild()
{
    MOZ_COUNT_CTOR(TestShellChild);
}

TestShellChild::~TestShellChild()
{
    MOZ_COUNT_DTOR(TestShellChild);
}

} // namespace ipc
} // namespace mozilla
#endif // if 0

#endif // ifndef PTestShellChild_h
