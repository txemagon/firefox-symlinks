//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PNeckoParent_h
#define PNeckoParent_h

#include "mozilla/net/PNecko.h"
namespace mozilla {
namespace dom {
class PContentParent;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace net {
class PHttpChannelParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {
class PCookieServiceParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace dom {
class PBrowserParent;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace net {
class PWyciwygChannelParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {
class PFTPChannelParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {
class PWebSocketParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {
class PTCPSocketParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {
class PRemoteOpenFileParent;
} // namespace net
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace net {
class PHttpChannelParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace net {
class PWyciwygChannelParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace net {
class PTCPSocketParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace net {
class PRemoteOpenFileParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace net {
class PFTPChannelParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace dom {
class PContentParent;
} // namespace dom
} // namespace mozilla


namespace mozilla {
namespace net {
class PWebSocketParent;
} // namespace net
} // namespace mozilla


namespace mozilla {
namespace net {
class PCookieServiceParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {


class /*NS_ABSTRACT_CLASS*/ PNeckoParent :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::net::PHttpChannelParent;

    friend class mozilla::net::PWyciwygChannelParent;

    friend class mozilla::net::PTCPSocketParent;

    friend class mozilla::net::PRemoteOpenFileParent;

    friend class mozilla::net::PFTPChannelParent;

    friend class mozilla::dom::PContentParent;

    friend class mozilla::net::PWebSocketParent;

    friend class mozilla::net::PCookieServiceParent;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::ipc::GenericURIParams GenericURIParams;
    typedef mozilla::ipc::JARURIParams JARURIParams;
    typedef mozilla::ipc::OptionalURIParams OptionalURIParams;
    typedef IPC::SerializedLoadContext SerializedLoadContext;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::ipc::SimpleURIParams SimpleURIParams;
    typedef mozilla::ipc::StandardURLParams StandardURLParams;
    typedef mozilla::ipc::StandardURLSegment StandardURLSegment;
    typedef mozilla::ipc::URIParams URIParams;
    typedef mozilla::void_t void_t;
    typedef mozilla::dom::PContentParent PContentParent;
    typedef mozilla::net::PHttpChannelParent PHttpChannelParent;
    typedef mozilla::net::PCookieServiceParent PCookieServiceParent;
    typedef mozilla::dom::PBrowserParent PBrowserParent;
    typedef mozilla::net::PWyciwygChannelParent PWyciwygChannelParent;
    typedef mozilla::net::PFTPChannelParent PFTPChannelParent;
    typedef mozilla::net::PWebSocketParent PWebSocketParent;
    typedef mozilla::net::PTCPSocketParent PTCPSocketParent;
    typedef mozilla::net::PRemoteOpenFileParent PRemoteOpenFileParent;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PNecko::State State;

    virtual bool
    Recv__delete__();
    virtual bool
    RecvPCookieServiceConstructor(PCookieServiceParent* actor);
    virtual bool
    RecvPHttpChannelConstructor(
            PHttpChannelParent* actor,
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);
    virtual bool
    RecvPWyciwygChannelConstructor(PWyciwygChannelParent* actor);
    virtual bool
    RecvPFTPChannelConstructor(
            PFTPChannelParent* actor,
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);
    virtual bool
    RecvPWebSocketConstructor(
            PWebSocketParent* actor,
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);
    virtual bool
    RecvPTCPSocketConstructor(
            PTCPSocketParent* actor,
            const nsString& host,
            const uint16_t& port,
            const bool& useSSL,
            const nsString& binaryType,
            PBrowserParent* browser);
    virtual bool
    RecvPRemoteOpenFileConstructor(
            PRemoteOpenFileParent* actor,
            const URIParams& fileuri,
            PBrowserParent* browser);
    virtual bool
    RecvHTMLDNSPrefetch(
            const nsString& hostname,
            const uint16_t& flags) = 0;
    virtual bool
    RecvCancelHTMLDNSPrefetch(
            const nsString& hostname,
            const uint16_t& flags,
            const nsresult& reason) = 0;
    virtual PCookieServiceParent*
    AllocPCookieService() = 0;
    virtual bool
    DeallocPCookieService(PCookieServiceParent* actor) = 0;
    virtual PHttpChannelParent*
    AllocPHttpChannel(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext) = 0;
    virtual bool
    DeallocPHttpChannel(PHttpChannelParent* actor) = 0;
    virtual PWyciwygChannelParent*
    AllocPWyciwygChannel() = 0;
    virtual bool
    DeallocPWyciwygChannel(PWyciwygChannelParent* actor) = 0;
    virtual PFTPChannelParent*
    AllocPFTPChannel(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext) = 0;
    virtual bool
    DeallocPFTPChannel(PFTPChannelParent* actor) = 0;
    virtual PWebSocketParent*
    AllocPWebSocket(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext) = 0;
    virtual bool
    DeallocPWebSocket(PWebSocketParent* actor) = 0;
    virtual PTCPSocketParent*
    AllocPTCPSocket(
            const nsString& host,
            const uint16_t& port,
            const bool& useSSL,
            const nsString& binaryType,
            PBrowserParent* browser) = 0;
    virtual bool
    DeallocPTCPSocket(PTCPSocketParent* actor) = 0;
    virtual PRemoteOpenFileParent*
    AllocPRemoteOpenFile(
            const URIParams& fileuri,
            PBrowserParent* browser) = 0;
    virtual bool
    DeallocPRemoteOpenFile(PRemoteOpenFileParent* actor) = 0;

    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PNeckoParent();

    virtual ~PNeckoParent();

    PContentParent*
    Manager() const;

    void
    ManagedPHttpChannelParent(InfallibleTArray<PHttpChannelParent*>& aArr) const;
    const InfallibleTArray<PHttpChannelParent*>&
    ManagedPHttpChannelParent() const;

    void
    ManagedPCookieServiceParent(InfallibleTArray<PCookieServiceParent*>& aArr) const;
    const InfallibleTArray<PCookieServiceParent*>&
    ManagedPCookieServiceParent() const;

    void
    ManagedPWyciwygChannelParent(InfallibleTArray<PWyciwygChannelParent*>& aArr) const;
    const InfallibleTArray<PWyciwygChannelParent*>&
    ManagedPWyciwygChannelParent() const;

    void
    ManagedPFTPChannelParent(InfallibleTArray<PFTPChannelParent*>& aArr) const;
    const InfallibleTArray<PFTPChannelParent*>&
    ManagedPFTPChannelParent() const;

    void
    ManagedPWebSocketParent(InfallibleTArray<PWebSocketParent*>& aArr) const;
    const InfallibleTArray<PWebSocketParent*>&
    ManagedPWebSocketParent() const;

    void
    ManagedPTCPSocketParent(InfallibleTArray<PTCPSocketParent*>& aArr) const;
    const InfallibleTArray<PTCPSocketParent*>&
    ManagedPTCPSocketParent() const;

    void
    ManagedPRemoteOpenFileParent(InfallibleTArray<PRemoteOpenFileParent*>& aArr) const;
    const InfallibleTArray<PRemoteOpenFileParent*>&
    ManagedPRemoteOpenFileParent() const;

    PNecko::State
    state();

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            PHttpChannelParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PHttpChannelParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PWyciwygChannelParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PWyciwygChannelParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PBrowserParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PBrowserParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PTCPSocketParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PTCPSocketParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PRemoteOpenFileParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PRemoteOpenFileParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const StandardURLSegment& __v,
            Message* __msg);

    bool
    Read(
            StandardURLSegment* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const StandardURLParams& __v,
            Message* __msg);

    bool
    Read(
            StandardURLParams* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PNeckoParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PNeckoParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const JARURIParams& __v,
            Message* __msg);

    bool
    Read(
            JARURIParams* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const URIParams& __v,
            Message* __msg);

    bool
    Read(
            URIParams* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PFTPChannelParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PFTPChannelParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PWebSocketParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PWebSocketParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const SimpleURIParams& __v,
            Message* __msg);

    bool
    Read(
            SimpleURIParams* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PCookieServiceParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PCookieServiceParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const GenericURIParams& __v,
            Message* __msg);

    bool
    Read(
            GenericURIParams* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
    // Sorted by pointer value
    InfallibleTArray<PHttpChannelParent*> mManagedPHttpChannelParent;
    // Sorted by pointer value
    InfallibleTArray<PCookieServiceParent*> mManagedPCookieServiceParent;
    // Sorted by pointer value
    InfallibleTArray<PWyciwygChannelParent*> mManagedPWyciwygChannelParent;
    // Sorted by pointer value
    InfallibleTArray<PFTPChannelParent*> mManagedPFTPChannelParent;
    // Sorted by pointer value
    InfallibleTArray<PWebSocketParent*> mManagedPWebSocketParent;
    // Sorted by pointer value
    InfallibleTArray<PTCPSocketParent*> mManagedPTCPSocketParent;
    // Sorted by pointer value
    InfallibleTArray<PRemoteOpenFileParent*> mManagedPRemoteOpenFileParent;
};


} // namespace net
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace net {
class NeckoParent :
    public PNeckoParent
{
    virtual bool
    RecvHTMLDNSPrefetch(
            const nsString& hostname,
            const uint16_t& flags);

    virtual bool
    RecvCancelHTMLDNSPrefetch(
            const nsString& hostname,
            const uint16_t& flags,
            const nsresult& reason);

    virtual PCookieServiceParent*
    AllocPCookieService();

    virtual bool
    DeallocPCookieService(PCookieServiceParent* actor);

    virtual PHttpChannelParent*
    AllocPHttpChannel(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);

    virtual bool
    DeallocPHttpChannel(PHttpChannelParent* actor);

    virtual PWyciwygChannelParent*
    AllocPWyciwygChannel();

    virtual bool
    DeallocPWyciwygChannel(PWyciwygChannelParent* actor);

    virtual PFTPChannelParent*
    AllocPFTPChannel(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);

    virtual bool
    DeallocPFTPChannel(PFTPChannelParent* actor);

    virtual PWebSocketParent*
    AllocPWebSocket(
            PBrowserParent* browser,
            const SerializedLoadContext& loadContext);

    virtual bool
    DeallocPWebSocket(PWebSocketParent* actor);

    virtual PTCPSocketParent*
    AllocPTCPSocket(
            const nsString& host,
            const uint16_t& port,
            const bool& useSSL,
            const nsString& binaryType,
            PBrowserParent* browser);

    virtual bool
    DeallocPTCPSocket(PTCPSocketParent* actor);

    virtual PRemoteOpenFileParent*
    AllocPRemoteOpenFile(
            const URIParams& fileuri,
            PBrowserParent* browser);

    virtual bool
    DeallocPRemoteOpenFile(PRemoteOpenFileParent* actor);

    NeckoParent();
    virtual ~NeckoParent();
};
} // namespace net
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace net {
bool
NeckoParent::RecvHTMLDNSPrefetch(
        const nsString& hostname,
        const uint16_t& flags)
{
    return false;
}

bool
NeckoParent::RecvCancelHTMLDNSPrefetch(
        const nsString& hostname,
        const uint16_t& flags,
        const nsresult& reason)
{
    return false;
}

PCookieServiceParent*
NeckoParent::AllocPCookieService()
{
    return 0;
}

bool
NeckoParent::DeallocPCookieService(PCookieServiceParent* actor)
{
    return false;
}

PHttpChannelParent*
NeckoParent::AllocPHttpChannel(
        PBrowserParent* browser,
        const SerializedLoadContext& loadContext)
{
    return 0;
}

bool
NeckoParent::DeallocPHttpChannel(PHttpChannelParent* actor)
{
    return false;
}

PWyciwygChannelParent*
NeckoParent::AllocPWyciwygChannel()
{
    return 0;
}

bool
NeckoParent::DeallocPWyciwygChannel(PWyciwygChannelParent* actor)
{
    return false;
}

PFTPChannelParent*
NeckoParent::AllocPFTPChannel(
        PBrowserParent* browser,
        const SerializedLoadContext& loadContext)
{
    return 0;
}

bool
NeckoParent::DeallocPFTPChannel(PFTPChannelParent* actor)
{
    return false;
}

PWebSocketParent*
NeckoParent::AllocPWebSocket(
        PBrowserParent* browser,
        const SerializedLoadContext& loadContext)
{
    return 0;
}

bool
NeckoParent::DeallocPWebSocket(PWebSocketParent* actor)
{
    return false;
}

PTCPSocketParent*
NeckoParent::AllocPTCPSocket(
        const nsString& host,
        const uint16_t& port,
        const bool& useSSL,
        const nsString& binaryType,
        PBrowserParent* browser)
{
    return 0;
}

bool
NeckoParent::DeallocPTCPSocket(PTCPSocketParent* actor)
{
    return false;
}

PRemoteOpenFileParent*
NeckoParent::AllocPRemoteOpenFile(
        const URIParams& fileuri,
        PBrowserParent* browser)
{
    return 0;
}

bool
NeckoParent::DeallocPRemoteOpenFile(PRemoteOpenFileParent* actor)
{
    return false;
}

NeckoParent::NeckoParent()
{
    MOZ_COUNT_CTOR(NeckoParent);
}

NeckoParent::~NeckoParent()
{
    MOZ_COUNT_DTOR(NeckoParent);
}

} // namespace net
} // namespace mozilla
#endif // if 0

#endif // ifndef PNeckoParent_h
