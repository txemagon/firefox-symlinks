//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PTCPSocketParent_h
#define PTCPSocketParent_h

#include "mozilla/net/PTCPSocket.h"
namespace mozilla {
namespace net {
class PNeckoParent;
} // namespace net
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace net {
class PNeckoParent;
} // namespace net
} // namespace mozilla

namespace mozilla {
namespace net {


class /*NS_ABSTRACT_CLASS*/ PTCPSocketParent :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::net::PNeckoParent;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::void_t void_t;
    typedef mozilla::net::PNeckoParent PNeckoParent;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PTCPSocket::State State;

    virtual bool
    RecvData(const SendableData& data) = 0;
    virtual bool
    RecvSuspend() = 0;
    virtual bool
    RecvResume() = 0;
    virtual bool
    RecvClose() = 0;
    virtual bool
    RecvRequestDelete() = 0;

    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PTCPSocketParent();

    virtual ~PTCPSocketParent();

    PNeckoParent*
    Manager() const;

    PTCPSocket::State
    state();

    bool
    SendCallback(
            const nsString& type,
            const CallbackData& data,
            const nsString& readyState,
            const uint32_t& bufferedAmount) NS_WARN_UNUSED_RESULT;

    static bool
    Send__delete__(PTCPSocketParent* actor) NS_WARN_UNUSED_RESULT;

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            const SendableData& __v,
            Message* __msg);

    bool
    Read(
            SendableData* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PTCPSocketParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PTCPSocketParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const CallbackData& __v,
            Message* __msg);

    bool
    Read(
            CallbackData* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const JSError& __v,
            Message* __msg);

    bool
    Read(
            JSError* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
};


} // namespace net
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace net {
class TCPSocketParent :
    public PTCPSocketParent
{
    virtual bool
    RecvData(const SendableData& data);

    virtual bool
    RecvSuspend();

    virtual bool
    RecvResume();

    virtual bool
    RecvClose();

    virtual bool
    RecvRequestDelete();

    TCPSocketParent();
    virtual ~TCPSocketParent();
};
} // namespace net
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace net {
bool
TCPSocketParent::RecvData(const SendableData& data)
{
    return false;
}

bool
TCPSocketParent::RecvSuspend()
{
    return false;
}

bool
TCPSocketParent::RecvResume()
{
    return false;
}

bool
TCPSocketParent::RecvClose()
{
    return false;
}

bool
TCPSocketParent::RecvRequestDelete()
{
    return false;
}

TCPSocketParent::TCPSocketParent()
{
    MOZ_COUNT_CTOR(TCPSocketParent);
}

TCPSocketParent::~TCPSocketParent()
{
    MOZ_COUNT_DTOR(TCPSocketParent);
}

} // namespace net
} // namespace mozilla
#endif // if 0

#endif // ifndef PTCPSocketParent_h
