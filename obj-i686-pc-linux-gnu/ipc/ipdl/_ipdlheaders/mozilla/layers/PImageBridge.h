//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PImageBridge_h
#define PImageBridge_h

#include "mozilla/Attributes.h"
#include "base/basictypes.h"
#include "prtime.h"
#include "nscore.h"
#include "IPCMessageStart.h"
#include "ipc/IPCMessageUtils.h"
#include "nsAutoPtr.h"
#include "nsStringGlue.h"
#include "nsTArray.h"
#include "nsIFile.h"
#include "mozilla/ipc/ProtocolUtils.h"
#include "mozilla/layers/LayersSurfaces.h"

namespace mozilla {
namespace dom {
class PContentParent;
} // namespace dom
} // namespace mozilla

//-----------------------------------------------------------------------------
// Code common to PImageBridgeChild and PImageBridgeParent
//
namespace mozilla {
namespace layers {
namespace PImageBridge {

bool
Open(mozilla::dom::PContentParent* opener);

enum State {
    __Dead,
    __Null,
    __Error,
    __Dying,
    __Start = __Null
};

enum MessageType {
    PImageBridgeStart = PImageBridgeMsgStart << 16,
    PImageBridgePreStart = (PImageBridgeMsgStart << 16) - 1,
    Msg_PGrallocBufferConstructor__ID,
    Reply_PGrallocBufferConstructor__ID,
    Msg_Stop__ID,
    Reply_Stop__ID,
    Msg_PImageContainerConstructor__ID,
    Reply_PImageContainerConstructor__ID,
    PImageBridgeEnd
};

bool
Transition(
        State from,
        mozilla::ipc::Trigger trigger,
        State* next);

class Msg_PGrallocBufferConstructor :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Msg_PGrallocBufferConstructor__ID
    };
    Msg_PGrallocBufferConstructor() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Msg_PGrallocBufferConstructor")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Msg_PGrallocBufferConstructor(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};

class Reply_PGrallocBufferConstructor :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Reply_PGrallocBufferConstructor__ID
    };
    Reply_PGrallocBufferConstructor() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Reply_PGrallocBufferConstructor")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Reply_PGrallocBufferConstructor(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};

class Msg_Stop :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Msg_Stop__ID
    };
    Msg_Stop() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Msg_Stop")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Msg_Stop(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};

class Reply_Stop :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Reply_Stop__ID
    };
    Reply_Stop() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Reply_Stop")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Reply_Stop(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};

class Msg_PImageContainerConstructor :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Msg_PImageContainerConstructor__ID
    };
    Msg_PImageContainerConstructor() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Msg_PImageContainerConstructor")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Msg_PImageContainerConstructor(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};

class Reply_PImageContainerConstructor :
    public IPC::Message
{
private:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::layers::FrameMetrics FrameMetrics;
    typedef mozilla::GraphicsFilterType GraphicsFilterType;
    typedef mozilla::layers::MagicGrallocBufferHandle MagicGrallocBufferHandle;
    typedef mozilla::layers::MaybeMagicGrallocBufferHandle MaybeMagicGrallocBufferHandle;
    typedef mozilla::layers::RGBImage RGBImage;
    typedef mozilla::layers::SharedImage SharedImage;
    typedef mozilla::layers::SharedImageID SharedImageID;
    typedef mozilla::layers::SharedTextureDescriptor SharedTextureDescriptor;
    typedef mozilla::gl::SharedTextureHandle SharedTextureHandle;
    typedef mozilla::gl::GLContext::SharedTextureShareType SharedTextureShareType;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::layers::SurfaceDescriptor SurfaceDescriptor;
    typedef mozilla::layers::SurfaceDescriptorD3D10 SurfaceDescriptorD3D10;
    typedef mozilla::layers::SurfaceDescriptorGralloc SurfaceDescriptorGralloc;
    typedef mozilla::layers::SurfaceDescriptorX11 SurfaceDescriptorX11;
    typedef mozilla::WindowsHandle WindowsHandle;
    typedef mozilla::layers::YCbCrImage YCbCrImage;
    typedef mozilla::layers::YUVImage YUVImage;
    typedef mozilla::gfxContentType gfxContentType;
    typedef mozilla::null_t null_t;

public:
    enum {
        ID = Reply_PImageContainerConstructor__ID
    };
    Reply_PImageContainerConstructor() :
        IPC::Message(MSG_ROUTING_NONE, ID, PRIORITY_NORMAL, COMPRESSION_NONE, "PImageBridge::Reply_PImageContainerConstructor")
    {
    }

    void
    Log(
            const std::string& __pfx,
            FILE* __outf) const
    {
        std::string __logmsg;
        StringAppendF((&(__logmsg)), "[time:%" PRId64 "][%d]", PR_Now(), base::GetCurrentProcId());
        (__logmsg).append(__pfx);
        (__logmsg).append("Reply_PImageContainerConstructor(");

        (__logmsg).append("[TODO])\n");
        fputs((__logmsg).c_str(), __outf);
    }
};



} // namespace PImageBridge
} // namespace layers
} // namespace mozilla

#endif // ifndef PImageBridge_h
