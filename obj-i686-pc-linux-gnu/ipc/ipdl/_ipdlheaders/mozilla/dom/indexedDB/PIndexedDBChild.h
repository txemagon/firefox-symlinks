//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PIndexedDBChild_h
#define PIndexedDBChild_h

#include "mozilla/dom/indexedDB/PIndexedDB.h"
namespace mozilla {
namespace dom {
class PBrowserChild;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
class PContentChild;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
namespace indexedDB {
class PIndexedDBDatabaseChild;
} // namespace indexedDB
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
namespace indexedDB {
class PIndexedDBDeleteDatabaseRequestChild;
} // namespace indexedDB
} // namespace dom
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace dom {
namespace indexedDB {
class PIndexedDBDatabaseChild;
} // namespace indexedDB
} // namespace dom
} // namespace mozilla


namespace mozilla {
namespace dom {
class PContentChild;
} // namespace dom
} // namespace mozilla


namespace mozilla {
namespace dom {
namespace indexedDB {
class PIndexedDBDeleteDatabaseRequestChild;
} // namespace indexedDB
} // namespace dom
} // namespace mozilla


namespace mozilla {
namespace dom {
class PBrowserChild;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
namespace indexedDB {


class /*NS_ABSTRACT_CLASS*/ PIndexedDBChild :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::dom::indexedDB::PIndexedDBDatabaseChild;

    friend class mozilla::dom::PContentChild;

    friend class mozilla::dom::indexedDB::PIndexedDBDeleteDatabaseRequestChild;

    friend class mozilla::dom::PBrowserChild;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::dom::PBrowserChild PBrowserChild;
    typedef mozilla::dom::PContentChild PContentChild;
    typedef mozilla::dom::indexedDB::PIndexedDBDatabaseChild PIndexedDBDatabaseChild;
    typedef mozilla::dom::indexedDB::PIndexedDBDeleteDatabaseRequestChild PIndexedDBDeleteDatabaseRequestChild;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PIndexedDB::State State;

    virtual PIndexedDBDatabaseChild*
    AllocPIndexedDBDatabase(
            const nsString& name,
            const uint64_t& version) = 0;
    virtual bool
    DeallocPIndexedDBDatabase(PIndexedDBDatabaseChild* actor) = 0;
    virtual PIndexedDBDeleteDatabaseRequestChild*
    AllocPIndexedDBDeleteDatabaseRequest(const nsString& name) = 0;
    virtual bool
    DeallocPIndexedDBDeleteDatabaseRequest(PIndexedDBDeleteDatabaseRequestChild* actor) = 0;

    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PIndexedDBChild();

    virtual ~PIndexedDBChild();

    void
    ManagedPIndexedDBDatabaseChild(InfallibleTArray<PIndexedDBDatabaseChild*>& aArr) const;
    const InfallibleTArray<PIndexedDBDatabaseChild*>&
    ManagedPIndexedDBDatabaseChild() const;

    void
    ManagedPIndexedDBDeleteDatabaseRequestChild(InfallibleTArray<PIndexedDBDeleteDatabaseRequestChild*>& aArr) const;
    const InfallibleTArray<PIndexedDBDeleteDatabaseRequestChild*>&
    ManagedPIndexedDBDeleteDatabaseRequestChild() const;

    PIndexedDB::State
    state();

    static bool
    Send__delete__(PIndexedDBChild* actor);

    PIndexedDBDatabaseChild*
    SendPIndexedDBDatabaseConstructor(
            const nsString& name,
            const uint64_t& version);

    PIndexedDBDatabaseChild*
    SendPIndexedDBDatabaseConstructor(
            PIndexedDBDatabaseChild* actor,
            const nsString& name,
            const uint64_t& version);

    PIndexedDBDeleteDatabaseRequestChild*
    SendPIndexedDBDeleteDatabaseRequestConstructor(const nsString& name);

    PIndexedDBDeleteDatabaseRequestChild*
    SendPIndexedDBDeleteDatabaseRequestConstructor(
            PIndexedDBDeleteDatabaseRequestChild* actor,
            const nsString& name);

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            PIndexedDBDatabaseChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PIndexedDBDatabaseChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PIndexedDBDeleteDatabaseRequestChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PIndexedDBDeleteDatabaseRequestChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PIndexedDBChild* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PIndexedDBChild** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
    // Sorted by pointer value
    InfallibleTArray<PIndexedDBDatabaseChild*> mManagedPIndexedDBDatabaseChild;
    // Sorted by pointer value
    InfallibleTArray<PIndexedDBDeleteDatabaseRequestChild*> mManagedPIndexedDBDeleteDatabaseRequestChild;
};


} // namespace indexedDB
} // namespace dom
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace dom {
namespace indexedDB {
class IndexedDBChild :
    public PIndexedDBChild
{
    virtual PIndexedDBDatabaseChild*
    AllocPIndexedDBDatabase(
            const nsString& name,
            const uint64_t& version);

    virtual bool
    DeallocPIndexedDBDatabase(PIndexedDBDatabaseChild* actor);

    virtual PIndexedDBDeleteDatabaseRequestChild*
    AllocPIndexedDBDeleteDatabaseRequest(const nsString& name);

    virtual bool
    DeallocPIndexedDBDeleteDatabaseRequest(PIndexedDBDeleteDatabaseRequestChild* actor);

    IndexedDBChild();
    virtual ~IndexedDBChild();
};
} // namespace indexedDB
} // namespace dom
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace dom {
namespace indexedDB {
PIndexedDBDatabaseChild*
IndexedDBChild::AllocPIndexedDBDatabase(
        const nsString& name,
        const uint64_t& version)
{
    return 0;
}

bool
IndexedDBChild::DeallocPIndexedDBDatabase(PIndexedDBDatabaseChild* actor)
{
    return false;
}

PIndexedDBDeleteDatabaseRequestChild*
IndexedDBChild::AllocPIndexedDBDeleteDatabaseRequest(const nsString& name)
{
    return 0;
}

bool
IndexedDBChild::DeallocPIndexedDBDeleteDatabaseRequest(PIndexedDBDeleteDatabaseRequestChild* actor)
{
    return false;
}

IndexedDBChild::IndexedDBChild()
{
    MOZ_COUNT_CTOR(IndexedDBChild);
}

IndexedDBChild::~IndexedDBChild()
{
    MOZ_COUNT_DTOR(IndexedDBChild);
}

} // namespace indexedDB
} // namespace dom
} // namespace mozilla
#endif // if 0

#endif // ifndef PIndexedDBChild_h
