//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef PDeviceStorageRequestParent_h
#define PDeviceStorageRequestParent_h

#include "mozilla/dom/devicestorage/PDeviceStorageRequest.h"
namespace mozilla {
namespace dom {
class PBlobParent;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
class PContentParent;
} // namespace dom
} // namespace mozilla

#ifdef DEBUG
#include "prenv.h"
#endif // DEBUG
#include "base/id_map.h"
#include "mozilla/ipc/RPCChannel.h"


namespace mozilla {
namespace dom {
class PContentParent;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
namespace devicestorage {


class /*NS_ABSTRACT_CLASS*/ PDeviceStorageRequestParent :
    protected mozilla::ipc::RPCChannel::RPCListener,
    protected mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>
{
    friend class mozilla::dom::PContentParent;

protected:
    typedef mozilla::ipc::ActorHandle ActorHandle;
    typedef mozilla::dom::devicestorage::BlobResponse BlobResponse;
    typedef mozilla::dom::devicestorage::DeviceStorageFileValue DeviceStorageFileValue;
    typedef mozilla::dom::devicestorage::DeviceStorageResponseValue DeviceStorageResponseValue;
    typedef mozilla::dom::devicestorage::EnumerationResponse EnumerationResponse;
    typedef mozilla::dom::devicestorage::ErrorResponse ErrorResponse;
    typedef mozilla::ipc::FileDescriptor FileDescriptor;
    typedef mozilla::ipc::Shmem Shmem;
    typedef mozilla::dom::devicestorage::StatStorageResponse StatStorageResponse;
    typedef mozilla::dom::devicestorage::SuccessResponse SuccessResponse;
    typedef mozilla::dom::PBlobParent PBlobParent;
    typedef mozilla::dom::PContentParent PContentParent;
    typedef base::ProcessId ProcessId;
    typedef mozilla::ipc::ProtocolId ProtocolId;
    typedef mozilla::ipc::Transport Transport;
    typedef mozilla::ipc::TransportDescriptor TransportDescriptor;

    typedef PDeviceStorageRequest::State State;


    virtual void
    ActorDestroy(ActorDestroyReason why);

public:
    typedef IPC::Message Message;
    typedef mozilla::ipc::RPCChannel Channel;
    typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
    typedef base::ProcessHandle ProcessHandle;
    typedef mozilla::ipc::AsyncChannel AsyncChannel;
    typedef mozilla::ipc::SharedMemory SharedMemory;
    typedef mozilla::ipc::Trigger Trigger;

public:
    PDeviceStorageRequestParent();

    virtual ~PDeviceStorageRequestParent();

    PContentParent*
    Manager() const;

    PDeviceStorageRequest::State
    state();

    static bool
    Send__delete__(
            PDeviceStorageRequestParent* actor,
            const DeviceStorageResponseValue& response) NS_WARN_UNUSED_RESULT;

    virtual int32_t
    Register(ChannelListener* aRouted);
    virtual int32_t
    RegisterID(
            ChannelListener* aRouted,
            int32_t aId);
    virtual ChannelListener*
    Lookup(int32_t aId);
    virtual void
    Unregister(int32_t aId);
    virtual void
    RemoveManagee(
            int32_t aProtocolId,
            ChannelListener* aListener);
    virtual Shmem::SharedMemory*
    CreateSharedMemory(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            bool aUnsafe,
            Shmem::id_t* aId);
    virtual bool
    AdoptSharedMemory(
            Shmem::SharedMemory* segment,
            Shmem::id_t* aId);
    virtual Shmem::SharedMemory*
    LookupSharedMemory(Shmem::id_t aId);
    virtual bool
    IsTrackingSharedMemory(Shmem::SharedMemory* segment);
    virtual bool
    DestroySharedMemory(Shmem& shmem);
    virtual ProcessHandle
    OtherProcess() const;
    virtual AsyncChannel*
    GetIPCChannel();

    virtual Result
    OnMessageReceived(const Message& __msg);

    virtual Result
    OnMessageReceived(
            const Message& __msg,
            Message*& __reply);

    virtual Result
    OnCallReceived(
            const Message& __msg,
            Message*& __reply);

    void
    OnProcessingError(Result code);

    int32_t
    GetProtocolTypeId();

    bool
    OnReplyTimeout();

    void
    OnChannelClose();

    void
    OnChannelError();

    void
    OnChannelConnected(int32_t pid);

    // Methods for managing shmem
    bool
    AllocShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AllocUnsafeShmem(
            size_t aSize,
            Shmem::SharedMemory::SharedMemoryType aType,
            Shmem* aMem);

    bool
    AdoptShmem(
            Shmem& aMem,
            Shmem* aOutMem);

    bool
    DeallocShmem(Shmem& aMem);

private:
    virtual void
    FatalError(const char* const msg) const;

    void
    DestroySubtree(ActorDestroyReason why);

    void
    DeallocSubtree();

    template<typename T>
    void
    Write(
            const T& __v,
            Message* __msg)
    {
        IPC::WriteParam(__msg, __v);
    }

    template<typename T>
    bool
    Read(
            T* __v,
            const Message* __msg,
            void** __iter)
    {
        return IPC::ReadParam(__msg, __iter, __v);
    }

    void
    Write(
            const DeviceStorageFileValue& __v,
            Message* __msg);

    bool
    Read(
            DeviceStorageFileValue* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const SuccessResponse& __v,
            Message* __msg);

    bool
    Read(
            SuccessResponse* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const EnumerationResponse& __v,
            Message* __msg);

    bool
    Read(
            EnumerationResponse* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PDeviceStorageRequestParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PDeviceStorageRequestParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const DeviceStorageResponseValue& __v,
            Message* __msg);

    bool
    Read(
            DeviceStorageResponseValue* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const InfallibleTArray<DeviceStorageFileValue>& __v,
            Message* __msg);

    bool
    Read(
            InfallibleTArray<DeviceStorageFileValue>* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const ErrorResponse& __v,
            Message* __msg);

    bool
    Read(
            ErrorResponse* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            PBlobParent* __v,
            Message* __msg,
            bool __nullable);

    bool
    Read(
            PBlobParent** __v,
            const Message* __msg,
            void** __iter,
            bool __nullable) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const StatStorageResponse& __v,
            Message* __msg);

    bool
    Read(
            StatStorageResponse* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    void
    Write(
            const BlobResponse& __v,
            Message* __msg);

    bool
    Read(
            BlobResponse* __v,
            const Message* __msg,
            void** __iter) NS_WARN_UNUSED_RESULT;

    Channel* mChannel;
    mozilla::ipc::IProtocolManager<mozilla::ipc::RPCChannel::RPCListener>* mManager;
    int32_t mId;
    State mState;
};


} // namespace devicestorage
} // namespace dom
} // namespace mozilla

#if 0

//-----------------------------------------------------------------------------
// Skeleton implementation of abstract actor class

// Header file contents
namespace mozilla {
namespace dom {
namespace devicestorage {
class DeviceStorageRequestParent :
    public PDeviceStorageRequestParent
{
    DeviceStorageRequestParent();
    virtual ~DeviceStorageRequestParent();
};
} // namespace devicestorage
} // namespace dom
} // namespace mozilla


// C++ file contents
namespace mozilla {
namespace dom {
namespace devicestorage {
DeviceStorageRequestParent::DeviceStorageRequestParent()
{
    MOZ_COUNT_CTOR(DeviceStorageRequestParent);
}

DeviceStorageRequestParent::~DeviceStorageRequestParent()
{
    MOZ_COUNT_DTOR(DeviceStorageRequestParent);
}

} // namespace devicestorage
} // namespace dom
} // namespace mozilla
#endif // if 0

#endif // ifndef PDeviceStorageRequestParent_h
