//
// Automatically generated by ipdlc.
// Edit at your own risk
//

#ifndef DOMTypes_h
#define DOMTypes_h

#include "mozilla/Attributes.h"
#include "base/basictypes.h"
#include "prtime.h"
#include "nscore.h"
#include "IPCMessageStart.h"
#include "ipc/IPCMessageUtils.h"
#include "nsAutoPtr.h"
#include "nsStringGlue.h"
#include "nsTArray.h"
#include "nsIFile.h"
#include "mozilla/ipc/ProtocolUtils.h"

namespace mozilla {
namespace dom {
class PBlobParent;
} // namespace dom
} // namespace mozilla

namespace mozilla {
namespace dom {
class PBlobChild;
} // namespace dom
} // namespace mozilla


//-----------------------------------------------------------------------------
// Declaration of the IPDL type |struct ClonedMessageData|
//
namespace mozilla {
namespace dom {
class ClonedMessageData MOZ_FINAL
{
private:
    typedef mozilla::SerializedStructuredCloneBuffer SerializedStructuredCloneBuffer;
    typedef mozilla::dom::PBlobParent PBlobParent;
    typedef mozilla::dom::PBlobChild PBlobChild;

public:
    ClonedMessageData();

    ClonedMessageData(
            const SerializedStructuredCloneBuffer& _data,
            const InfallibleTArray<PBlobParent*>& _blobsParent,
            const InfallibleTArray<PBlobChild*>& _blobsChild)
    {
        Init();
        Assign(_data, _blobsParent, _blobsChild);
    }

    ClonedMessageData(const ClonedMessageData& _o)
    {
        Init();
        Assign((_o).data(), (_o).blobsParent(), (_o).blobsChild());
    }

    ~ClonedMessageData();

    void
    operator=(const ClonedMessageData& _o)
    {
        Assign((_o).data(), (_o).blobsParent(), (_o).blobsChild());
    }

    bool
    operator==(const ClonedMessageData& _o) const;

    SerializedStructuredCloneBuffer&
    data()
    {
        return data_;
    }
    const SerializedStructuredCloneBuffer&
    data() const
    {
        return data_;
    }

    InfallibleTArray<PBlobParent*>&
    blobsParent()
    {
        return blobsParent_;
    }
    const InfallibleTArray<PBlobParent*>&
    blobsParent() const
    {
        return blobsParent_;
    }

    InfallibleTArray<PBlobChild*>&
    blobsChild()
    {
        return blobsChild_;
    }
    const InfallibleTArray<PBlobChild*>&
    blobsChild() const
    {
        return blobsChild_;
    }

private:
    void
    Init();

    void
    Assign(
            const SerializedStructuredCloneBuffer& _data,
            const InfallibleTArray<PBlobParent*>& _blobsParent,
            const InfallibleTArray<PBlobChild*>& _blobsChild);

    SerializedStructuredCloneBuffer data_;
    InfallibleTArray<PBlobParent*> blobsParent_;
    InfallibleTArray<PBlobChild*> blobsChild_;
};
} // namespace dom
} // namespace mozilla


//-----------------------------------------------------------------------------
// Declaration of the IPDL type |struct NormalBlobConstructorParams|
//
namespace mozilla {
namespace dom {
class NormalBlobConstructorParams MOZ_FINAL
{
private:

public:
    NormalBlobConstructorParams();

    NormalBlobConstructorParams(
            const nsString& _contentType,
            const uint64_t& _length)
    {
        Init();
        Assign(_contentType, _length);
    }

    NormalBlobConstructorParams(const NormalBlobConstructorParams& _o)
    {
        Init();
        Assign((_o).contentType(), (_o).length());
    }

    ~NormalBlobConstructorParams();

    void
    operator=(const NormalBlobConstructorParams& _o)
    {
        Assign((_o).contentType(), (_o).length());
    }

    bool
    operator==(const NormalBlobConstructorParams& _o) const;

    nsString&
    contentType()
    {
        return contentType_;
    }
    const nsString&
    contentType() const
    {
        return contentType_;
    }

    uint64_t&
    length()
    {
        return length_;
    }
    const uint64_t&
    length() const
    {
        return length_;
    }

private:
    void
    Init();

    void
    Assign(
            const nsString& _contentType,
            const uint64_t& _length);

    nsString contentType_;
    uint64_t length_;
};
} // namespace dom
} // namespace mozilla


//-----------------------------------------------------------------------------
// Declaration of the IPDL type |struct FileBlobConstructorParams|
//
namespace mozilla {
namespace dom {
class FileBlobConstructorParams MOZ_FINAL
{
private:

public:
    FileBlobConstructorParams();

    FileBlobConstructorParams(
            const nsString& _name,
            const nsString& _contentType,
            const uint64_t& _length,
            const uint64_t& _modDate)
    {
        Init();
        Assign(_name, _contentType, _length, _modDate);
    }

    FileBlobConstructorParams(const FileBlobConstructorParams& _o)
    {
        Init();
        Assign((_o).name(), (_o).contentType(), (_o).length(), (_o).modDate());
    }

    ~FileBlobConstructorParams();

    void
    operator=(const FileBlobConstructorParams& _o)
    {
        Assign((_o).name(), (_o).contentType(), (_o).length(), (_o).modDate());
    }

    bool
    operator==(const FileBlobConstructorParams& _o) const;

    nsString&
    name()
    {
        return name_;
    }
    const nsString&
    name() const
    {
        return name_;
    }

    nsString&
    contentType()
    {
        return contentType_;
    }
    const nsString&
    contentType() const
    {
        return contentType_;
    }

    uint64_t&
    length()
    {
        return length_;
    }
    const uint64_t&
    length() const
    {
        return length_;
    }

    uint64_t&
    modDate()
    {
        return modDate_;
    }
    const uint64_t&
    modDate() const
    {
        return modDate_;
    }

private:
    void
    Init();

    void
    Assign(
            const nsString& _name,
            const nsString& _contentType,
            const uint64_t& _length,
            const uint64_t& _modDate);

    nsString name_;
    nsString contentType_;
    uint64_t length_;
    uint64_t modDate_;
};
} // namespace dom
} // namespace mozilla

#endif // ifndef DOMTypes_h
