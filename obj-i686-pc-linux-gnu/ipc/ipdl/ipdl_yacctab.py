
# ipdl_yacctab.py
# This file is automatically generated. Do not edit.
_tabversion = '3.2'

_lr_method = 'LALR'

_lr_signature = '\xc7e\xd0\x8a\xa9rz\x98{\xdd\xa4\xb5Q\x15\xb6\n'
    
_lr_action_items = {'$end':([2,4,7,8,11,13,23,91,96,128,132,145,],[0,-14,-12,-1,-15,-16,-11,-13,-18,-22,-17,-25,]),'BRIDGES':([44,63,67,164,180,181,],[57,57,57,-36,-29,-30,]),'COMPRESS':([142,157,172,191,],[-64,176,-62,-63,]),'SYNC':([0,1,4,7,8,11,13,22,23,38,44,49,58,63,67,72,75,76,91,96,102,124,128,132,137,139,145,151,161,163,164,180,181,],[-3,6,-14,-12,6,-15,-16,-2,-11,6,6,6,6,6,6,6,6,6,-13,-18,6,-52,-22,-17,-43,-48,-25,-51,-39,-40,-36,-29,-30,]),',':([98,141,154,155,174,177,186,187,],[133,-91,-90,173,-92,-91,-89,173,]),'RECV':([152,166,171,185,188,190,194,],[165,-73,165,-72,-76,-75,-74,]),'>':([56,],[97,]),'OPENS':([73,81,106,107,],[120,126,120,126,]),'GOTO':([183,],[189,]),'BOTH':([44,58,63,67,72,75,76,124,137,139,151,161,163,164,180,181,],[65,65,65,65,65,65,65,-52,-43,-48,-51,-39,-40,-36,-29,-30,]),'NULLABLE':([37,40,47,51,83,95,127,131,141,173,177,],[45,45,45,45,-24,-20,-23,-19,45,45,45,]),')':([141,154,155,174,177,186,187,],[-91,-90,172,-92,-91,-89,191,]),'(':([113,115,116,117,140,158,],[-58,141,-60,-59,-61,177,]),'MANAGER':([44,63,67,75,161,163,164,180,181,],[69,69,69,69,-39,-40,-36,-29,-30,]),'RPC':([0,1,4,7,8,11,13,22,23,38,44,49,58,63,67,72,75,76,91,96,102,124,128,132,137,139,145,151,161,163,164,180,181,],[-3,12,-14,-12,12,-15,-16,-2,-11,12,12,12,12,12,12,12,12,12,-13,-18,12,-52,-22,-17,-43,-48,-25,-51,-39,-40,-36,-29,-30,]),'MANAGES':([44,58,63,67,75,76,137,139,161,163,164,180,181,],[70,70,70,70,70,70,-43,-48,-39,-40,-36,-29,-30,]),'ANSWER':([152,166,171,185,188,190,194,],[170,-73,170,-72,-76,-75,-74,]),';':([5,9,10,20,28,29,31,32,33,34,35,39,46,52,53,54,55,78,84,85,86,87,88,89,90,92,93,94,97,109,110,111,112,121,134,136,142,143,144,146,147,148,149,150,153,157,160,162,172,175,176,178,179,182,184,191,192,193,196,],[22,-5,-4,-6,-7,-9,-10,-106,-102,-101,-105,-8,83,95,96,-104,-103,124,127,128,-96,-93,-94,-97,-105,-21,131,132,-107,137,-44,139,-56,145,151,-83,-64,-33,161,-33,163,-95,-98,164,-45,-66,180,181,-62,-57,-65,-31,-32,188,190,-63,-82,194,-81,]),':':([60,65,73,81,90,99,101,106,107,135,136,],[102,-55,-53,-54,130,-53,-54,-53,-54,152,-83,]),'<':([35,90,],[43,43,]),'__DELETE__':([3,6,12,17,44,58,63,67,71,72,75,76,102,124,137,139,151,161,163,164,165,167,168,169,170,180,181,],[-84,-88,-87,-86,-85,-85,-85,-85,117,-85,-85,-85,-85,-52,-43,-48,-51,-39,-40,-36,-78,-77,184,-79,-80,-29,-30,]),'PROTOCOL':([0,1,3,4,6,7,8,11,12,13,14,17,18,22,23,38,49,91,96,128,132,145,],[-3,-85,-84,-14,-88,-12,-85,-15,-87,-16,24,-86,27,-2,-11,-85,-85,-13,-18,-22,-17,-25,]),'STRING':([18,],[28,]),'PARENT':([44,58,63,67,72,75,76,124,137,139,151,159,161,163,164,180,181,],[73,99,73,106,99,106,99,-52,-43,-48,-51,178,-39,-40,-36,-29,-30,]),'UNION':([0,1,4,7,8,11,13,22,23,38,49,91,96,128,132,145,],[-3,15,-14,-12,15,-15,-16,-2,-11,15,15,-13,-18,-22,-17,-25,]),'NAMESPACE':([0,1,4,7,8,11,13,22,23,38,49,91,96,128,132,145,],[-3,16,-14,-12,16,-15,-16,-2,-11,16,16,-13,-18,-22,-17,-25,]),'COLONCOLON':([32,33,34,35,54,55,97,],[-106,41,42,-105,-104,-103,-107,]),'START':([44,58,63,64,67,72,75,76,124,137,139,151,161,163,164,166,171,180,181,185,188,190,194,],[77,77,77,77,77,77,77,77,-52,-43,-48,-51,-39,-40,-36,-73,-69,-29,-30,-72,-76,-75,-74,]),'STATE':([44,58,63,64,67,68,72,75,76,77,124,137,139,151,161,163,164,166,171,180,181,185,188,190,194,],[-71,-71,-71,-71,-71,108,-71,-71,-71,-70,-52,-43,-48,-51,-39,-40,-36,-73,-69,-29,-30,-72,-76,-75,-74,]),'ASYNC':([0,1,4,7,8,11,13,22,23,38,44,49,58,63,67,72,75,76,91,96,102,124,128,132,137,139,145,151,161,163,164,180,181,],[-3,17,-14,-12,17,-15,-16,-2,-11,17,17,17,17,17,17,17,17,17,-13,-18,17,-52,-22,-17,-43,-48,-25,-51,-39,-40,-36,-29,-30,]),'[':([32,86,88,89,90,97,136,149,],[-106,-96,129,-97,-105,-107,-83,-98,]),'INCLUDE':([0,1,22,],[-3,18,-2,]),']':([129,],[148,]),'ID':([3,6,12,15,16,17,18,19,21,24,27,32,37,40,41,42,43,44,45,47,48,50,51,57,58,63,67,69,70,71,72,75,76,83,86,87,88,89,90,95,97,102,108,114,119,120,124,125,126,127,130,131,133,136,137,138,139,141,148,149,151,156,161,163,164,165,167,168,169,170,173,177,180,181,189,195,],[-84,-88,-87,25,26,-86,29,30,35,36,39,-106,-100,-100,35,35,56,-85,-99,-100,90,92,-100,98,-85,-85,-85,110,111,113,-85,-85,-85,-24,-96,-93,-94,-97,-105,-20,-107,-85,136,140,143,144,-52,146,147,-23,136,-19,150,-83,-43,153,-48,-100,-95,-98,-51,174,-39,-40,-36,-78,-77,183,-79,-80,-100,-100,-29,-30,136,136,]),'AS':([143,146,],[159,159,]),'STRUCT':([0,1,4,7,8,11,13,22,23,38,49,91,96,128,132,145,],[-3,19,-14,-12,19,-15,-16,-2,-11,19,19,-13,-18,-22,-17,-25,]),'RETURNS':([142,172,],[158,-62,]),'SEND':([152,166,171,185,188,190,194,],[167,-73,167,-72,-76,-75,-74,]),'SPAWNS':([73,81,],[119,125,]),'CALL':([152,166,171,185,188,190,194,],[169,-73,169,-72,-76,-75,-74,]),'CHILD':([44,58,63,67,72,75,76,124,137,139,151,159,161,163,164,180,181,],[81,101,81,107,101,107,101,-52,-43,-48,-51,179,-39,-40,-36,-29,-30,]),'USING':([0,1,22,],[-3,21,-2,]),'{':([25,26,30,36,],[37,38,40,44,]),'DELETE':([3,6,12,17,44,58,63,67,71,72,75,76,102,124,137,139,151,161,163,164,165,167,168,169,170,180,181,],[-84,-88,-87,-86,-85,-85,-85,-85,116,-85,-85,-85,-85,-52,-43,-48,-51,-39,-40,-36,-78,-77,182,-79,-80,-29,-30,]),'}':([4,7,11,13,23,40,44,47,49,51,58,59,61,62,63,64,66,67,72,74,75,76,79,80,82,83,91,95,96,100,103,104,105,118,122,123,124,127,128,131,132,137,139,145,151,161,163,164,166,171,180,181,185,188,190,194,],[-14,-12,-15,-16,-11,53,-68,85,91,94,-68,-50,-28,-35,-68,-68,-47,-68,-68,121,-68,-68,-38,-42,-26,-24,-13,-20,-18,-41,-27,-67,-34,-49,-37,-46,-52,-23,-22,-19,-17,-43,-48,-25,-51,-39,-40,-36,-73,-69,-29,-30,-72,-76,-75,-74,]),'OR':([109,110,136,153,192,193,196,],[138,-44,-83,-45,-82,195,-81,]),'~':([3,6,12,17,44,58,63,67,71,72,75,76,102,124,137,139,151,161,163,164,180,181,],[-84,-88,-87,-86,-85,-85,-85,-85,114,-85,-85,-85,-85,-52,-43,-48,-51,-39,-40,-36,-29,-30,]),}

_lr_action = { }
for _k, _v in _lr_action_items.items():
   for _x,_y in zip(_v[0],_v[1]):
      if not _x in _lr_action:  _lr_action[_x] = { }
      _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {'SendSemanticsQual':([1,8,38,44,49,58,63,67,72,75,76,102,],[3,3,3,3,3,3,3,3,3,3,3,3,]),'ManagersStmt':([44,63,67,75,],[58,58,58,58,]),'TransitionStmtsOpt':([44,58,63,64,67,72,75,76,],[59,59,59,104,59,59,59,59,]),'CxxType':([21,],[31,]),'ScalarType':([48,],[88,]),'StructDecl':([1,8,38,49,],[4,4,4,4,]),'PreambleStmt':([1,],[5,]),'MessageId':([71,],[115,]),'Param':([141,173,177,],[154,186,154,]),'MessageDirectionLabel':([44,58,63,67,72,75,76,],[60,60,60,60,60,60,60,]),'NamespaceThing':([1,8,38,49,],[7,23,7,23,]),'State':([108,130,189,195,],[135,149,192,196,]),'OpensStmtsOpt':([44,63,67,75,],[62,62,62,122,]),'NamespacedStuff':([1,38,],[8,49,]),'IncludeStmt':([1,],[9,]),'CxxTemplateInst':([21,41,42,48,],[32,32,32,32,]),'CxxIncludeStmt':([1,],[10,]),'CxxID':([21,41,42,48,],[33,54,55,89,]),'Type':([37,40,47,51,141,173,177,],[46,50,84,50,156,156,156,]),'MessageDeclsOpt':([44,58,63,67,72,75,76,],[66,66,66,66,118,66,66,]),'MessageOutParams':([142,],[157,]),'BridgesStmt':([44,63,67,],[67,67,67,]),'StructFields':([40,],[51,]),'BasicType':([48,],[87,]),'UnionDecl':([1,8,38,49,],[11,11,11,11,]),'OptionalMessageCompress':([157,],[175,]),'BridgesStmtsOpt':([44,63,67,],[61,61,105,]),'MaybeNullable':([37,40,47,51,141,173,177,],[48,48,48,48,48,48,48,]),'ProtocolDefn':([1,8,38,49,],[13,13,13,13,]),'QualifiedID':([21,],[34,]),'Preamble':([0,],[1,]),'OptionalSendSemanticsQual':([1,8,38,44,49,58,63,67,72,75,76,102,],[14,14,14,71,14,71,71,71,71,71,71,71,]),'MessageDeclThing':([44,58,63,67,72,75,76,],[72,72,72,72,72,72,72,]),'ManagerList':([69,],[109,]),'ProtocolBody':([44,],[74,]),'Transition':([152,171,],[166,185,]),'OpensStmt':([44,63,67,75,],[75,75,75,75,]),'ManagesStmt':([44,58,63,67,75,76,],[76,76,76,76,76,76,]),'MessageInParams':([115,],[142,]),'StateList':([189,],[193,]),'ParamList':([141,177,],[155,187,]),'OptionalStart':([44,58,63,64,67,72,75,76,],[68,68,68,68,68,68,68,68,]),'MessageDecl':([44,58,63,67,72,75,76,102,],[78,78,78,78,78,78,78,134,]),'Transitions':([152,],[171,]),'ManagersStmtOpt':([44,63,67,75,],[79,79,79,79,]),'ManagesStmtsOpt':([44,58,63,67,75,76,],[80,100,80,80,80,123,]),'TransitionStmt':([44,58,63,64,67,72,75,76,],[64,64,64,64,64,64,64,64,]),'ComponentTypes':([37,],[47,]),'TranslationUnit':([0,],[2,]),'MessageBody':([71,],[112,]),'Trigger':([152,171,],[168,168,]),'AsOpt':([143,146,],[160,162,]),'UsingStmt':([1,],[20,]),'SpawnsStmt':([44,63,],[63,63,]),'ActorType':([48,],[86,]),'StructField':([40,51,],[52,93,]),'SpawnsStmtsOpt':([44,63,],[82,103,]),}

_lr_goto = { }
for _k, _v in _lr_goto_items.items():
   for _x,_y in zip(_v[0],_v[1]):
       if not _x in _lr_goto: _lr_goto[_x] = { }
       _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
  ("S' -> TranslationUnit","S'",1,None,None,None),
  ('TranslationUnit -> Preamble NamespacedStuff','TranslationUnit',2,'p_TranslationUnit','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',186),
  ('Preamble -> Preamble PreambleStmt ;','Preamble',3,'p_Preamble','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',231),
  ('Preamble -> <empty>','Preamble',0,'p_Preamble','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',232),
  ('PreambleStmt -> CxxIncludeStmt','PreambleStmt',1,'p_PreambleStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',240),
  ('PreambleStmt -> IncludeStmt','PreambleStmt',1,'p_PreambleStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',241),
  ('PreambleStmt -> UsingStmt','PreambleStmt',1,'p_PreambleStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',242),
  ('CxxIncludeStmt -> INCLUDE STRING','CxxIncludeStmt',2,'p_CxxIncludeStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',246),
  ('IncludeStmt -> INCLUDE PROTOCOL ID','IncludeStmt',3,'p_IncludeStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',250),
  ('IncludeStmt -> INCLUDE ID','IncludeStmt',2,'p_IncludeStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',251),
  ('UsingStmt -> USING CxxType','UsingStmt',2,'p_UsingStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',272),
  ('NamespacedStuff -> NamespacedStuff NamespaceThing','NamespacedStuff',2,'p_NamespacedStuff','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',278),
  ('NamespacedStuff -> NamespaceThing','NamespacedStuff',1,'p_NamespacedStuff','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',279),
  ('NamespaceThing -> NAMESPACE ID { NamespacedStuff }','NamespaceThing',5,'p_NamespaceThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',287),
  ('NamespaceThing -> StructDecl','NamespaceThing',1,'p_NamespaceThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',288),
  ('NamespaceThing -> UnionDecl','NamespaceThing',1,'p_NamespaceThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',289),
  ('NamespaceThing -> ProtocolDefn','NamespaceThing',1,'p_NamespaceThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',290),
  ('StructDecl -> STRUCT ID { StructFields } ;','StructDecl',6,'p_StructDecl','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',299),
  ('StructDecl -> STRUCT ID { } ;','StructDecl',5,'p_StructDecl','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',300),
  ('StructFields -> StructFields StructField ;','StructFields',3,'p_StructFields','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',307),
  ('StructFields -> StructField ;','StructFields',2,'p_StructFields','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',308),
  ('StructField -> Type ID','StructField',2,'p_StructField','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',316),
  ('UnionDecl -> UNION ID { ComponentTypes } ;','UnionDecl',6,'p_UnionDecl','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',320),
  ('ComponentTypes -> ComponentTypes Type ;','ComponentTypes',3,'p_ComponentTypes','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',324),
  ('ComponentTypes -> Type ;','ComponentTypes',2,'p_ComponentTypes','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',325),
  ('ProtocolDefn -> OptionalSendSemanticsQual PROTOCOL ID { ProtocolBody } ;','ProtocolDefn',7,'p_ProtocolDefn','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',333),
  ('ProtocolBody -> SpawnsStmtsOpt','ProtocolBody',1,'p_ProtocolBody','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',345),
  ('SpawnsStmtsOpt -> SpawnsStmt SpawnsStmtsOpt','SpawnsStmtsOpt',2,'p_SpawnsStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',352),
  ('SpawnsStmtsOpt -> BridgesStmtsOpt','SpawnsStmtsOpt',1,'p_SpawnsStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',353),
  ('SpawnsStmt -> PARENT SPAWNS ID AsOpt ;','SpawnsStmt',5,'p_SpawnsStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',361),
  ('SpawnsStmt -> CHILD SPAWNS ID AsOpt ;','SpawnsStmt',5,'p_SpawnsStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',362),
  ('AsOpt -> AS PARENT','AsOpt',2,'p_AsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',366),
  ('AsOpt -> AS CHILD','AsOpt',2,'p_AsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',367),
  ('AsOpt -> <empty>','AsOpt',0,'p_AsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',368),
  ('BridgesStmtsOpt -> BridgesStmt BridgesStmtsOpt','BridgesStmtsOpt',2,'p_BridgesStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',375),
  ('BridgesStmtsOpt -> OpensStmtsOpt','BridgesStmtsOpt',1,'p_BridgesStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',376),
  ('BridgesStmt -> BRIDGES ID , ID ;','BridgesStmt',5,'p_BridgesStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',384),
  ('OpensStmtsOpt -> OpensStmt OpensStmtsOpt','OpensStmtsOpt',2,'p_OpensStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',388),
  ('OpensStmtsOpt -> ManagersStmtOpt','OpensStmtsOpt',1,'p_OpensStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',389),
  ('OpensStmt -> PARENT OPENS ID ;','OpensStmt',4,'p_OpensStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',397),
  ('OpensStmt -> CHILD OPENS ID ;','OpensStmt',4,'p_OpensStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',398),
  ('ManagersStmtOpt -> ManagersStmt ManagesStmtsOpt','ManagersStmtOpt',2,'p_ManagersStmtOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',405),
  ('ManagersStmtOpt -> ManagesStmtsOpt','ManagersStmtOpt',1,'p_ManagersStmtOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',406),
  ('ManagersStmt -> MANAGER ManagerList ;','ManagersStmt',3,'p_ManagersStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',414),
  ('ManagerList -> ID','ManagerList',1,'p_ManagerList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',421),
  ('ManagerList -> ManagerList OR ID','ManagerList',3,'p_ManagerList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',422),
  ('ManagesStmtsOpt -> ManagesStmt ManagesStmtsOpt','ManagesStmtsOpt',2,'p_ManagesStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',430),
  ('ManagesStmtsOpt -> MessageDeclsOpt','ManagesStmtsOpt',1,'p_ManagesStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',431),
  ('ManagesStmt -> MANAGES ID ;','ManagesStmt',3,'p_ManagesStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',439),
  ('MessageDeclsOpt -> MessageDeclThing MessageDeclsOpt','MessageDeclsOpt',2,'p_MessageDeclsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',447),
  ('MessageDeclsOpt -> TransitionStmtsOpt','MessageDeclsOpt',1,'p_MessageDeclsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',448),
  ('MessageDeclThing -> MessageDirectionLabel : MessageDecl ;','MessageDeclThing',4,'p_MessageDeclThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',456),
  ('MessageDeclThing -> MessageDecl ;','MessageDeclThing',2,'p_MessageDeclThing','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',457),
  ('MessageDirectionLabel -> PARENT','MessageDirectionLabel',1,'p_MessageDirectionLabel','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',464),
  ('MessageDirectionLabel -> CHILD','MessageDirectionLabel',1,'p_MessageDirectionLabel','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',465),
  ('MessageDirectionLabel -> BOTH','MessageDirectionLabel',1,'p_MessageDirectionLabel','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',466),
  ('MessageDecl -> OptionalSendSemanticsQual MessageBody','MessageDecl',2,'p_MessageDecl','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',477),
  ('MessageBody -> MessageId MessageInParams MessageOutParams OptionalMessageCompress','MessageBody',4,'p_MessageBody','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',488),
  ('MessageId -> ID','MessageId',1,'p_MessageId','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',500),
  ('MessageId -> __DELETE__','MessageId',1,'p_MessageId','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',501),
  ('MessageId -> DELETE','MessageId',1,'p_MessageId','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',502),
  ('MessageId -> ~ ID','MessageId',2,'p_MessageId','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',503),
  ('MessageInParams -> ( ParamList )','MessageInParams',3,'p_MessageInParams','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',512),
  ('MessageOutParams -> RETURNS ( ParamList )','MessageOutParams',4,'p_MessageOutParams','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',516),
  ('MessageOutParams -> <empty>','MessageOutParams',0,'p_MessageOutParams','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',517),
  ('OptionalMessageCompress -> COMPRESS','OptionalMessageCompress',1,'p_OptionalMessageCompress','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',524),
  ('OptionalMessageCompress -> <empty>','OptionalMessageCompress',0,'p_OptionalMessageCompress','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',525),
  ('TransitionStmtsOpt -> TransitionStmt TransitionStmtsOpt','TransitionStmtsOpt',2,'p_TransitionStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',535),
  ('TransitionStmtsOpt -> <empty>','TransitionStmtsOpt',0,'p_TransitionStmtsOpt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',536),
  ('TransitionStmt -> OptionalStart STATE State : Transitions','TransitionStmt',5,'p_TransitionStmt','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',545),
  ('OptionalStart -> START','OptionalStart',1,'p_OptionalStart','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',550),
  ('OptionalStart -> <empty>','OptionalStart',0,'p_OptionalStart','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',551),
  ('Transitions -> Transitions Transition','Transitions',2,'p_Transitions','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',555),
  ('Transitions -> Transition','Transitions',1,'p_Transitions','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',556),
  ('Transition -> Trigger ID GOTO StateList ;','Transition',5,'p_Transition','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',564),
  ('Transition -> Trigger __DELETE__ ;','Transition',3,'p_Transition','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',565),
  ('Transition -> Trigger DELETE ;','Transition',3,'p_Transition','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',566),
  ('Trigger -> SEND','Trigger',1,'p_Trigger','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',578),
  ('Trigger -> RECV','Trigger',1,'p_Trigger','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',579),
  ('Trigger -> CALL','Trigger',1,'p_Trigger','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',580),
  ('Trigger -> ANSWER','Trigger',1,'p_Trigger','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',581),
  ('StateList -> StateList OR State','StateList',3,'p_StateList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',585),
  ('StateList -> State','StateList',1,'p_StateList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',586),
  ('State -> ID','State',1,'p_State','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',594),
  ('OptionalSendSemanticsQual -> SendSemanticsQual','OptionalSendSemanticsQual',1,'p_OptionalSendSemanticsQual','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',600),
  ('OptionalSendSemanticsQual -> <empty>','OptionalSendSemanticsQual',0,'p_OptionalSendSemanticsQual','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',601),
  ('SendSemanticsQual -> ASYNC','SendSemanticsQual',1,'p_SendSemanticsQual','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',606),
  ('SendSemanticsQual -> RPC','SendSemanticsQual',1,'p_SendSemanticsQual','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',607),
  ('SendSemanticsQual -> SYNC','SendSemanticsQual',1,'p_SendSemanticsQual','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',608),
  ('ParamList -> ParamList , Param','ParamList',3,'p_ParamList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',617),
  ('ParamList -> Param','ParamList',1,'p_ParamList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',618),
  ('ParamList -> <empty>','ParamList',0,'p_ParamList','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',619),
  ('Param -> Type ID','Param',2,'p_Param','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',629),
  ('Type -> MaybeNullable BasicType','Type',2,'p_Type','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',633),
  ('BasicType -> ScalarType','BasicType',1,'p_BasicType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',639),
  ('BasicType -> ScalarType [ ]','BasicType',3,'p_BasicType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',640),
  ('ScalarType -> ActorType','ScalarType',1,'p_ScalarType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',646),
  ('ScalarType -> CxxID','ScalarType',1,'p_ScalarType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',647),
  ('ActorType -> ID : State','ActorType',3,'p_ActorType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',656),
  ('MaybeNullable -> NULLABLE','MaybeNullable',1,'p_MaybeNullable','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',661),
  ('MaybeNullable -> <empty>','MaybeNullable',0,'p_MaybeNullable','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',662),
  ('CxxType -> QualifiedID','CxxType',1,'p_CxxType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',668),
  ('CxxType -> CxxID','CxxType',1,'p_CxxType','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',669),
  ('QualifiedID -> QualifiedID COLONCOLON CxxID','QualifiedID',3,'p_QualifiedID','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',677),
  ('QualifiedID -> CxxID COLONCOLON CxxID','QualifiedID',3,'p_QualifiedID','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',678),
  ('CxxID -> ID','CxxID',1,'p_CxxID','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',689),
  ('CxxID -> CxxTemplateInst','CxxID',1,'p_CxxID','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',690),
  ('CxxTemplateInst -> ID < ID >','CxxTemplateInst',4,'p_CxxTemplateInst','/home/txema/work/firefox-20.0+build1/ipc/ipdl/ipdl/parser.py',697),
]
