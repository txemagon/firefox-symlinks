//
// Automatically generated by ipdlc.
// Edit at your own risk
//


#include "mozilla/dom/indexedDB/PIndexedDBRequestParent.h"

#include "mozilla/dom/PBlobParent.h"
#include "mozilla/dom/indexedDB/PIndexedDBCursorParent.h"
#include "mozilla/dom/indexedDB/PIndexedDBIndexParent.h"
#include "mozilla/dom/indexedDB/PIndexedDBObjectStoreParent.h"

typedef IPC::Message Message;
typedef mozilla::ipc::RPCChannel Channel;
typedef mozilla::ipc::RPCChannel::RPCListener ChannelListener;
typedef base::ProcessHandle ProcessHandle;
typedef mozilla::ipc::AsyncChannel AsyncChannel;
typedef mozilla::ipc::SharedMemory SharedMemory;
typedef mozilla::ipc::Trigger Trigger;
typedef mozilla::ipc::ActorHandle ActorHandle;
typedef mozilla::dom::indexedDB::ipc::AddResponse AddResponse;
typedef mozilla::dom::indexedDB::ipc::BlobArray BlobArray;
typedef mozilla::dom::indexedDB::ipc::ClearResponse ClearResponse;
typedef mozilla::dom::indexedDB::ipc::ContinueResponse ContinueResponse;
typedef mozilla::dom::indexedDB::ipc::CountResponse CountResponse;
typedef mozilla::dom::indexedDB::ipc::DeleteResponse DeleteResponse;
typedef mozilla::ipc::FileDescriptor FileDescriptor;
typedef mozilla::dom::indexedDB::ipc::GetAllKeysResponse GetAllKeysResponse;
typedef mozilla::dom::indexedDB::ipc::GetAllResponse GetAllResponse;
typedef mozilla::dom::indexedDB::ipc::GetKeyResponse GetKeyResponse;
typedef mozilla::dom::indexedDB::ipc::GetResponse GetResponse;
typedef mozilla::dom::indexedDB::Key Key;
typedef mozilla::dom::indexedDB::ipc::OpenCursorResponse OpenCursorResponse;
typedef mozilla::dom::indexedDB::ipc::PutResponse PutResponse;
typedef mozilla::dom::indexedDB::ipc::ResponseValue ResponseValue;
typedef mozilla::dom::indexedDB::SerializedStructuredCloneReadInfo SerializedStructuredCloneReadInfo;
typedef mozilla::ipc::Shmem Shmem;
typedef mozilla::void_t void_t;
using mozilla::dom::PBlobParent;
using mozilla::dom::indexedDB::PIndexedDBCursorParent;
using mozilla::dom::indexedDB::PIndexedDBIndexParent;
using mozilla::dom::indexedDB::PIndexedDBObjectStoreParent;

namespace mozilla {
namespace dom {
namespace indexedDB {


void
PIndexedDBRequestParent::ActorDestroy(ActorDestroyReason why)
{
}

PIndexedDBRequestParent::PIndexedDBRequestParent() :
    mId(0),
    mState(PIndexedDBRequest::__Dead)
{
    MOZ_COUNT_CTOR(PIndexedDBRequestParent);
}

PIndexedDBRequestParent::~PIndexedDBRequestParent()
{
    MOZ_COUNT_DTOR(PIndexedDBRequestParent);
}

PIndexedDBRequest::State
PIndexedDBRequestParent::state()
{
    return mState;
}

bool
PIndexedDBRequestParent::Send__delete__(
        PIndexedDBRequestParent* actor,
        const ResponseValue& response)
{
    if ((!(actor))) {
        return false;
    }

    if ((!(actor))) {
        return false;
    }

    PIndexedDBRequest::Msg___delete__* __msg = new PIndexedDBRequest::Msg___delete__();

    (actor)->Write(actor, __msg, false);
    (actor)->Write(response, __msg);

    (__msg)->set_routing_id((actor)->mId);


    if (mozilla::ipc::LoggingEnabled()) {
        (__msg)->Log("[PIndexedDBRequestParent] Sending ", stderr);
    }
    if ((!(PIndexedDBRequest::Transition((actor)->mState, Trigger(Trigger::Send, PIndexedDBRequest::Msg___delete____ID), (&((actor)->mState)))))) {
        NS_WARNING("bad state transition!");
    }

    bool __sendok = ((actor)->mChannel)->Send(__msg);

    (actor)->DestroySubtree(Deletion);
    (actor)->DeallocSubtree();
    ((actor)->mManager)->RemoveManagee(PIndexedDBRequestMsgStart, actor);
    return __sendok;
}

int32_t
PIndexedDBRequestParent::Register(ChannelListener* aRouted)
{
    return (mManager)->Register(aRouted);
}

int32_t
PIndexedDBRequestParent::RegisterID(
        ChannelListener* aRouted,
        int32_t aId)
{
    return (mManager)->RegisterID(aRouted, aId);
}

ChannelListener*
PIndexedDBRequestParent::Lookup(int32_t aId)
{
    return (mManager)->Lookup(aId);
}

void
PIndexedDBRequestParent::Unregister(int32_t aId)
{
    return (mManager)->Unregister(aId);
}

void
PIndexedDBRequestParent::RemoveManagee(
        int32_t aProtocolId,
        ChannelListener* aListener)
{
    NS_RUNTIMEABORT("unreached");
    return;
}

Shmem::SharedMemory*
PIndexedDBRequestParent::CreateSharedMemory(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        bool aUnsafe,
        Shmem::id_t* aId)
{
    return (mManager)->CreateSharedMemory(aSize, aType, aUnsafe, aId);
}

bool
PIndexedDBRequestParent::AdoptSharedMemory(
        Shmem::SharedMemory* segment,
        Shmem::id_t* aId)
{
    return (mManager)->AdoptSharedMemory(segment, aId);
}

Shmem::SharedMemory*
PIndexedDBRequestParent::LookupSharedMemory(Shmem::id_t aId)
{
    return (mManager)->LookupSharedMemory(aId);
}

bool
PIndexedDBRequestParent::IsTrackingSharedMemory(Shmem::SharedMemory* segment)
{
    return (mManager)->IsTrackingSharedMemory(segment);
}

bool
PIndexedDBRequestParent::DestroySharedMemory(Shmem& shmem)
{
    return (mManager)->DestroySharedMemory(shmem);
}

ProcessHandle
PIndexedDBRequestParent::OtherProcess() const
{
    return (mManager)->OtherProcess();
}

AsyncChannel*
PIndexedDBRequestParent::GetIPCChannel()
{
    return mChannel;
}

PIndexedDBRequestParent::Result
PIndexedDBRequestParent::OnMessageReceived(const Message& __msg)
{
    switch ((__msg).type()) {
    case PIndexedDBRequest::Reply___delete____ID:
        {
            return MsgProcessed;
        }
    default:
        {
            return MsgNotKnown;
        }
    }
}

PIndexedDBRequestParent::Result
PIndexedDBRequestParent::OnMessageReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

PIndexedDBRequestParent::Result
PIndexedDBRequestParent::OnCallReceived(
        const Message& __msg,
        Message*& __reply)
{
    return MsgNotKnown;
}

void
PIndexedDBRequestParent::OnProcessingError(Result code)
{
    NS_RUNTIMEABORT("`OnProcessingError' called on non-toplevel actor");
}

int32_t
PIndexedDBRequestParent::GetProtocolTypeId()
{
    return PIndexedDBRequestMsgStart;
}

bool
PIndexedDBRequestParent::OnReplyTimeout()
{
    NS_RUNTIMEABORT("`OnReplyTimeout' called on non-toplevel actor");
    return false;
}

void
PIndexedDBRequestParent::OnChannelClose()
{
    NS_RUNTIMEABORT("`OnClose' called on non-toplevel actor");
}

void
PIndexedDBRequestParent::OnChannelError()
{
    NS_RUNTIMEABORT("`OnError' called on non-toplevel actor");
}

void
PIndexedDBRequestParent::OnChannelConnected(int32_t pid)
{
    NS_RUNTIMEABORT("'OnConnected' called on non-toplevel actor");
}

bool
PIndexedDBRequestParent::AllocShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, false, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PIndexedDBRequestParent::AllocUnsafeShmem(
        size_t aSize,
        Shmem::SharedMemory::SharedMemoryType aType,
        Shmem* aMem)
{
    Shmem::id_t aId;
    nsAutoPtr<Shmem::SharedMemory> rawmem(CreateSharedMemory(aSize, aType, true, (&(aId))));
    if ((!(rawmem))) {
        return false;
    }

    (*(aMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), (rawmem).forget(), aId);
    return true;
}

bool
PIndexedDBRequestParent::AdoptShmem(
        Shmem& aMem,
        Shmem* aOutMem)
{
    Shmem::SharedMemory* rawmem = (aMem).Segment(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    if (((!(rawmem))) || (IsTrackingSharedMemory(rawmem))) {
        NS_RUNTIMEABORT("bad Shmem");
    }

    Shmem::id_t aId;
    if ((!(AdoptSharedMemory(rawmem, (&(aId)))))) {
        return false;
    }

    (*(aOutMem)) = Shmem(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead(), rawmem, aId);
    return true;
}

bool
PIndexedDBRequestParent::DeallocShmem(Shmem& aMem)
{
    bool ok = DestroySharedMemory(aMem);
    (aMem).forget(Shmem::IHadBetterBeIPDLCodeCallingThis_OtherwiseIAmADoodyhead());
    return ok;
}

void
PIndexedDBRequestParent::FatalError(const char* const msg) const
{
    // Virtual method to prevent inlining.
    // This give us better error reporting.
    // See bug 589371

    NS_ERROR("IPDL error:");
    NS_ERROR(msg);
    mozilla::ipc::ProtocolErrorBreakpoint(msg);

    NS_ERROR("[PIndexedDBRequestParent] killing child side as a result");

    if ((!(base::KillProcess(OtherProcess(), base::PROCESS_END_KILLED_BY_USER, false)))) {
        NS_ERROR("  may have failed to kill child!");
    }
}

void
PIndexedDBRequestParent::DestroySubtree(ActorDestroyReason why)
{
    // Unregister from our manager.
    Unregister(mId);
    mId = 1;

    // Finally, destroy "us".
    ActorDestroy(why);
}

void
PIndexedDBRequestParent::DeallocSubtree()
{
}

void
PIndexedDBRequestParent::Write(
        const CountResponse& __v,
        Message* __msg)
{
    Write((__v).count(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        CountResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->count())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const GetAllKeysResponse& __v,
        Message* __msg)
{
    Write((__v).keys(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        GetAllKeysResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->keys())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const DeleteResponse& __v,
        Message* __msg)
{
    Write((__v).FIXME_Bug_753159(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        DeleteResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->FIXME_Bug_753159())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const ResponseValue& __v,
        Message* __msg)
{
    typedef ResponseValue __type;
    Write(int((__v).type()), __msg);

    switch ((__v).type()) {
    case __type::Tnsresult:
        {
            Write((__v).get_nsresult(), __msg);
            return;
        }
    case __type::TGetResponse:
        {
            Write((__v).get_GetResponse(), __msg);
            return;
        }
    case __type::TGetKeyResponse:
        {
            Write((__v).get_GetKeyResponse(), __msg);
            return;
        }
    case __type::TGetAllResponse:
        {
            Write((__v).get_GetAllResponse(), __msg);
            return;
        }
    case __type::TGetAllKeysResponse:
        {
            Write((__v).get_GetAllKeysResponse(), __msg);
            return;
        }
    case __type::TAddResponse:
        {
            Write((__v).get_AddResponse(), __msg);
            return;
        }
    case __type::TPutResponse:
        {
            Write((__v).get_PutResponse(), __msg);
            return;
        }
    case __type::TDeleteResponse:
        {
            Write((__v).get_DeleteResponse(), __msg);
            return;
        }
    case __type::TClearResponse:
        {
            Write((__v).get_ClearResponse(), __msg);
            return;
        }
    case __type::TCountResponse:
        {
            Write((__v).get_CountResponse(), __msg);
            return;
        }
    case __type::TOpenCursorResponse:
        {
            Write((__v).get_OpenCursorResponse(), __msg);
            return;
        }
    case __type::TContinueResponse:
        {
            Write((__v).get_ContinueResponse(), __msg);
            return;
        }
    default:
        {
            NS_RUNTIMEABORT("unknown union type");
            return;
        }
    }
}

bool
PIndexedDBRequestParent::Read(
        ResponseValue* __v,
        const Message* __msg,
        void** __iter)
{
    typedef ResponseValue __type;
    int type;
    if ((!(Read((&(type)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    switch (type) {
    case __type::Tnsresult:
        {
            nsresult tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_nsresult())), __msg, __iter);
        }
    case __type::TGetResponse:
        {
            GetResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GetResponse())), __msg, __iter);
        }
    case __type::TGetKeyResponse:
        {
            GetKeyResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GetKeyResponse())), __msg, __iter);
        }
    case __type::TGetAllResponse:
        {
            GetAllResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GetAllResponse())), __msg, __iter);
        }
    case __type::TGetAllKeysResponse:
        {
            GetAllKeysResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_GetAllKeysResponse())), __msg, __iter);
        }
    case __type::TAddResponse:
        {
            AddResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_AddResponse())), __msg, __iter);
        }
    case __type::TPutResponse:
        {
            PutResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_PutResponse())), __msg, __iter);
        }
    case __type::TDeleteResponse:
        {
            DeleteResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_DeleteResponse())), __msg, __iter);
        }
    case __type::TClearResponse:
        {
            ClearResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_ClearResponse())), __msg, __iter);
        }
    case __type::TCountResponse:
        {
            CountResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_CountResponse())), __msg, __iter);
        }
    case __type::TOpenCursorResponse:
        {
            OpenCursorResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_OpenCursorResponse())), __msg, __iter);
        }
    case __type::TContinueResponse:
        {
            ContinueResponse tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_ContinueResponse())), __msg, __iter);
        }
    default:
        {
            mozilla::ipc::ProtocolErrorBreakpoint("unknown union type");
            return false;
        }
    }
}

void
PIndexedDBRequestParent::Write(
        const OpenCursorResponse& __v,
        Message* __msg)
{
    typedef OpenCursorResponse __type;
    Write(int((__v).type()), __msg);

    switch ((__v).type()) {
    case __type::TPIndexedDBCursorParent:
        {
            Write((__v).get_PIndexedDBCursorParent(), __msg, false);
            return;
        }
    case __type::TPIndexedDBCursorChild:
        {
            NS_RUNTIMEABORT("wrong side!");
            return;
        }
    case __type::Tvoid_t:
        {
            Write((__v).get_void_t(), __msg);
            return;
        }
    default:
        {
            NS_RUNTIMEABORT("unknown union type");
            return;
        }
    }
}

bool
PIndexedDBRequestParent::Read(
        OpenCursorResponse* __v,
        const Message* __msg,
        void** __iter)
{
    typedef OpenCursorResponse __type;
    int type;
    if ((!(Read((&(type)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    switch (type) {
    case __type::TPIndexedDBCursorParent:
        {
            return false;
        }
    case __type::TPIndexedDBCursorChild:
        {
            PIndexedDBCursorParent* tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_PIndexedDBCursorParent())), __msg, __iter, false);
        }
    case __type::Tvoid_t:
        {
            void_t tmp;
            (*(__v)) = tmp;
            return Read((&((__v)->get_void_t())), __msg, __iter);
        }
    default:
        {
            mozilla::ipc::ProtocolErrorBreakpoint("unknown union type");
            return false;
        }
    }
}

void
PIndexedDBRequestParent::Write(
        const AddResponse& __v,
        Message* __msg)
{
    Write((__v).key(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        AddResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->key())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const InfallibleTArray<PBlobParent*>& __v,
        Message* __msg)
{
    uint32_t length = (__v).Length();
    Write(length, __msg);

    for (uint32_t i = 0; (i) < (length); (++(i))) {
        Write(__v[i], __msg, false);
    }
}

bool
PIndexedDBRequestParent::Read(
        InfallibleTArray<PBlobParent*>* __v,
        const Message* __msg,
        void** __iter)
{
    InfallibleTArray<PBlobParent*>& a = (*(__v));
    uint32_t length;
    if ((!(Read((&(length)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    (__v)->SetLength(length);
    for (uint32_t i = 0; (i) < (length); (++(i))) {
        if ((!(Read((&(a[i])), __msg, __iter, false)))) {
            mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
            return false;
        }
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const InfallibleTArray<BlobArray>& __v,
        Message* __msg)
{
    uint32_t length = (__v).Length();
    Write(length, __msg);

    for (uint32_t i = 0; (i) < (length); (++(i))) {
        Write(__v[i], __msg);
    }
}

bool
PIndexedDBRequestParent::Read(
        InfallibleTArray<BlobArray>* __v,
        const Message* __msg,
        void** __iter)
{
    InfallibleTArray<BlobArray>& a = (*(__v));
    uint32_t length;
    if ((!(Read((&(length)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }

    (__v)->SetLength(length);
    for (uint32_t i = 0; (i) < (length); (++(i))) {
        if ((!(Read((&(a[i])), __msg, __iter)))) {
            mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
            return false;
        }
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const GetResponse& __v,
        Message* __msg)
{
    Write((__v).cloneInfo(), __msg);
    Write((__v).blobsParent(), __msg);
    // skipping actor field that's meaningless on this side
}

bool
PIndexedDBRequestParent::Read(
        GetResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->cloneInfo())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->blobsParent())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    // skipping actor field that's meaningless on this side
    return true;
}

void
PIndexedDBRequestParent::Write(
        const BlobArray& __v,
        Message* __msg)
{
    Write((__v).blobsParent(), __msg);
    // skipping actor field that's meaningless on this side
}

bool
PIndexedDBRequestParent::Read(
        BlobArray* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->blobsParent())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    // skipping actor field that's meaningless on this side
    return true;
}

void
PIndexedDBRequestParent::Write(
        PIndexedDBRequestParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PIndexedDBRequestParent::Read(
        PIndexedDBRequestParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PIndexedDBRequest");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PIndexedDBRequest");
        return false;
    }

    if ((PIndexedDBRequestMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PIndexedDBRequest has different type");
        return false;
    }

    (*(__v)) = static_cast<PIndexedDBRequestParent*>(listener);
    return true;
}

void
PIndexedDBRequestParent::Write(
        const ContinueResponse& __v,
        Message* __msg)
{
    Write((__v).key(), __msg);
    Write((__v).objectKey(), __msg);
    Write((__v).cloneInfo(), __msg);
    Write((__v).blobsParent(), __msg);
    // skipping actor field that's meaningless on this side
}

bool
PIndexedDBRequestParent::Read(
        ContinueResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->key())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->objectKey())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->cloneInfo())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->blobsParent())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    // skipping actor field that's meaningless on this side
    return true;
}

void
PIndexedDBRequestParent::Write(
        const ClearResponse& __v,
        Message* __msg)
{
    Write((__v).FIXME_Bug_753159(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        ClearResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->FIXME_Bug_753159())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const PutResponse& __v,
        Message* __msg)
{
    Write((__v).key(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        PutResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->key())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        PBlobParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PIndexedDBRequestParent::Read(
        PBlobParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PIndexedDBRequest");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PBlob");
        return false;
    }

    if ((PBlobMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PBlob has different type");
        return false;
    }

    (*(__v)) = static_cast<PBlobParent*>(listener);
    return true;
}

void
PIndexedDBRequestParent::Write(
        PIndexedDBCursorParent* __v,
        Message* __msg,
        bool __nullable)
{
    int32_t id;
    if ((!(__v))) {
        if ((!(__nullable))) {
            NS_RUNTIMEABORT("NULL actor value passed to non-nullable param");
        }
        id = 0;
    }
    else {
        id = (__v)->mId;
        if ((1) == (id)) {
            NS_RUNTIMEABORT("actor has been |delete|d");
        }
    }

    Write(id, __msg);
}

bool
PIndexedDBRequestParent::Read(
        PIndexedDBCursorParent** __v,
        const Message* __msg,
        void** __iter,
        bool __nullable)
{
    int32_t id;
    if ((!(Read((&(id)), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if (((1) == (id)) || (((0) == (id)) && ((!(__nullable))))) {
        mozilla::ipc::ProtocolErrorBreakpoint("bad ID for PIndexedDBRequest");
        return false;
    }

    if ((0) == (id)) {
        (*(__v)) = 0;
        return true;
    }

    ChannelListener* listener = Lookup(id);
    if ((!(listener))) {
        mozilla::ipc::ProtocolErrorBreakpoint("could not look up PIndexedDBCursor");
        return false;
    }

    if ((PIndexedDBCursorMsgStart) != ((listener)->GetProtocolTypeId())) {
        mozilla::ipc::ProtocolErrorBreakpoint("actor that should be of type PIndexedDBCursor has different type");
        return false;
    }

    (*(__v)) = static_cast<PIndexedDBCursorParent*>(listener);
    return true;
}

void
PIndexedDBRequestParent::Write(
        const GetAllResponse& __v,
        Message* __msg)
{
    Write((__v).cloneInfos(), __msg);
    Write((__v).blobs(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        GetAllResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->cloneInfos())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    if ((!(Read((&((__v)->blobs())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}

void
PIndexedDBRequestParent::Write(
        const GetKeyResponse& __v,
        Message* __msg)
{
    Write((__v).key(), __msg);
}

bool
PIndexedDBRequestParent::Read(
        GetKeyResponse* __v,
        const Message* __msg,
        void** __iter)
{
    if ((!(Read((&((__v)->key())), __msg, __iter)))) {
        mozilla::ipc::ProtocolErrorBreakpoint("error deserializing (better message TODO)");
        return false;
    }
    return true;
}



} // namespace indexedDB
} // namespace dom
} // namespace mozilla
