/* THIS FILE IS AUTOGENERATED - DO NOT EDIT */

#include "AccessCheck.h"
#include "CSSStyleDeclarationBinding.h"
#include "CSSValueBinding.h"
#include "PrimitiveConversions.h"
#include "WorkerPrivate.h"
#include "XPCQuickStubs.h"
#include "XPCWrapper.h"
#include "mozilla/Preferences.h"
#include "mozilla/dom/BindingUtils.h"
#include "mozilla/dom/CSSValue.h"
#include "mozilla/dom/NonRefcountedDOMObject.h"
#include "mozilla/dom/Nullable.h"
#include "nsContentUtils.h"
#include "nsDOMQS.h"
#include "nsICSSDeclaration.h"
#include "nsIDOMCSSRule.h"

using namespace mozilla::dom;
namespace mozilla {
namespace dom {

namespace CSSStyleDeclarationBinding {

static bool
get_cssText(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, JS::Value* vp)
{
  nsString result;
  self->GetCssText(result);
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}

static bool
set_cssText(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, JS::Value* argv)
{
  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  ErrorResult rv;
  self->SetCssText(arg0, rv);
  if (rv.Failed()) {
    return ThrowMethodFailedWithDetails<true>(cx, rv, "CSSStyleDeclaration", "cssText");
  }

  return true;
}


const JSJitInfo cssText_getterinfo = {
  (JSJitPropertyOp)get_cssText,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

const JSJitInfo cssText_setterinfo = {
  (JSJitPropertyOp)set_cssText,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Setter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNDEFINED   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_length(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, JS::Value* vp)
{
  uint32_t result;
  result = self->Length();
  *vp = UINT_TO_JSVAL(result);
  return true;
}


const JSJitInfo length_getterinfo = {
  (JSJitPropertyOp)get_length,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Getter,
  true,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_DOUBLE   /* returnType.  Only relevant for getters/methods. */
};

static bool
item(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 1) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.item");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  uint32_t arg0;
  if (!ValueToPrimitive<uint32_t, eDefault>(cx, argv[0], &arg0)) {
    return false;
  }
  nsString result;
  self->Item(arg0, result);
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo item_methodinfo = {
  (JSJitPropertyOp)item,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

static bool
getPropertyValue(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 1) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.getPropertyValue");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  ErrorResult rv;
  nsString result;
  self->GetPropertyValue(arg0, result, rv);
  if (rv.Failed()) {
    return ThrowMethodFailedWithDetails<true>(cx, rv, "CSSStyleDeclaration", "getPropertyValue");
  }
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo getPropertyValue_methodinfo = {
  (JSJitPropertyOp)getPropertyValue,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

static bool
getPropertyCSSValue(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 1) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.getPropertyCSSValue");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  ErrorResult rv;
  nsRefPtr<mozilla::dom::CSSValue> result;
  result = self->GetPropertyCSSValue(arg0, rv);
  if (rv.Failed()) {
    return ThrowMethodFailedWithDetails<true>(cx, rv, "CSSStyleDeclaration", "getPropertyCSSValue");
  }
  if (!result) {
    *vp = JSVAL_NULL;
    return true;
  }
  if (!WrapNewBindingObject(cx, obj, result, vp)) {
    MOZ_ASSERT(JS_IsExceptionPending(cx));
    return false;
  }
  return true;
}


const JSJitInfo getPropertyCSSValue_methodinfo = {
  (JSJitPropertyOp)getPropertyCSSValue,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNKNOWN   /* returnType.  Only relevant for getters/methods. */
};

static bool
getPropertyPriority(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 1) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.getPropertyPriority");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  nsString result;
  self->GetPropertyPriority(arg0, result);
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo getPropertyPriority_methodinfo = {
  (JSJitPropertyOp)getPropertyPriority,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

static bool
setProperty(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 2) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.setProperty");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  FakeDependentString arg1_holder;
  const NonNull<nsAString> arg1;
  if (!ConvertJSValueToString(cx, argv[1], &argv[1], eStringify, eStringify, arg1_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg1) = &arg1_holder;
  FakeDependentString arg2_holder;
  const Optional<nsAString> arg2;
  if (2 < argc) {
    if (!ConvertJSValueToString(cx, argv[2], &argv[2], eEmpty, eStringify, arg2_holder)) {
      return false;
    }
    const_cast<Optional<nsAString>&>(arg2) = &arg2_holder;
  }
  ErrorResult rv;
  self->SetProperty(arg0, arg1, arg2, rv);
  if (rv.Failed()) {
    return ThrowMethodFailedWithDetails<true>(cx, rv, "CSSStyleDeclaration", "setProperty");
  }
  *vp = JSVAL_VOID;
  return true;
}


const JSJitInfo setProperty_methodinfo = {
  (JSJitPropertyOp)setProperty,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNDEFINED   /* returnType.  Only relevant for getters/methods. */
};

static bool
removeProperty(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, unsigned argc, JS::Value* vp)
{

  if (argc < 1) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration.removeProperty");
  }

  JS::Value* argv = JS_ARGV(cx, vp);

  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  ErrorResult rv;
  nsString result;
  self->RemoveProperty(arg0, result, rv);
  if (rv.Failed()) {
    return ThrowMethodFailedWithDetails<true>(cx, rv, "CSSStyleDeclaration", "removeProperty");
  }
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo removeProperty_methodinfo = {
  (JSJitPropertyOp)removeProperty,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Method,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_parentRule(JSContext* cx, JSHandleObject obj, nsICSSDeclaration* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMCSSRule> result;
  result = self->GetParentRule();
  if (!result) {
    *vp = JSVAL_NULL;
    return true;
  }
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo parentRule_getterinfo = {
  (JSJitPropertyOp)get_parentRule,
  prototypes::id::CSSStyleDeclaration,
  PrototypeTraits<prototypes::id::CSSStyleDeclaration>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNKNOWN   /* returnType.  Only relevant for getters/methods. */
};

static JSBool
genericMethod(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  nsICSSDeclaration* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::CSSStyleDeclaration, nsICSSDeclaration>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "CSSStyleDeclaration");
    }
  }
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Method);
  JSJitMethodOp method = (JSJitMethodOp)info->op;
  return method(cx, obj, self, argc, vp);
}

static JSBool
genericGetter(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  nsICSSDeclaration* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::CSSStyleDeclaration, nsICSSDeclaration>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "CSSStyleDeclaration");
    }
  }
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Getter);
  JSJitPropertyOp getter = info->op;
  return getter(cx, obj, self, vp);
}

static JSBool
genericSetter(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  nsICSSDeclaration* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::CSSStyleDeclaration, nsICSSDeclaration>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "CSSStyleDeclaration");
    }
  }
  if (argc == 0) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "CSSStyleDeclaration attribute setter");
  }
  JS::Value* argv = JS_ARGV(cx, vp);
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Setter);
  JSJitPropertyOp setter = info->op;
  if (!setter(cx, obj, self, argv)) {
    return false;
  }
  *vp = JSVAL_VOID;
  return true;
}

static JSFunctionSpec sMethods_specs[] = {
  JS_FNINFO("item", genericMethod, &item_methodinfo, 1, JSPROP_ENUMERATE),
  JS_FNINFO("getPropertyValue", genericMethod, &getPropertyValue_methodinfo, 1, JSPROP_ENUMERATE),
  JS_FNINFO("getPropertyCSSValue", genericMethod, &getPropertyCSSValue_methodinfo, 1, JSPROP_ENUMERATE),
  JS_FNINFO("getPropertyPriority", genericMethod, &getPropertyPriority_methodinfo, 1, JSPROP_ENUMERATE),
  JS_FNINFO("setProperty", genericMethod, &setProperty_methodinfo, 2, JSPROP_ENUMERATE),
  JS_FNINFO("removeProperty", genericMethod, &removeProperty_methodinfo, 1, JSPROP_ENUMERATE),
  JS_FNINFO("iterator", JS_ArrayIterator, nullptr, 0, JSPROP_ENUMERATE),
  JS_FS_END
};

static Prefable<JSFunctionSpec> sMethods[] = {
  { true, &sMethods_specs[0] },
  { false, NULL }
};

static jsid sMethods_ids[8] = { JSID_VOID };

static JSFunctionSpec sChromeMethods_specs[] = {
  JS_FNINFO("QueryInterface", QueryInterface, nullptr, 1, 0),
  JS_FS_END
};

static Prefable<JSFunctionSpec> sChromeMethods[] = {
  { true, &sChromeMethods_specs[0] },
  { false, NULL }
};

static jsid sChromeMethods_ids[2] = { JSID_VOID };

static JSPropertySpec sAttributes_specs[] = {
  { "cssText", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &cssText_getterinfo }, { (JSStrictPropertyOp)genericSetter, &cssText_setterinfo }},
  { "length", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &length_getterinfo }, JSOP_NULLWRAPPER},
  { "parentRule", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &parentRule_getterinfo }, JSOP_NULLWRAPPER},
  { 0, 0, 0, JSOP_NULLWRAPPER, JSOP_NULLWRAPPER }
};

static Prefable<JSPropertySpec> sAttributes[] = {
  { true, &sAttributes_specs[0] },
  { false, NULL }
};

static jsid sAttributes_ids[4] = { JSID_VOID };


static const NativeProperties sNativeProperties = {
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  sMethods, sMethods_ids, sMethods_specs,
  sAttributes, sAttributes_ids, sAttributes_specs,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr
};

static const NativeProperties sChromeOnlyNativeProperties = {
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  sChromeMethods, sChromeMethods_ids, sChromeMethods_specs,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr
};
const NativePropertyHooks sNativePropertyHooks = {
  ResolveOwnProperty,
  EnumerateOwnProperties,
  { &sNativeProperties, &sChromeOnlyNativeProperties },
  prototypes::id::CSSStyleDeclaration,
  constructors::id::CSSStyleDeclaration,
  NULL
};

JSNativeHolder _constructor_holder = {
  ThrowingConstructor,
  &sNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "CSSStyleDeclarationPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = JS_GetObjectPrototype(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  if (sChromeMethods_ids[0] == JSID_VOID &&
      (!InitIds(aCx, sChromeMethods, sChromeMethods_ids) ||
       !InitIds(aCx, sMethods, sMethods_ids) ||
       !InitIds(aCx, sAttributes, sAttributes_ids))) {
    sChromeMethods_ids[0] = JSID_VOID;
    return;
  }

  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::CSSStyleDeclaration],
                              nullptr, &_constructor_holder, 0, &protoAndIfaceArray[constructors::id::CSSStyleDeclaration],
                              &Class,
                              &sNativeProperties,
                              xpc::AccessCheck::isChrome(aGlobal) ? &sChromeOnlyNativeProperties : nullptr,
                              "CSSStyleDeclaration");
}

bool
ResolveOwnProperty(JSContext* cx, JSObject* wrapper, JSObject* obj, jsid id, JSPropertyDescriptor* desc, unsigned flags)
{
  // We rely on getOwnPropertyDescriptor not shadowing prototype properties by named
  // properties. If that changes we'll need to filter here.
  return js::GetProxyHandler(obj)->getOwnPropertyDescriptor(cx, wrapper, id, desc, flags);

}

bool
EnumerateOwnProperties(JSContext* cx, JSObject* wrapper, JSObject* obj, JS::AutoIdVector& props)
{
  // We rely on getOwnPropertyNames not shadowing prototype properties by named
  // properties. If that changes we'll need to filter here.
  return js::GetProxyHandler(obj)->getOwnPropertyNames(cx, wrapper, props);

}

JSObject*
DefineDOMInterface(JSContext* aCx, JSObject* aGlobal, bool* aEnabled)
{

  *aEnabled = true;
  return GetConstructorObject(aCx, aGlobal);
}

MOZ_ALWAYS_INLINE bool
IsProxy(JSObject* obj)
{
  return js::IsProxy(obj) && js::GetProxyHandler(obj) == DOMProxyHandler::getInstance();
}

MOZ_ALWAYS_INLINE nsICSSDeclaration*
UnwrapProxy(JSObject* obj)
{
  if (xpc::WrapperFactory::IsXrayWrapper(obj)) {
    obj = js::UnwrapObject(obj);
  }
  MOZ_ASSERT(IsProxy(obj));
  return static_cast<nsICSSDeclaration*>(js::GetProxyPrivate(obj).toPrivate());
}


const DOMClass Class = {
  { prototypes::id::CSSStyleDeclaration, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count },
  true,
  &sNativePropertyHooks,
  GetParentObject<nsICSSDeclaration>::Get,
  GetProtoObject,
  nullptr
};



DOMProxyHandler::DOMProxyHandler()
  : mozilla::dom::DOMProxyHandler(Class)
{

}


bool
DOMProxyHandler::getOwnPropertyDescriptor(JSContext* cx, JSObject* proxy, jsid id, JSPropertyDescriptor* desc, unsigned flags)
{
  int32_t index = GetArrayIndexFromId(cx, id);
  if (!(flags & JSRESOLVE_ASSIGNING)) {
    if (IsArrayIndex(index)) {
      nsICSSDeclaration* self = UnwrapProxy(proxy);
      bool found;
      nsString result;
      self->IndexedGetter(index, found, result);

      if (found) {
        if (!xpc::NonVoidStringToJsval(cx, result, &desc->value)) {
          return false;
        }
        FillPropertyDescriptor(desc, proxy, true);
        return true;
      }
    }
  }

  JSObject* expando;
  if (!xpc::WrapperFactory::IsXrayWrapper(proxy) && (expando = GetExpandoObject(proxy))) {
    if (!JS_GetPropertyDescriptorById(cx, expando, id, flags, desc)) {
      return false;
    }
    if (desc->obj) {
      // Pretend the property lives on the wrapper.
      desc->obj = proxy;
      return true;
    }
  }

  desc->obj = NULL;
  return true;
}


bool
DOMProxyHandler::getOwnPropertyNames(JSContext* cx, JSObject* proxy, JS::AutoIdVector& props)
{
  uint32_t length = UnwrapProxy(proxy)->Length();
  MOZ_ASSERT(int32_t(length) >= 0);
  for (int32_t i = 0; i < int32_t(length); ++i) {
    if (!props.append(INT_TO_JSID(i))) {
      return false;
    }
  }

  JSObject* expando;
  if (!xpc::WrapperFactory::IsXrayWrapper(proxy) && (expando = DOMProxyHandler::GetExpandoObject(proxy)) &&
      !js::GetPropertyNames(cx, expando, JSITER_OWNONLY | JSITER_HIDDEN, &props)) {
    return false;
  }

  return true;
}


bool
DOMProxyHandler::hasOwn(JSContext* cx, JSObject* proxy, jsid id, bool* bp)
{
  int32_t index = GetArrayIndexFromId(cx, id);
  if (IsArrayIndex(index)) {
    nsICSSDeclaration* self = UnwrapProxy(proxy);
    bool found;
    nsString result;
    self->IndexedGetter(index, found, result);
    (void)result;

    *bp = found;
    return true;
  }

  JSObject* expando = GetExpandoObject(proxy);
  if (expando) {
    JSBool b = true;
    JSBool ok = JS_HasPropertyById(cx, expando, id, &b);
    *bp = !!b;
    if (!ok || *bp) {
      return ok;
    }
  }

  *bp = false;
  return true;
}


bool
DOMProxyHandler::get(JSContext* cx, JSObject* proxy, JSObject* receiver, jsid id, JS::Value* vp)
{
  MOZ_ASSERT(!xpc::WrapperFactory::IsXrayWrapper(proxy),
              "Should not have a XrayWrapper here");

  int32_t index = GetArrayIndexFromId(cx, id);
  if (IsArrayIndex(index)) {
    nsICSSDeclaration* self = UnwrapProxy(proxy);
    bool found;
    nsString result;
    self->IndexedGetter(index, found, result);

    if (found) {
      if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
        return false;
      }
      return true;
    }
    // Even if we don't have this index, we don't forward the
    // get on to our expando object.
  } else {
    JSObject* expando = DOMProxyHandler::GetExpandoObject(proxy);
    if (expando) {
      JSBool hasProp;
      if (!JS_HasPropertyById(cx, expando, id, &hasProp)) {
        return false;
      }

      if (hasProp) {
        return JS_GetPropertyById(cx, expando, id, vp);
      }
    }
  }

  {  // Scope for this "found" so it doesn't leak to things below
    bool found;
    if (!GetPropertyOnPrototype(cx, proxy, id, &found, vp)) {
      return false;
    }

    if (found) {
      return true;
    }
  }

  vp->setUndefined();
  return true;
}


JSString*
DOMProxyHandler::obj_toString(JSContext* cx, JSObject* proxy)
{
  return mozilla::dom::DOMProxyHandler::obj_toString(cx, "CSSStyleDeclaration");
}


void
DOMProxyHandler::finalize(JSFreeOp* fop, JSObject* proxy)
{
  nsICSSDeclaration* self = UnwrapProxy(proxy);

  if (self) {
    ClearWrapper(self, self);
    XPCJSRuntime *rt = nsXPConnect::GetRuntimeInstance();
    if (rt) {
      rt->DeferredRelease(reinterpret_cast<nsISupports*>(self));
    } else {
      NS_RELEASE(self);
    }
  }
}


bool
DOMProxyHandler::getElementIfPresent(JSContext* cx, JSObject* proxy, JSObject* receiver, uint32_t index, JS::Value* vp, bool* present)
{
  MOZ_ASSERT(!xpc::WrapperFactory::IsXrayWrapper(proxy),
               "Should not have a XrayWrapper here");

  nsICSSDeclaration* self = UnwrapProxy(proxy);
  bool found;
  nsString result;
  self->IndexedGetter(index, found, result);

  if (found) {
    if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
      return false;
    }
    *present = found;
    return true;
  }
  // We skip the expando object and any named getters if
  // there is an indexed getter.


  JSObject *proto;
  if (!js::GetObjectProto(cx, proxy, &proto)) {
    return false;
  }
  if (proto) {
    JSBool isPresent;
    if (!JS_GetElementIfPresent(cx, proto, index, proxy, vp, &isPresent)) {
      return false;
    }
    *present = isPresent;
    return true;
  }

  *present = false;
  // Can't Debug_SetValueRangeToCrashOnTouch because it's not public
  return true;
}


DOMProxyHandler*
DOMProxyHandler::getInstance()
{
  static DOMProxyHandler instance;
  return &instance;
}


bool
DOMProxyHandler::delete_(JSContext* cx, JSObject* proxy, jsid id, bool* bp)
{
  int32_t index = GetArrayIndexFromId(cx, id);
  if (IsArrayIndex(index)) {
    nsICSSDeclaration* self = UnwrapProxy(proxy);
    bool found;
    nsString result;
    self->IndexedGetter(index, found, result);
    (void)result;
    if (found) {
      // XXXbz we should throw if Throw is true!
      *bp = false;
    } else {
      *bp = true;
    }
    // We always return here, even if the property was not found
    return true;
  }
  return dom::DOMProxyHandler::delete_(cx, proxy, id, bp);
}


bool
Is(JSObject* obj)
{
  return IsProxy(obj);
}

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, nsICSSDeclaration* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  MOZ_ASSERT(static_cast<nsICSSDeclaration*>(aObject) ==
             reinterpret_cast<nsICSSDeclaration*>(aObject));

  *aTriedToWrap = true;

  JSObject* parent = WrapNativeParent(aCx, aScope, aObject->GetParentObject());
  if (!parent) {
    return NULL;
  }

  // That might have ended up wrapping us already, due to the wonders
  // of XBL.  Check for that, and bail out as needed.  Scope so we don't
  // collide with the "obj" we declare in CreateBindingJSObject.
  {
    JSObject* obj = aCache->GetWrapper();
    if (obj) {
      return obj;
    }
  }

  JSAutoCompartment ac(aCx, parent);
  JSObject* global = JS_GetGlobalForObject(aCx, parent);

  JSObject* proto = GetProtoObject(aCx, global);
  if (!proto) {
    return NULL;
  }

  JSObject *obj = NewProxyObject(aCx, DOMProxyHandler::getInstance(),
                                 JS::PrivateValue(aObject), proto, parent);
  if (!obj) {
    return NULL;
  }

  NS_ADDREF(aObject);


  aCache->SetWrapper(obj);

  return obj;
}

} // namespace CSSStyleDeclarationBinding



} // namespace dom
} // namespace mozilla
