/* THIS FILE IS AUTOGENERATED - DO NOT EDIT */

#include "AccessCheck.h"
#include "ElementBinding.h"
#include "EventTargetBinding.h"
#include "NodeBinding.h"
#include "PrimitiveConversions.h"
#include "SVGAltGlyphElementBinding.h"
#include "SVGElementBinding.h"
#include "SVGGraphicsElementBinding.h"
#include "SVGLocatableElementBinding.h"
#include "SVGTextContentElementBinding.h"
#include "SVGTextPositioningElementBinding.h"
#include "SVGTransformableElementBinding.h"
#include "WorkerPrivate.h"
#include "XPCQuickStubs.h"
#include "XPCWrapper.h"
#include "mozilla/Preferences.h"
#include "mozilla/dom/BindingUtils.h"
#include "mozilla/dom/NonRefcountedDOMObject.h"
#include "mozilla/dom/Nullable.h"
#include "mozilla/dom/SVGAltGlyphElement.h"
#include "nsContentUtils.h"
#include "nsDOMQS.h"
#include "nsIDOMSVGAnimatedString.h"

using namespace mozilla::dom;
namespace mozilla {
namespace dom {

namespace SVGAltGlyphElementBinding {

static bool
get_glyphRef(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGAltGlyphElement* self, JS::Value* vp)
{
  nsString result;
  self->GetGlyphRef(result);
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}

static bool
set_glyphRef(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGAltGlyphElement* self, JS::Value* argv)
{
  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  self->SetGlyphRef(arg0);

  return true;
}


const JSJitInfo glyphRef_getterinfo = {
  (JSJitPropertyOp)get_glyphRef,
  prototypes::id::SVGAltGlyphElement,
  PrototypeTraits<prototypes::id::SVGAltGlyphElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

const JSJitInfo glyphRef_setterinfo = {
  (JSJitPropertyOp)set_glyphRef,
  prototypes::id::SVGAltGlyphElement,
  PrototypeTraits<prototypes::id::SVGAltGlyphElement>::Depth,
  JSJitInfo::Setter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNDEFINED   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_format(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGAltGlyphElement* self, JS::Value* vp)
{
  nsString result;
  self->GetFormat(result);
  if (!xpc::NonVoidStringToJsval(cx, result, vp)) {
    return false;
  }
  return true;
}

static bool
set_format(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGAltGlyphElement* self, JS::Value* argv)
{
  FakeDependentString arg0_holder;
  const NonNull<nsAString> arg0;
  if (!ConvertJSValueToString(cx, argv[0], &argv[0], eStringify, eStringify, arg0_holder)) {
    return false;
  }
  const_cast<NonNull<nsAString>&>(arg0) = &arg0_holder;
  self->SetFormat(arg0);

  return true;
}


const JSJitInfo format_getterinfo = {
  (JSJitPropertyOp)get_format,
  prototypes::id::SVGAltGlyphElement,
  PrototypeTraits<prototypes::id::SVGAltGlyphElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_STRING   /* returnType.  Only relevant for getters/methods. */
};

const JSJitInfo format_setterinfo = {
  (JSJitPropertyOp)set_format,
  prototypes::id::SVGAltGlyphElement,
  PrototypeTraits<prototypes::id::SVGAltGlyphElement>::Depth,
  JSJitInfo::Setter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_UNDEFINED   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_href(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGAltGlyphElement* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMSVGAnimatedString> result;
  result = self->Href();
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo href_getterinfo = {
  (JSJitPropertyOp)get_href,
  prototypes::id::SVGAltGlyphElement,
  PrototypeTraits<prototypes::id::SVGAltGlyphElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_OBJECT   /* returnType.  Only relevant for getters/methods. */
};

static JSBool
genericGetter(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  mozilla::dom::SVGAltGlyphElement* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::SVGAltGlyphElement, mozilla::dom::SVGAltGlyphElement>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "SVGAltGlyphElement");
    }
  }
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Getter);
  JSJitPropertyOp getter = info->op;
  return getter(cx, obj, self, vp);
}

static JSBool
genericSetter(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  mozilla::dom::SVGAltGlyphElement* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::SVGAltGlyphElement, mozilla::dom::SVGAltGlyphElement>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "SVGAltGlyphElement");
    }
  }
  if (argc == 0) {
    return ThrowErrorMessage(cx, MSG_MISSING_ARGUMENTS, "SVGAltGlyphElement attribute setter");
  }
  JS::Value* argv = JS_ARGV(cx, vp);
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Setter);
  JSJitPropertyOp setter = info->op;
  if (!setter(cx, obj, self, argv)) {
    return false;
  }
  *vp = JSVAL_VOID;
  return true;
}

static JSBool
_addProperty(JSContext* cx, JSHandleObject obj, JSHandleId id, JSMutableHandleValue vp)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::SVGAltGlyphElement>::value), "Must be an nsISupports class");
  mozilla::dom::SVGAltGlyphElement* self = UnwrapDOMObject<mozilla::dom::SVGAltGlyphElement>(obj);
  nsContentUtils::PreserveWrapper(reinterpret_cast<nsISupports*>(self), self);
  return true;
}

static void
_finalize(JSFreeOp* fop, JSObject* obj)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::SVGAltGlyphElement>::value), "Must be an nsISupports class");
  mozilla::dom::SVGAltGlyphElement* self = UnwrapDOMObject<mozilla::dom::SVGAltGlyphElement>(obj);
  if (self) {
    ClearWrapper(self, self);
    XPCJSRuntime *rt = nsXPConnect::GetRuntimeInstance();
    if (rt) {
      rt->DeferredRelease(reinterpret_cast<nsISupports*>(self));
    } else {
      NS_RELEASE(self);
    }
  }
}

static JSPropertySpec sAttributes_specs[] = {
  { "glyphRef", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &glyphRef_getterinfo }, { (JSStrictPropertyOp)genericSetter, &glyphRef_setterinfo }},
  { "format", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &format_getterinfo }, { (JSStrictPropertyOp)genericSetter, &format_setterinfo }},
  { "href", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &href_getterinfo }, JSOP_NULLWRAPPER},
  { 0, 0, 0, JSOP_NULLWRAPPER, JSOP_NULLWRAPPER }
};

static Prefable<JSPropertySpec> sAttributes[] = {
  { true, &sAttributes_specs[0] },
  { false, NULL }
};

static jsid sAttributes_ids[4] = { JSID_VOID };


static const NativeProperties sNativeProperties = {
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  sAttributes, sAttributes_ids, sAttributes_specs,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr
};
const NativePropertyHooks sNativePropertyHooks = {
  nullptr,
  nullptr,
  { &sNativeProperties, nullptr },
  prototypes::id::SVGAltGlyphElement,
  constructors::id::SVGAltGlyphElement,
  &SVGTextPositioningElementBinding::sNativePropertyHooks
};

JSNativeHolder _constructor_holder = {
  ThrowingConstructor,
  &sNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "SVGAltGlyphElementPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = SVGTextPositioningElementBinding::GetProtoObject(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  if (sAttributes_ids[0] == JSID_VOID &&
      !InitIds(aCx, sAttributes, sAttributes_ids)) {
    sAttributes_ids[0] = JSID_VOID;
    return;
  }

  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::SVGAltGlyphElement],
                              nullptr, &_constructor_holder, 0, &protoAndIfaceArray[constructors::id::SVGAltGlyphElement],
                              &Class.mClass,
                              &sNativeProperties,
                              nullptr,
                              "SVGAltGlyphElement");
}

JSObject*
DefineDOMInterface(JSContext* aCx, JSObject* aGlobal, bool* aEnabled)
{

  *aEnabled = true;
  return GetConstructorObject(aCx, aGlobal);
}


DOMJSClass Class = {
  { "SVGAltGlyphElement",
    JSCLASS_IS_DOMJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(3),
    _addProperty, /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    _finalize, /* finalize */
    NULL,                  /* checkAccess */
    NULL,                  /* call */
    NULL,                  /* hasInstance */
    NULL,                  /* construct */
    NULL, /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  {
    { prototypes::id::EventTarget, prototypes::id::Node, prototypes::id::Element, prototypes::id::SVGElement, prototypes::id::SVGLocatableElement, prototypes::id::SVGTransformableElement, prototypes::id::SVGGraphicsElement, prototypes::id::SVGTextContentElement, prototypes::id::SVGTextPositioningElement, prototypes::id::SVGAltGlyphElement },
    true,
    &sNativePropertyHooks,
    GetParentObject<mozilla::dom::SVGAltGlyphElement>::Get,
    GetProtoObject,
    nullptr
  }
};

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, mozilla::dom::SVGAltGlyphElement* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  MOZ_ASSERT(static_cast<mozilla::dom::SVGAltGlyphElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGAltGlyphElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTextPositioningElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTextPositioningElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTextContentElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTextContentElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGGraphicsElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGGraphicsElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTransformableElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTransformableElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGLocatableElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGLocatableElement*>(aObject));
  MOZ_ASSERT(static_cast<nsSVGElement*>(aObject) ==
             reinterpret_cast<nsSVGElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::Element*>(aObject) ==
             reinterpret_cast<mozilla::dom::Element*>(aObject));
  MOZ_ASSERT(static_cast<nsINode*>(aObject) ==
             reinterpret_cast<nsINode*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::EventTarget*>(aObject) ==
             reinterpret_cast<mozilla::dom::EventTarget*>(aObject));

  *aTriedToWrap = true;

  JSObject* parent = WrapNativeParent(aCx, aScope, aObject->GetParentObject());
  if (!parent) {
    return NULL;
  }

  // That might have ended up wrapping us already, due to the wonders
  // of XBL.  Check for that, and bail out as needed.  Scope so we don't
  // collide with the "obj" we declare in CreateBindingJSObject.
  {
    JSObject* obj = aCache->GetWrapper();
    if (obj) {
      return obj;
    }
  }

  JSAutoCompartment ac(aCx, parent);
  JSObject* global = JS_GetGlobalForObject(aCx, parent);

  JSObject* proto = GetProtoObject(aCx, global);
  if (!proto) {
    return NULL;
  }

  JSObject* obj = JS_NewObject(aCx, &Class.mBase, proto, parent);
  if (!obj) {
    return NULL;
  }

  js::SetReservedSlot(obj, DOM_OBJECT_SLOT, PRIVATE_TO_JSVAL(aObject));
  NS_ADDREF(aObject);


  aCache->SetWrapper(obj);

  return obj;
}

} // namespace SVGAltGlyphElementBinding



} // namespace dom
} // namespace mozilla
