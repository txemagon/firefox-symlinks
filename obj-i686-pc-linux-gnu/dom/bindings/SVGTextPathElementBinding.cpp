/* THIS FILE IS AUTOGENERATED - DO NOT EDIT */

#include "AccessCheck.h"
#include "ElementBinding.h"
#include "EventTargetBinding.h"
#include "NodeBinding.h"
#include "PrimitiveConversions.h"
#include "SVGCircleElementBinding.h"
#include "SVGElementBinding.h"
#include "SVGGraphicsElementBinding.h"
#include "SVGLocatableElementBinding.h"
#include "SVGTextContentElementBinding.h"
#include "SVGTextPathElementBinding.h"
#include "SVGTransformableElementBinding.h"
#include "WorkerPrivate.h"
#include "XPCQuickStubs.h"
#include "XPCWrapper.h"
#include "mozilla/Preferences.h"
#include "mozilla/dom/BindingUtils.h"
#include "mozilla/dom/NonRefcountedDOMObject.h"
#include "mozilla/dom/Nullable.h"
#include "mozilla/dom/SVGTextPathElement.h"
#include "nsContentUtils.h"
#include "nsDOMQS.h"
#include "nsIDOMSVGAnimatedEnum.h"
#include "nsIDOMSVGAnimatedLength.h"
#include "nsIDOMSVGAnimatedString.h"

using namespace mozilla::dom;
namespace mozilla {
namespace dom {

namespace SVGTextPathElementBinding {

static bool
get_startOffset(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGTextPathElement* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMSVGAnimatedLength> result;
  result = self->StartOffset();
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo startOffset_getterinfo = {
  (JSJitPropertyOp)get_startOffset,
  prototypes::id::SVGTextPathElement,
  PrototypeTraits<prototypes::id::SVGTextPathElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_OBJECT   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_method(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGTextPathElement* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMSVGAnimatedEnumeration> result;
  result = self->Method();
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo method_getterinfo = {
  (JSJitPropertyOp)get_method,
  prototypes::id::SVGTextPathElement,
  PrototypeTraits<prototypes::id::SVGTextPathElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_OBJECT   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_spacing(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGTextPathElement* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMSVGAnimatedEnumeration> result;
  result = self->Spacing();
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo spacing_getterinfo = {
  (JSJitPropertyOp)get_spacing,
  prototypes::id::SVGTextPathElement,
  PrototypeTraits<prototypes::id::SVGTextPathElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_OBJECT   /* returnType.  Only relevant for getters/methods. */
};

static bool
get_href(JSContext* cx, JSHandleObject obj, mozilla::dom::SVGTextPathElement* self, JS::Value* vp)
{
  nsRefPtr<nsIDOMSVGAnimatedString> result;
  result = self->Href();
  if (!WrapObject(cx, obj, result, vp)) {
    return false;
  }
  return true;
}


const JSJitInfo href_getterinfo = {
  (JSJitPropertyOp)get_href,
  prototypes::id::SVGTextPathElement,
  PrototypeTraits<prototypes::id::SVGTextPathElement>::Depth,
  JSJitInfo::Getter,
  false,  /* isInfallible. False in setters. */
  false,  /* isConstant. Only relevant for getters. */
  JSVAL_TYPE_OBJECT   /* returnType.  Only relevant for getters/methods. */
};

static JSBool
genericGetter(JSContext* cx, unsigned argc, JS::Value* vp)
{
  js::RootedObject obj(cx, JS_THIS_OBJECT(cx, vp));
  if (!obj) {
    return false;
  }

  mozilla::dom::SVGTextPathElement* self;
  {
    nsresult rv = UnwrapObject<prototypes::id::SVGTextPathElement, mozilla::dom::SVGTextPathElement>(cx, obj, self);
    if (NS_FAILED(rv)) {
      return ThrowErrorMessage(cx, MSG_DOES_NOT_IMPLEMENT_INTERFACE, "SVGTextPathElement");
    }
  }
  const JSJitInfo *info = FUNCTION_VALUE_TO_JITINFO(JS_CALLEE(cx, vp));
  MOZ_ASSERT(info->type == JSJitInfo::Getter);
  JSJitPropertyOp getter = info->op;
  return getter(cx, obj, self, vp);
}

static JSBool
_addProperty(JSContext* cx, JSHandleObject obj, JSHandleId id, JSMutableHandleValue vp)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::SVGTextPathElement>::value), "Must be an nsISupports class");
  mozilla::dom::SVGTextPathElement* self = UnwrapDOMObject<mozilla::dom::SVGTextPathElement>(obj);
  nsContentUtils::PreserveWrapper(reinterpret_cast<nsISupports*>(self), self);
  return true;
}

static void
_finalize(JSFreeOp* fop, JSObject* obj)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::SVGTextPathElement>::value), "Must be an nsISupports class");
  mozilla::dom::SVGTextPathElement* self = UnwrapDOMObject<mozilla::dom::SVGTextPathElement>(obj);
  if (self) {
    ClearWrapper(self, self);
    XPCJSRuntime *rt = nsXPConnect::GetRuntimeInstance();
    if (rt) {
      rt->DeferredRelease(reinterpret_cast<nsISupports*>(self));
    } else {
      NS_RELEASE(self);
    }
  }
}

static JSPropertySpec sAttributes_specs[] = {
  { "startOffset", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &startOffset_getterinfo }, JSOP_NULLWRAPPER},
  { "method", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &method_getterinfo }, JSOP_NULLWRAPPER},
  { "spacing", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &spacing_getterinfo }, JSOP_NULLWRAPPER},
  { "href", 0, JSPROP_SHARED | JSPROP_ENUMERATE | JSPROP_NATIVE_ACCESSORS, { (JSPropertyOp)genericGetter, &href_getterinfo }, JSOP_NULLWRAPPER},
  { 0, 0, 0, JSOP_NULLWRAPPER, JSOP_NULLWRAPPER }
};

static Prefable<JSPropertySpec> sAttributes[] = {
  { true, &sAttributes_specs[0] },
  { false, NULL }
};

static jsid sAttributes_ids[5] = { JSID_VOID };

static ConstantSpec sConstants_specs[] = {
  { "TEXTPATH_METHODTYPE_UNKNOWN", INT_TO_JSVAL(0) },
  { "TEXTPATH_METHODTYPE_ALIGN", INT_TO_JSVAL(1) },
  { "TEXTPATH_METHODTYPE_STRETCH", INT_TO_JSVAL(2) },
  { "TEXTPATH_SPACINGTYPE_UNKNOWN", INT_TO_JSVAL(0) },
  { "TEXTPATH_SPACINGTYPE_AUTO", INT_TO_JSVAL(1) },
  { "TEXTPATH_SPACINGTYPE_EXACT", INT_TO_JSVAL(2) },
  { 0, JSVAL_VOID }
};

static Prefable<ConstantSpec> sConstants[] = {
  { true, &sConstants_specs[0] },
  { false, NULL }
};

static jsid sConstants_ids[7] = { JSID_VOID };


static const NativeProperties sNativeProperties = {
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  nullptr, nullptr, nullptr,
  sAttributes, sAttributes_ids, sAttributes_specs,
  nullptr, nullptr, nullptr,
  sConstants, sConstants_ids, sConstants_specs
};
const NativePropertyHooks sNativePropertyHooks = {
  nullptr,
  nullptr,
  { &sNativeProperties, nullptr },
  prototypes::id::SVGTextPathElement,
  constructors::id::SVGTextPathElement,
  &SVGTextContentElementBinding::sNativePropertyHooks
};

JSNativeHolder _constructor_holder = {
  ThrowingConstructor,
  &sNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "SVGTextPathElementPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = SVGTextContentElementBinding::GetProtoObject(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  if (sAttributes_ids[0] == JSID_VOID &&
      (!InitIds(aCx, sAttributes, sAttributes_ids) ||
       !InitIds(aCx, sConstants, sConstants_ids))) {
    sAttributes_ids[0] = JSID_VOID;
    return;
  }

  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::SVGTextPathElement],
                              nullptr, &_constructor_holder, 0, &protoAndIfaceArray[constructors::id::SVGTextPathElement],
                              &Class.mClass,
                              &sNativeProperties,
                              nullptr,
                              "SVGTextPathElement");
}

JSObject*
DefineDOMInterface(JSContext* aCx, JSObject* aGlobal, bool* aEnabled)
{

  *aEnabled = true;
  return GetConstructorObject(aCx, aGlobal);
}


DOMJSClass Class = {
  { "SVGTextPathElement",
    JSCLASS_IS_DOMJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(3),
    _addProperty, /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    _finalize, /* finalize */
    NULL,                  /* checkAccess */
    NULL,                  /* call */
    NULL,                  /* hasInstance */
    NULL,                  /* construct */
    NULL, /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  {
    { prototypes::id::EventTarget, prototypes::id::Node, prototypes::id::Element, prototypes::id::SVGElement, prototypes::id::SVGLocatableElement, prototypes::id::SVGTransformableElement, prototypes::id::SVGGraphicsElement, prototypes::id::SVGTextContentElement, prototypes::id::SVGTextPathElement, prototypes::id::_ID_Count },
    true,
    &sNativePropertyHooks,
    GetParentObject<mozilla::dom::SVGTextPathElement>::Get,
    GetProtoObject,
    nullptr
  }
};

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, mozilla::dom::SVGTextPathElement* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTextPathElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTextPathElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTextContentElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTextContentElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGGraphicsElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGGraphicsElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGTransformableElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGTransformableElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::SVGLocatableElement*>(aObject) ==
             reinterpret_cast<mozilla::dom::SVGLocatableElement*>(aObject));
  MOZ_ASSERT(static_cast<nsSVGElement*>(aObject) ==
             reinterpret_cast<nsSVGElement*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::Element*>(aObject) ==
             reinterpret_cast<mozilla::dom::Element*>(aObject));
  MOZ_ASSERT(static_cast<nsINode*>(aObject) ==
             reinterpret_cast<nsINode*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::EventTarget*>(aObject) ==
             reinterpret_cast<mozilla::dom::EventTarget*>(aObject));

  *aTriedToWrap = true;

  JSObject* parent = WrapNativeParent(aCx, aScope, aObject->GetParentObject());
  if (!parent) {
    return NULL;
  }

  // That might have ended up wrapping us already, due to the wonders
  // of XBL.  Check for that, and bail out as needed.  Scope so we don't
  // collide with the "obj" we declare in CreateBindingJSObject.
  {
    JSObject* obj = aCache->GetWrapper();
    if (obj) {
      return obj;
    }
  }

  JSAutoCompartment ac(aCx, parent);
  JSObject* global = JS_GetGlobalForObject(aCx, parent);

  JSObject* proto = GetProtoObject(aCx, global);
  if (!proto) {
    return NULL;
  }

  JSObject* obj = JS_NewObject(aCx, &Class.mBase, proto, parent);
  if (!obj) {
    return NULL;
  }

  js::SetReservedSlot(obj, DOM_OBJECT_SLOT, PRIVATE_TO_JSVAL(aObject));
  NS_ADDREF(aObject);


  aCache->SetWrapper(obj);

  return obj;
}

} // namespace SVGTextPathElementBinding



} // namespace dom
} // namespace mozilla
