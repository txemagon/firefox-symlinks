/* THIS FILE IS AUTOGENERATED - DO NOT EDIT */

#include "AccessCheck.h"
#include "CharacterDataBinding.h"
#include "CommentBinding.h"
#include "EventTargetBinding.h"
#include "NodeBinding.h"
#include "PrimitiveConversions.h"
#include "WorkerPrivate.h"
#include "XPCQuickStubs.h"
#include "XPCWrapper.h"
#include "mozilla/Preferences.h"
#include "mozilla/dom/BindingUtils.h"
#include "mozilla/dom/Comment.h"
#include "mozilla/dom/NonRefcountedDOMObject.h"
#include "mozilla/dom/Nullable.h"
#include "nsContentUtils.h"
#include "nsDOMQS.h"
#include "nsIDOMComment.h"

using namespace mozilla::dom;
namespace mozilla {
namespace dom {

namespace CommentBinding {

static JSBool
_addProperty(JSContext* cx, JSHandleObject obj, JSHandleId id, JSMutableHandleValue vp)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::Comment>::value), "Must be an nsISupports class");
  mozilla::dom::Comment* self = UnwrapDOMObject<mozilla::dom::Comment>(obj);
  nsContentUtils::PreserveWrapper(reinterpret_cast<nsISupports*>(self), self);
  return true;
}

static void
_finalize(JSFreeOp* fop, JSObject* obj)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, mozilla::dom::Comment>::value), "Must be an nsISupports class");
  mozilla::dom::Comment* self = UnwrapDOMObject<mozilla::dom::Comment>(obj);
  if (self) {
    ClearWrapper(self, self);
    XPCJSRuntime *rt = nsXPConnect::GetRuntimeInstance();
    if (rt) {
      rt->DeferredRelease(reinterpret_cast<nsISupports*>(self));
    } else {
      NS_RELEASE(self);
    }
  }
}

const NativePropertyHooks sNativePropertyHooks = {
  nullptr,
  nullptr,
  { nullptr, nullptr },
  prototypes::id::Comment,
  constructors::id::Comment,
  &CharacterDataBinding::sNativePropertyHooks
};

static JSBool
_hasInstance(JSContext* cx, JSHandleObject obj, JSMutableHandleValue vp, JSBool* bp)
{
  if (!vp.isObject()) {
    *bp = false;
    return true;
  }

  jsval protov;
  if (!JS_GetProperty(cx, obj, "prototype", &protov))
    return false;
  if (!protov.isObject()) {
    JS_ReportErrorNumber(cx, js_GetErrorMessage, NULL, JSMSG_BAD_PROTOTYPE,
                         "Comment");
    return false;
  }
  JSObject *objProto = &protov.toObject();

  JSObject* instance = &vp.toObject();
  JSObject* proto;
  if (!JS_GetPrototype(cx, instance, &proto))
    return false;
  while (proto) {
    if (proto == objProto) {
      *bp = true;
      return true;
    }
    if (!JS_GetPrototype(cx, proto, &proto))
      return false;
  }

  // FIXME Limit this to chrome by checking xpc::AccessCheck::isChrome(obj).
  nsISupports* native =
    nsContentUtils::XPConnect()->GetNativeOfWrapper(cx, instance);
  nsCOMPtr<nsIDOMComment> qiResult = do_QueryInterface(native);
  *bp = !!qiResult;
  return true;

}


static DOMIfaceAndProtoJSClass InterfaceObjectClass = {
  {
    "Function",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    ThrowingConstructor, /* call */
    _hasInstance, /* hasInstance */
    ThrowingConstructor, /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterface,
  &sNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "CommentPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = CharacterDataBinding::GetProtoObject(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::Comment],
                              &InterfaceObjectClass.mBase, nullptr, 0, &protoAndIfaceArray[constructors::id::Comment],
                              &Class.mClass,
                              nullptr,
                              nullptr,
                              "Comment");
}

JSObject*
DefineDOMInterface(JSContext* aCx, JSObject* aGlobal, bool* aEnabled)
{

  *aEnabled = true;
  return GetConstructorObject(aCx, aGlobal);
}


DOMJSClass Class = {
  { "Comment",
    JSCLASS_IS_DOMJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(3),
    _addProperty, /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    _finalize, /* finalize */
    NULL,                  /* checkAccess */
    NULL,                  /* call */
    NULL,                  /* hasInstance */
    NULL,                  /* construct */
    NULL, /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  {
    { prototypes::id::EventTarget, prototypes::id::Node, prototypes::id::CharacterData, prototypes::id::Comment, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count },
    true,
    &sNativePropertyHooks,
    GetParentObject<mozilla::dom::Comment>::Get,
    GetProtoObject,
    nullptr
  }
};

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, mozilla::dom::Comment* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  MOZ_ASSERT(static_cast<mozilla::dom::Comment*>(aObject) ==
             reinterpret_cast<mozilla::dom::Comment*>(aObject));
  MOZ_ASSERT(static_cast<nsGenericDOMDataNode*>(aObject) ==
             reinterpret_cast<nsGenericDOMDataNode*>(aObject));
  MOZ_ASSERT(static_cast<nsINode*>(aObject) ==
             reinterpret_cast<nsINode*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::EventTarget*>(aObject) ==
             reinterpret_cast<mozilla::dom::EventTarget*>(aObject));

  *aTriedToWrap = true;

  JSObject* parent = WrapNativeParent(aCx, aScope, aObject->GetParentObject());
  if (!parent) {
    return NULL;
  }

  // That might have ended up wrapping us already, due to the wonders
  // of XBL.  Check for that, and bail out as needed.  Scope so we don't
  // collide with the "obj" we declare in CreateBindingJSObject.
  {
    JSObject* obj = aCache->GetWrapper();
    if (obj) {
      return obj;
    }
  }

  JSAutoCompartment ac(aCx, parent);
  JSObject* global = JS_GetGlobalForObject(aCx, parent);

  JSObject* proto = GetProtoObject(aCx, global);
  if (!proto) {
    return NULL;
  }

  JSObject* obj = JS_NewObject(aCx, &Class.mBase, proto, parent);
  if (!obj) {
    return NULL;
  }

  js::SetReservedSlot(obj, DOM_OBJECT_SLOT, PRIVATE_TO_JSVAL(aObject));
  NS_ADDREF(aObject);


  aCache->SetWrapper(obj);

  return obj;
}

} // namespace CommentBinding



} // namespace dom
} // namespace mozilla
