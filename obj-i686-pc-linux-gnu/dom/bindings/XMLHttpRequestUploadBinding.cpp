/* THIS FILE IS AUTOGENERATED - DO NOT EDIT */

#include "AccessCheck.h"
#include "EventTargetBinding.h"
#include "PrimitiveConversions.h"
#include "WorkerPrivate.h"
#include "XMLHttpRequestEventTargetBinding.h"
#include "XMLHttpRequestUploadBinding.h"
#include "XPCQuickStubs.h"
#include "XPCWrapper.h"
#include "mozilla/Preferences.h"
#include "mozilla/dom/BindingUtils.h"
#include "mozilla/dom/NonRefcountedDOMObject.h"
#include "mozilla/dom/Nullable.h"
#include "mozilla/dom/workers/bindings/XMLHttpRequestUpload.h"
#include "nsContentUtils.h"
#include "nsDOMQS.h"
#include "nsXMLHttpRequest.h"

using namespace mozilla::dom;
namespace mozilla {
namespace dom {

namespace XMLHttpRequestUploadBinding {

static JSBool
_addProperty(JSContext* cx, JSHandleObject obj, JSHandleId id, JSMutableHandleValue vp)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, nsXMLHttpRequestUpload>::value), "Must be an nsISupports class");
  nsXMLHttpRequestUpload* self = UnwrapDOMObject<nsXMLHttpRequestUpload>(obj);
  nsContentUtils::PreserveWrapper(reinterpret_cast<nsISupports*>(self), self);
  return true;
}

static void
_finalize(JSFreeOp* fop, JSObject* obj)
{
  MOZ_STATIC_ASSERT((IsBaseOf<nsISupports, nsXMLHttpRequestUpload>::value), "Must be an nsISupports class");
  nsXMLHttpRequestUpload* self = UnwrapDOMObject<nsXMLHttpRequestUpload>(obj);
  if (self) {
    ClearWrapper(self, self);
    XPCJSRuntime *rt = nsXPConnect::GetRuntimeInstance();
    if (rt) {
      rt->DeferredRelease(reinterpret_cast<nsISupports*>(self));
    } else {
      NS_RELEASE(self);
    }
  }
}

const NativePropertyHooks sNativePropertyHooks = {
  nullptr,
  nullptr,
  { nullptr, nullptr },
  prototypes::id::XMLHttpRequestUpload,
  constructors::id::XMLHttpRequestUpload,
  &XMLHttpRequestEventTargetBinding::sNativePropertyHooks
};

JSNativeHolder _constructor_holder = {
  ThrowingConstructor,
  &sNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "XMLHttpRequestUploadPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = XMLHttpRequestEventTargetBinding::GetProtoObject(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::XMLHttpRequestUpload],
                              nullptr, &_constructor_holder, 0, &protoAndIfaceArray[constructors::id::XMLHttpRequestUpload],
                              &Class.mClass,
                              nullptr,
                              nullptr,
                              "XMLHttpRequestUpload");
}

JSObject*
DefineDOMInterface(JSContext* aCx, JSObject* aGlobal, bool* aEnabled)
{

  *aEnabled = true;
  return GetConstructorObject(aCx, aGlobal);
}


DOMJSClass Class = {
  { "XMLHttpRequestUpload",
    JSCLASS_IS_DOMJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(3),
    _addProperty, /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    _finalize, /* finalize */
    NULL,                  /* checkAccess */
    NULL,                  /* call */
    NULL,                  /* hasInstance */
    NULL,                  /* construct */
    NULL, /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  {
    { prototypes::id::EventTarget, prototypes::id::XMLHttpRequestEventTarget, prototypes::id::XMLHttpRequestUpload, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count },
    true,
    &sNativePropertyHooks,
    GetParentObject<nsXMLHttpRequestUpload>::Get,
    GetProtoObject,
    nullptr
  }
};

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, nsXMLHttpRequestUpload* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  MOZ_ASSERT(static_cast<nsXMLHttpRequestUpload*>(aObject) ==
             reinterpret_cast<nsXMLHttpRequestUpload*>(aObject));
  MOZ_ASSERT(static_cast<nsXHREventTarget*>(aObject) ==
             reinterpret_cast<nsXHREventTarget*>(aObject));
  MOZ_ASSERT(static_cast<mozilla::dom::EventTarget*>(aObject) ==
             reinterpret_cast<mozilla::dom::EventTarget*>(aObject));

  *aTriedToWrap = true;

  JSObject* parent = WrapNativeParent(aCx, aScope, aObject->GetParentObject());
  if (!parent) {
    return NULL;
  }

  // That might have ended up wrapping us already, due to the wonders
  // of XBL.  Check for that, and bail out as needed.  Scope so we don't
  // collide with the "obj" we declare in CreateBindingJSObject.
  {
    JSObject* obj = aCache->GetWrapper();
    if (obj) {
      return obj;
    }
  }

  JSAutoCompartment ac(aCx, parent);
  JSObject* global = JS_GetGlobalForObject(aCx, parent);

  JSObject* proto = GetProtoObject(aCx, global);
  if (!proto) {
    return NULL;
  }

  JSObject* obj = JS_NewObject(aCx, &Class.mBase, proto, parent);
  if (!obj) {
    return NULL;
  }

  js::SetReservedSlot(obj, DOM_OBJECT_SLOT, PRIVATE_TO_JSVAL(aObject));
  NS_ADDREF(aObject);


  aCache->SetWrapper(obj);

  return obj;
}

} // namespace XMLHttpRequestUploadBinding



namespace XMLHttpRequestUploadBinding_workers {

static void
_finalize(JSFreeOp* fop, JSObject* obj)
{

  mozilla::dom::workers::XMLHttpRequestUpload* self = UnwrapDOMObject<mozilla::dom::workers::XMLHttpRequestUpload>(obj);
  if (self) {
    self->_finalize(fop);
  }
}

static void
_trace(JSTracer* trc, JSObject* obj)
{

  mozilla::dom::workers::XMLHttpRequestUpload* self = UnwrapDOMObject<mozilla::dom::workers::XMLHttpRequestUpload>(obj);
  if (self) {
    self->_trace(trc);
  }
}

JSNativeHolder _constructor_holder = {
  ThrowingConstructor,
  &sWorkerNativePropertyHooks
};

static DOMIfaceAndProtoJSClass PrototypeClass = {
  {
    "XMLHttpRequestUploadPrototype",
    JSCLASS_IS_DOMIFACEANDPROTOJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(2),
    JS_PropertyStub,       /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    nullptr,               /* finalize */
    nullptr,               /* checkAccess */
    nullptr,               /* call */
    nullptr,               /* hasInstance */
    nullptr,               /* construct */
    nullptr,               /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  eInterfacePrototype,
  &sWorkerNativePropertyHooks
};

void
CreateInterfaceObjects(JSContext* aCx, JSObject* aGlobal, JSObject** protoAndIfaceArray)
{
  JSObject* parentProto = XMLHttpRequestEventTargetBinding_workers::GetProtoObject(aCx, aGlobal);
  if (!parentProto) {
    return;
  }


  dom::CreateInterfaceObjects(aCx, aGlobal, parentProto,
                              &PrototypeClass.mBase, &protoAndIfaceArray[prototypes::id::XMLHttpRequestUpload_workers],
                              nullptr, &_constructor_holder, 0, &protoAndIfaceArray[constructors::id::XMLHttpRequestUpload_workers],
                              &Class.mClass,
                              nullptr,
                              nullptr,
                              "XMLHttpRequestUpload");
}


DOMJSClass Class = {
  { "XMLHttpRequestUpload",
    JSCLASS_IS_DOMJSCLASS | JSCLASS_HAS_RESERVED_SLOTS(3),
    JS_PropertyStub, /* addProperty */
    JS_PropertyStub,       /* delProperty */
    JS_PropertyStub,       /* getProperty */
    JS_StrictPropertyStub, /* setProperty */
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    _finalize, /* finalize */
    NULL,                  /* checkAccess */
    NULL,                  /* call */
    NULL,                  /* hasInstance */
    NULL,                  /* construct */
    _trace, /* trace */
    JSCLASS_NO_INTERNAL_MEMBERS
  },
  {
    { prototypes::id::EventTarget_workers, prototypes::id::XMLHttpRequestEventTarget_workers, prototypes::id::XMLHttpRequestUpload_workers, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count, prototypes::id::_ID_Count },
    false,
    &sWorkerNativePropertyHooks,
    GetParentObject<mozilla::dom::workers::XMLHttpRequestUpload>::Get,
    GetProtoObject,
    nullptr
  }
};

JSObject*
Wrap(JSContext* aCx, JSObject* aScope, mozilla::dom::workers::XMLHttpRequestUpload* aObject, nsWrapperCache* aCache, bool* aTriedToWrap)
{
  *aTriedToWrap = true;
  return aObject->GetJSObject();
}

} // namespace XMLHttpRequestUploadBinding_workers



} // namespace dom
} // namespace mozilla
